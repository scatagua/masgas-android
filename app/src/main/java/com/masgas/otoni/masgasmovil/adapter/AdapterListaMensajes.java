package com.masgas.otoni.masgasmovil.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.view.ViewBuzonNotificaciones;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by otoni on 22/2/2018.
 */

public class AdapterListaMensajes extends RecyclerView.Adapter<AdapterListaMensajes.ViewHolderNotificacion>
        implements ItemClickListener {

    private List<ViewBuzonNotificaciones> items;
    private Context context;
    public ModeloGeneral modeloRelacional;

    public static class ViewHolderNotificacion extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView nombre, contenido, fecha;

        // Interfaz de comunicación
        public ItemClickListener listener;

        public ViewHolderNotificacion(View v, ItemClickListener listener) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.titulo);
            contenido = (TextView) v.findViewById(R.id.contenido);
            fecha = (TextView) v.findViewById(R.id.fecha);

            v.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }

    public AdapterListaMensajes(List<ViewBuzonNotificaciones> items, Context context) {
        this.items = items;
        this.context = context;
        modeloRelacional = new ModeloGeneral(context);
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<ViewBuzonNotificaciones> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {
        // Imagen a compartir entre transiciones
        //View sharedImage = view.findViewById(R.id.imagen);
        //ListaCuentasPorCobrar.launch((Activity) context, items.get(position));
        Log.e("Pruebas", position + "");
        dialogDetalle(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolderNotificacion onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_mensaje_buzon, viewGroup, false);
        return new ViewHolderNotificacion(v, this);
    }

    @Override
    public void onBindViewHolder(final ViewHolderNotificacion viewHolder, final int position) {
        viewHolder.nombre.setText(items.get(position).getMensaje());
        viewHolder.contenido.setText("");
        try {
            Date date = new SimpleDateFormat("dd-MM-yyyy").parse(items.get(position).getFecha());
            viewHolder.fecha.setText(new SimpleDateFormat("dd-MM-yyyy").format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("Error", "Problema con las fechas");
        }
    }

    private void dialogDetalle(ViewBuzonNotificaciones obj) {

        final Dialog ventana = new Dialog(context);

        ventana.setContentView(R.layout.dialog_detalle);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.titulo);
        aceptar.setText(obj.getMensaje());

        TextView cuerpo = (TextView) ventana.findViewById(R.id.cuerpo);
        cuerpo.setText(obj.getCuerpo());

        ventana.show();
    }
}
