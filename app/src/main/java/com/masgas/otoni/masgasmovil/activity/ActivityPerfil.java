package com.masgas.otoni.masgasmovil.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.MapFragment;
import com.google.zxing.WriterException;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterListViewPerfil;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Direccion;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.utils.WebService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by otoni on 6/1/2018.
 */

public class ActivityPerfil extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    private NavigationView navigationView;
    private Toolbar toolbar;

    private AlertDialog dialog;

    TextView nombre, nombre2;
    LinearLayout empty, contDir;
    // Manejo de Places
    PlaceAutocompleteFragment autocompleteFragment;

    EditText tF, tL, correo, dI, fecha, municipio;
    TextInputLayout eTF, eTL, eC, eDI;

    List<String> titulos, contenidos;

    ModeloGeneral modeloGeneral;

    Usuario usuario;
    List<Direccion> direccions;
    String domicilio;
    Double lat = 0.0, lng = 0.0;
    Boolean isLocation = false;

    List<String> parametros, valores;

    Dialog ventana;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        //setContentView(R.layout.activity_perfil);
        setContentView(R.layout.activity_menu_perfil);

        modeloGeneral = new ModeloGeneral(this);
        usuario = modeloGeneral.consultarUsuarioActivo();
        direccions = modeloGeneral.consultarMisDirecciones();

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.menu_navegacion_menu_principal_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View hview = navigationView.getHeaderView(0);
        nombre2 = (TextView) hview.findViewById(R.id.menu_lateral_nombre);
        nombre2.setText(usuario.getNombre1() + " " + usuario.getApellido1());

        setTitle("Mi perfil");

        if (usuario.getIdRol() == 2) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_lateral_distribuidor);
        }

        /*
        titulos = new ArrayList<>();
        titulos.add("Número teléfono fijo");
        titulos.add("Número celular");
        titulos.add("Correo electrónico");
        titulos.add("Documento de identidad");

        contenidos = new ArrayList<>();
        contenidos.add(usuario.getTelfLocal());
        contenidos.add(usuario.getTelfCelular());
        contenidos.add(usuario.getCorreo());
        contenidos.add("" + usuario.getIdentificacion());
        */

        nombre = (TextView) findViewById(R.id.perfil_nombre);
        nombre.setTag(nombre.getKeyListener());
        nombre.setKeyListener(null);
        nombre.setText(usuario.getNombre1() + " " + usuario.getApellido1());

        tF = (EditText) findViewById(R.id.perfil_telefono_fijo);
        tF.setTag(tF.getKeyListener());
        tF.setKeyListener(null);
        tF.setText(usuario.getTelfLocal());

        tL = (EditText) findViewById(R.id.perfil_telefono_celular);
        tL.setTag(tL.getKeyListener());
        tL.setKeyListener(null);
        tL.setText(usuario.getTelfCelular());

        correo = (EditText) findViewById(R.id.perfil_correo);
        correo.setTag(correo.getKeyListener());
        correo.setKeyListener(null);
        correo.setText(usuario.getCorreo());

        dI = (EditText) findViewById(R.id.perfil_documento_identidad);
        dI.setTag(dI.getKeyListener());
        dI.setKeyListener(null);
        dI.setText("" + usuario.getIdentificacion());

        fecha = (EditText) findViewById(R.id.perfil_fecha_nacimiento);
        fecha.setTag(fecha.getKeyListener());
        fecha.setKeyListener(null);
        Date date = null;
        try {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(usuario.getFechaNac());
            fecha.setText(new SimpleDateFormat("dd-MM-yyyy").format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("Error", "no se pudo la fecga");
        }

        contDir = (LinearLayout) findViewById(R.id.perfil_content_direcciones);

        actualizarDirecciones();
        /*
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        */

        parametros = new ArrayList<>();

        valores = new ArrayList<>();

        // Obetener vista vacia
        empty = (LinearLayout) findViewById(R.id.empty_state_container);

        /*
        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        adapter = new AdapterListViewPerfil(titulos, contenidos, this);
        recycler.setAdapter(adapter);

        verificarVistas();
        */
    }

    @Override
    public void onResume() {
        super.onResume();
        actualizarDirecciones();
        actualizarUser();
    }

    public void actualizarDirecciones() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params2.setMargins(0, 0, 0, 12);

        direccions = modeloGeneral.consultarMisDirecciones();

        contDir.removeAllViews();

        for (int i = 0; i < direccions.size(); i++) {
            final Integer aux = i;
            TextView nombre = new TextView(this);
            nombre.setLayoutParams(params);
            nombre.setText(direccions.get(i).getNombre());
            contDir.addView(nombre);

            nombre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogUpdateDireccion(aux);
                }
            });

            TextView dir = new TextView(this);
            dir.setLayoutParams(params2);
            dir.setText(direccions.get(i).getDenominacion());
            dir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogUpdateDireccion(aux);
                }
            });

            contDir.addView(dir);
        }
    }

    public void actualizarUser() {
        usuario = modeloGeneral.consultarUsuarioActivo();

        tF.setText(usuario.getTelfCelular());
        correo.setText(usuario.getCorreo());
        tL.setText(usuario.getTelfLocal());
        Date date = null;
        try {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(usuario.getFechaNac());
            fecha.setText(new SimpleDateFormat("dd-MM-yyyy").format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("Error", "no se pudo la fecga");
        }
        //fecha.setText(usuario.getFechaNac());
        dI.setText(usuario.getIdentificacion() + "");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_icono) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            Snackbar.make(nombre, "Ya estas en este segmento", Snackbar.LENGTH_LONG).show();
        }
        if (id == R.id.nav_pedidos) {
            Intent intent = new Intent(ActivityPerfil.this, ActivityHistorialPedidos.class);
            startActivity(intent);
        }
        if (id == R.id.nav_notificaciones) {
            Intent intent = new Intent(ActivityPerfil.this, ActivityNovedades.class);
            startActivity(intent);
        }
        if (id == R.id.nav_pedidos_solicitar) {
            Intent intent = new Intent(ActivityPerfil.this, ActivityPedido.class);
            startActivity(intent);
        }

        if (id == R.id.nav_pedidos) {
            Intent intent = new Intent(ActivityPerfil.this, ActivityHistorialPedidos.class);
            startActivity(intent);
        }

        else if (id == R.id.nav_logout) {
            modeloGeneral.deleteAll();
            startActivity(new Intent(getBaseContext(), ActivityLogin.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK &&
                validarDatos()) {
            dialogCambiarDatos();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public Boolean validarDatos() {
        if (!tL.getText().toString().equals(usuario.getTelfLocal()) ||
                !tF.getText().toString().equals(usuario.getTelfCelular()) ||
                !correo.getText().toString().equals(usuario.getCorreo()) ||
                !dI.getText().toString().equals(usuario.getIdentificacion() + "") ||
                !fecha.getText().toString().equals(usuario.getFechaNac())) {
            return true;
        }
        else {
            return false;
        }
    }

    public void verificarVistas() {
        if (titulos.size() > 0) {
            empty.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
        }
        else {
            recycler.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
        }
    }

    public void agregarDomicilio(View view) {
        dialogCrearDireccion();
        //Intent intent = new Intent(ActivityPerfil.this, ActivityCrearDireccion.class);
        //startActivity(intent);
    }

    public void edit(View view) {
        tL.setFocusable(true);
        tL.setKeyListener((KeyListener) tL.getTag());

        tF.setFocusable(true);
        tF.setKeyListener((KeyListener) tF.getTag());

        correo.setFocusable(true);
        correo.setKeyListener((KeyListener) correo.getTag());

        fecha.setFocusable(true);
        fecha.setKeyListener((KeyListener) fecha.getTag());

        dI.setFocusable(true);
        dI.setKeyListener((KeyListener) dI.getTag());

        tF.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(tF, InputMethodManager.SHOW_IMPLICIT);

        Toast.makeText(getApplicationContext(), "Ya puedes editar tus datos", Toast.LENGTH_LONG).show();
    }

    public void mostrarCodigoQR(View view) {
        showAlertDialog("Generando codigo QR", false);
    }

    private void dialogConfirmacion() {

        final Dialog ventana = new Dialog(this);

        ventana.setContentView(R.layout.dialog_mensaje_confimacion);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        TextView titulo = (TextView) ventana.findViewById(R.id.dialog_one_accion_titulo);
        TextView contenido = (TextView) ventana.findViewById(R.id.dialog_one_accion_contenido);
        TextView codigo = (TextView) ventana.findViewById(R.id.dialog_one_accion_codigo_qr);
        ImageView qr = (ImageView) ventana.findViewById(R.id.dialog_one_accion_qr);

        titulo.setText("Código QR");
        contenido.setVisibility(View.GONE);

        codigo.setText(usuario.getCodigo());

        try {
            qr.setImageBitmap(Utilidades.generarQR(usuario.getCodigo(), getApplicationContext()));
            dismissAlertDialog();

        } catch (WriterException e) {
            Log.e("WebService", e.toString());
            dismissAlertDialog();
            Toast.makeText(getApplicationContext(), "Hubo un error al intentar generar el QR", Toast.LENGTH_LONG).show();
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
            }
        });

        ventana.show();
    }

    private void dialogCambiarDatos() {

        final Dialog ventana = new Dialog(this);

        ventana.setContentView(R.layout.dialog_confirmacion);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.escanear_qr);
        TextView titulo = (TextView) ventana.findViewById(R.id.titulo);
        TextView no = (TextView) ventana.findViewById(R.id.codigo_pedido);

        titulo.setText("Se encontraron cambios en los datos, ¿desea actualizarlos?");

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parametros.clear();

                parametros.add("id_usuario");
                parametros.add("tx_correo");
                parametros.add("dt_fecha_nacimiento");
                parametros.add("tx_telefono_celular");
                parametros.add("tx_telefono_local");
                parametros.add("nu_identificacion");

                valores.clear();

                valores.add(usuario.getId() + "");
                valores.add(correo.getText().toString());
                valores.add(fecha.getText().toString());
                valores.add(tF.getText().toString());
                valores.add(tL.getText().toString());
                valores.add(dI.getText().toString());

                actualizarUsuario();

                ventana.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
                finish();
            }
        });

        ventana.show();
    }

    private void dialogCrearDireccion() {
        if (ventana == null) {
            ventana = new Dialog(this);
            ventana.setContentView(R.layout.dialog_registrar_direccion);
            ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        final Button aceptar = (Button) ventana.findViewById(R.id.dialog_registrar_direccion_guardar);

        final EditText alias = (EditText) ventana.findViewById(R.id.registrar_nombre);
        final EditText referencia = (EditText) ventana.findViewById(R.id.registrar_referencia);
        final EditText numInt = (EditText) ventana.findViewById(R.id.registrar_num_int);
        final EditText numExt = (EditText) ventana.findViewById(R.id.registrar_num_ext);
        final EditText postal = (EditText) ventana.findViewById(R.id.registrar_postal);
        final EditText colonia = (EditText) ventana.findViewById(R.id.registrar_colonia);
        final EditText municipio = (EditText) ventana.findViewById(R.id.registrar_municipio);
        final EditText telefono = (EditText) ventana.findViewById(R.id.registrar_telefono);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        // Autocomplete Cliente Fisico
        if (autocompleteFragment != null) {
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Ingresa tu domicilio");
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    isLocation = true;
                    domicilio = place.getAddress().toString();
                    lat = place.getLatLng().latitude;
                    lng = place.getLatLng().longitude;
                }

                @Override
                public void onError(Status status) {
                    // TODO: Handle the error.
                    isLocation = false;
                }
            });
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alias.getText().length() > 0) {
                    if (numExt.getText().length() > 0) {
                        if (colonia.getText().length() > 0) {
                            if (municipio.getText().length() > 0) {
                                parametros.clear();

                                //parametros.add("tx_nombre");
                                parametros.add("id_usuario");
                                parametros.add("nu_longitud");
                                parametros.add("nu_latitud");
                                parametros.add("tx_direccion");
                                parametros.add("tx_referencia");
                                parametros.add("tx_codigo_postal");
                                parametros.add("tx_numero_ext");
                                parametros.add("tx_numero_int");
                                parametros.add("tx_colonia");
                                parametros.add("tx_municipio");
                                parametros.add("telefono");

                                valores.clear();

                                //valores.add(alias.getText().toString());
                                valores.add(usuario.getId() + "");
                                valores.add(lng + "");
                                valores.add(lat + "");
                                valores.add(alias.getText().toString());
                                valores.add(referencia.getText().toString());
                                valores.add(postal.getText().toString());
                                valores.add(numExt.getText().toString());
                                valores.add(numInt.getText().toString());
                                valores.add(colonia.getText().toString());
                                valores.add(municipio.getText().toString());
                                valores.add(telefono.getText().toString());

                                isLocation = false;

                                registrarDireccion();
                            /*
                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment fragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.remove(fragment);
                            fragmentTransaction.commit();
                            */
                                ventana.dismiss();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "Debes ingresar el municipio", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Debes ingresar el nombre de la colonia", Toast.LENGTH_LONG).show();
                        }

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "El numero exterior no puede estar vacio", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes ingresar la av. o calle", Toast.LENGTH_LONG).show();
                }
                /*
                if (isLocation) {
                    if (alias.getText().length() > 0) {

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Debes ingresar un alias", Toast.LENGTH_LONG).show();
                    }
                    //Toast.makeText(getApplicationContext(), "Se guardo", Toast.LENGTH_LONG).show();

                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes ingresar una dirección valida", Toast.LENGTH_LONG).show();
                }
                */
            }
        });

        ventana.show();
    }

    private void dialogUpdateDireccion(final Integer id) {
        if (ventana == null) {
            ventana = new Dialog(this);
            ventana.setContentView(R.layout.dialog_registrar_direccion);
            ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        Button aceptar = (Button) ventana.findViewById(R.id.dialog_registrar_direccion_guardar);

        final EditText alias = (EditText) ventana.findViewById(R.id.registrar_nombre);
        alias.setText(direccions.get(id).getNombre());
        final EditText referencia = (EditText) ventana.findViewById(R.id.registrar_referencia);
        referencia.setText(direccions.get(id).getRefencia());
        final EditText numInt = (EditText) ventana.findViewById(R.id.registrar_num_int);
        numInt.setText(direccions.get(id).getNumInt());
        final EditText numExt = (EditText) ventana.findViewById(R.id.registrar_num_ext);
        numExt.setText(direccions.get(id).getNumExt());
        final EditText postal = (EditText) ventana.findViewById(R.id.registrar_postal);
        postal.setText(direccions.get(id).getCodigoPostal());
       final EditText colonia = (EditText) ventana.findViewById(R.id.registrar_colonia);
        colonia.setText(direccions.get(id).getColonia());

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        // Autocomplete Cliente Fisico
        if (autocompleteFragment != null) {
            /*autocompleteFragment = (PlaceAutocompleteFragment)
                    getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);*/

            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                    .build();

            autocompleteFragment.setFilter(typeFilter);

            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Ingresa tu domicilio");
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText(direccions.get(id).getDenominacion());

            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    isLocation = true;
                    domicilio = place.getAddress().toString();
                    lat = place.getLatLng().latitude;
                    lng = place.getLatLng().longitude;
                }

                @Override
                public void onError(Status status) {
                    // TODO: Handle the error.
                    isLocation = false;
                }
            });
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLocation) {
                    if (alias.getText().length() > 0) {
                        if (referencia.getText().length() > 0) {
                            if (postal.getText().length() > 0) {
                                if (postal.getText().length() > 0) {
                                    parametros.clear();

                                    parametros.add("tx_nombre");
                                    parametros.add("id_usuario");
                                    parametros.add("nu_longitud");
                                    parametros.add("nu_latitud");
                                    parametros.add("tx_direccion");
                                    parametros.add("tx_referencia");
                                    parametros.add("id_direccion");
                                    parametros.add("tx_codigo_postal");
                                    parametros.add("tx_numero_ext");
                                    parametros.add("tx_numero_int");
                                    parametros.add("tx_colonia");

                                    valores.clear();

                                    valores.add(alias.getText().toString());
                                    valores.add(usuario.getId() + "");
                                    valores.add(lng + "");
                                    valores.add(lat + "");
                                    valores.add(domicilio);
                                    valores.add(referencia.getText().toString());
                                    valores.add(direccions.get(id).getId() + "");
                                    valores.add(postal.getText().toString());
                                    valores.add(numExt.getText().toString());
                                    valores.add(numInt.getText().toString());
                                    valores.add(postal.getText().toString());

                                    isLocation = false;

                                    actualizarDireccion();
                            /*
                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment fragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.remove(fragment);
                            fragmentTransaction.commit();
                            */
                                    //autocompleteFragment.onDestroyView();
                                    //autocompleteFragment.onDestroy();
                                    //autocompleteFragment = null;
                                    ventana.dismiss();
                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Debes ingresar el nombre de tu colonia", Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "Debes ingresar el codigo postal", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Debes ingresar una referencia", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Debes ingresar un alias", Toast.LENGTH_LONG).show();
                    }
                    //Toast.makeText(getApplicationContext(), "Se guardo", Toast.LENGTH_LONG).show();

                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes ingresar una dirección valida", Toast.LENGTH_LONG).show();
                }
            }
        });

        ventana.show();
    }

    public void registrarDireccion() {
        WebService webService = new WebService(
                parametros,
                valores,
                ActivityPerfil.this,
                "POST",
                "direccion/cliente",
                null,
                "Registrando dirección",
                true
        );
        webService.execute();
    }

    public void actualizarUsuario() {
        WebService webService = new WebService(
                parametros,
                valores,
                ActivityPerfil.this,
                "POST",
                "usuario/update",
                null,
                "Actualizando datos",
                true
        );
        webService.execute();
    }

    public void actualizarDireccion() {
        WebService webService = new WebService(
                parametros,
                valores,
                ActivityPerfil.this,
                "POST",
                "direccion/update",
                null,
                "Actualizando datos",
                true
        );
        webService.execute();
    }

    private void showAlertDialog(String message, boolean isCancelable){
        //Toast.makeText(getApplicationContext(), "Espere generando codigo QR", Toast.LENGTH_LONG).show();
        dialog = Utilidades.buildAlertDialog("Espere", message, isCancelable, ActivityPerfil.this);
        dialog.show();

        dialogConfirmacion();
    }

    private void dismissAlertDialog() {
        dialog.dismiss();
    }
}

