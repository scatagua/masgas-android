package com.masgas.otoni.masgasmovil.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.masgas.otoni.masgasmovil.R;

import java.util.List;

/**
 * Created by otoni on 6/1/2018.
 */

public class AdapterListViewPerfil extends RecyclerView.Adapter<AdapterListViewPerfil.ViewHolderPerfil>
        implements ItemClickListener {

    private List<String> titulos, contenidos;
    private Context context;

    public static class ViewHolderPerfil extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView titulo, contenido;

        // Interfaz de comunicación
        public ItemClickListener listener;

        public ViewHolderPerfil(View v, ItemClickListener listener) {
            super(v);

            titulo = (TextView) v.findViewById(R.id.card_view_datos_perfil_titulo);
            contenido = (TextView) v.findViewById(R.id.card_view_datos_perfil_content);

            v.setOnClickListener(this);

            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }

    public AdapterListViewPerfil(List<String> titulos, List<String> contenidos, Context context) {
        this.titulos = titulos;
        this.contenidos = contenidos;
        this.context = context;
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<String> titulos, List<String> contenidos){
        this.titulos.addAll(titulos);
        this.contenidos.addAll(contenidos);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        titulos.clear();
        contenidos.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public int getItemCount() {
        return titulos.size();
    }

    @Override
    public ViewHolderPerfil onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_datos_perfil, viewGroup, false);
        return new ViewHolderPerfil(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolderPerfil viewHolder, int i) {
        //viewHolder.imagen.setImageResource(items.get(i).getImagen());
        viewHolder.titulo.setText(titulos.get(i));
        viewHolder.contenido.setText(contenidos.get(i));
    }

}

interface ItemClickListener {
    void onItemClick(View view, int position);
}
