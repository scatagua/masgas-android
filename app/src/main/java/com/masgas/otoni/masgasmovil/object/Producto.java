package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;
import android.content.Intent;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 10/1/2018.
 */

public class Producto {

    // Datos de la Tabla
    public static final String TABLE = "producto";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String NOMBRE = "nombre";
    public static final String PRECIO = "precio";
    public static final String CANTIDAD = "cantidad";
    public static final String ID_PRODUCTO_CATEGORIA = "id_producto_categoria";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private String nombre;
    private Double precio;
    private Integer cantidad;
    private Integer idProductoCategoria;

    public Producto() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                NOMBRE + " TEXT, " +
                CANTIDAD + " INTEGER, " +
                PRECIO + " REAL, " +
                ID_PRODUCTO_CATEGORIA + " INTEGER, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {
        Long id_articulo = (Long) pag.get("id_articulo");
        String articulo = (String) pag.get("articulo");
        Long precio = (Long) pag.get("precio");
        Long id_articulo_categoria = (Long) pag.get("id_articulo_categoria");
        String cantidad = (String) pag.get("cantidad");

        this.setId(id_articulo.intValue());
        this.setNombre(articulo);
        this.setPrecio(precio * 1.0);
        this.setCantidad(Integer.parseInt(cantidad));
        this.setIdProductoCategoria(id_articulo_categoria.intValue());
    }

    public ContentValues crearRegistro() {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getNombre() != null) {
            values.put(NOMBRE, this.getNombre());
        }

        if (this.getPrecio() != null) {
            values.put(PRECIO, this.getPrecio());
        }

        if (this.getCantidad() != null) {
            values.put(CANTIDAD, this.getCantidad());
        }

        if (this.getIdProductoCategoria() != null) {
            values.put(ID_PRODUCTO_CATEGORIA, this.getIdProductoCategoria());
        }

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdProductoCategoria() {
        return idProductoCategoria;
    }

    public void setIdProductoCategoria(Integer idProductoCategoria) {
        this.idProductoCategoria = idProductoCategoria;
    }
}
