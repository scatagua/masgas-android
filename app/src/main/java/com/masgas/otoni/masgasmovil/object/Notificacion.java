package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;
import android.content.Intent;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 22/2/2018.
 */

public class Notificacion {

    // Datos de la Tabla
    public static final String TABLE = "notificacion";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String ID_MENSAJE = "id_mensaje";
    public static final String FECHA = "fecha";
    public static final String CUERPO = "cuerpo";
    public static final String ID_USUARIO_EMISOR = "id_usuario_emisor";
    public static final String ID_USUARIO_RECEPTOR = "id_usuario_receptor";
    public static final String NUEVO = "nuevo";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private Integer idMensaje;
    private String fecha;
    private String cuerpo;
    private Integer idUsuarioEmisor;
    private Integer idUsuarioReceptor;

    public Notificacion()
    {
    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ID_MENSAJE + " INTEGER, " +
                FECHA + " TEXT, " +
                ID_USUARIO_RECEPTOR + " INTEGER, " +
                ID_USUARIO_EMISOR + " INTEGER, " +
                CUERPO + " TEXT, " +
                NUEVO + " BOOLEAN, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {

        Long id_notificacion = (Long) pag.get("id_notificacion");
        Long usuario_emisor = (Long) pag.get("usuario_emisor");
        Long usuario_receptor = (Long) pag.get("usuario_receptor");
        String fecha = (String) pag.get("fecha");
        String cuerpo = (String) pag.get("cuerpo");
        Long id_mensaje = (Long) pag.get("id_mensaje");

        this.setId(id_notificacion.intValue());
        this.setIdUsuarioEmisor(usuario_emisor.intValue());
        this.setIdUsuarioReceptor(usuario_receptor.intValue());
        this.setIdMensaje(id_mensaje.intValue());
        this.setFecha(fecha);
        this.setCuerpo(cuerpo);
    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean modificado) {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getFecha() != null) {
            values.put(FECHA, this.getFecha());
        }

        if (this.getIdMensaje() != null) {
            values.put(ID_MENSAJE, this.getIdMensaje());
        }

        if (this.getIdUsuarioEmisor() != null)
        {
            values.put(ID_USUARIO_EMISOR, this.getIdUsuarioEmisor());
        }

        if (this.getIdUsuarioReceptor() != null)
        {
            values.put(ID_USUARIO_RECEPTOR, this.getIdUsuarioReceptor());
        }

        if (this.getCuerpo() != null) {
            values.put(CUERPO, this.getCuerpo());
        }

        values.put(NUEVO, nuevo);
        values.put(MODIFICADO, modificado);

        return values;
    }

    public Integer getIdUsuarioReceptor() {
        return idUsuarioReceptor;
    }

    public void setIdUsuarioReceptor(Integer idUsuarioReceptor) {
        this.idUsuarioReceptor = idUsuarioReceptor;
    }

    public Integer getIdUsuarioEmisor() {
        return idUsuarioEmisor;
    }

    public void setIdUsuarioEmisor(Integer idUsuarioEmisor) {
        this.idUsuarioEmisor = idUsuarioEmisor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
}
