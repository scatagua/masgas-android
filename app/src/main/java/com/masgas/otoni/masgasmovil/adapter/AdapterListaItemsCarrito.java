package com.masgas.otoni.masgasmovil.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.Producto;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.util.List;

/**
 * Created by otoni on 17/1/2018.
 */

public class AdapterListaItemsCarrito extends RecyclerView.Adapter<AdapterListaItemsCarrito.ViewHolderItemCarrito> {

    private List<ViewItemCarrito> items;
    private Context context;
    public ModeloGeneral modeloRelacional;

    public static class ViewHolderItemCarrito extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public ImageView imagen, icono;
        public TextView nombre, contenido, cant;
        public EditText cantidad;

        // Interfaz de comunicación
        public ItemClickListener listener;

        public ViewHolderItemCarrito(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.producto_carrito_eliminar);
            icono = (ImageView) v.findViewById(R.id.imagen);
            nombre = (TextView) v.findViewById(R.id.titulo);
            contenido = (TextView) v.findViewById(R.id.contenido);
            //cantidad = (EditText) v.findViewById(R.id.cantidad);
            cant = (TextView) v.findViewById(R.id.cantidad);

        }

        @Override
        public void onClick(View v) {
            //listener.onItemClick(v, getAdapterPosition());
        }
    }

    public AdapterListaItemsCarrito(List<ViewItemCarrito> items, Context context) {
        this.items = items;
        this.context = context;
        modeloRelacional = new ModeloGeneral(context);
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<ViewItemCarrito> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolderItemCarrito onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_item_carrito, viewGroup, false);
        return new ViewHolderItemCarrito(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolderItemCarrito viewHolder, final int position) {
        viewHolder.nombre.setText(items.get(position).getProducto());
        viewHolder.contenido.setText(Utilidades.doubleToDollar(items.get(position).getPrecioIndependiente()));
        viewHolder.imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloRelacional.deleteItemCarrito(items.get(position).getIdItem());
                items.remove(position);
                notifyDataSetChanged();
            }
        });
        viewHolder.cant.setText(items.get(position).getCantidad() + "");

        if (items.get(position).getIdProductoTipo() == 2) {
            viewHolder.icono.setImageResource(R.drawable.pipa);
        }
        //viewHolder.cantidad.setText(items.get(position).getCantidad() + "");
        /*
        viewHolder.cantidad.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence.length() > 0) {
                            viewHolder.contenido.setText(Utilidades.doubleToDollar(items.get(position).getPrecion() * Integer.parseInt(charSequence.toString())));
                            modeloRelacional.actualizarProductoCarrito(
                                    items.get(position).getIdItem(),
                                    Integer.parseInt(charSequence.toString()),
                                    items.get(position).getPrecion() * Integer.parseInt(charSequence.toString()));
                            //notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        //Toast.makeText(context, "Valor guardado " + editable.toString(), Toast.LENGTH_LONG).show();
                    }
                });
                */
    }
}
