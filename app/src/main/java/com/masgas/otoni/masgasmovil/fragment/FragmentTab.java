package com.masgas.otoni.masgasmovil.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Direccion;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.utils.WebService;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONObject;
/*
import java.mx.openpay.android.Openpay;
import java.mx.openpay.android.OperationCallBack;
import java.mx.openpay.android.OperationResult;
import java.mx.openpay.android.exceptions.OpenpayServiceException;
import java.mx.openpay.android.exceptions.ServiceUnavailableException;
import java.mx.openpay.android.model.Card;
*/
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;

/**
 * Created by otoni on 24/12/2017.
 */

public class FragmentTab extends Fragment {

    View root;
    ModeloGeneral modeloGeneral;

    LinearLayout empty, contenedor;
    private LinearLayout cilindro, estacionario, carburacion, contDet, contInf, contPago;
    private View cilV, estV, carV;
    private TextView cliT, estT, carT, nombre, telefono, correo, montoText;
    private MaterialBetterSpinner direccion;
    private Button button;
    private LinearLayout contentPago, contTranferencia;
    private RadioGroup radio;
    private CheckBox checkbox;
    private TextInputLayout fecha_pago;

    private EditText cvv, numerdoTDC, nombreTDC, anoExp, mesExp, fechaNac;

    List<ViewItemCarrito> productos;
    List<Direccion> direccions;
    List<Item> items;

    Integer pos = 1, idDireccion;
    Usuario usuario;
    Double monto = 0.0;
    List<String> parametros, valores;
    Integer pago = 1;

    String domicilio, llave, fechaNacimiento;
    Double lat, lng;
    Boolean isLocation = false;
    Dialog ventana;
    Thread splashThread;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        root =  inflater.inflate(R.layout.tab_pedido, viewGroup, false);
        modeloGeneral = new ModeloGeneral(getContext());
        cargarVista();

        return root;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (verificar()) {
                poblarVista();
                montoText.setText(Utilidades.doubleToDollar(monto));
            }
        }
    }

    public void cargarVista() {
        if (root != null) {
            empty = (LinearLayout) root.findViewById(R.id.detalle_pedido_contenedor_vacio);
            contenedor = (LinearLayout) root.findViewById(R.id.detalle_pedido_contenedor_lista);
            button = (Button) root.findViewById(R.id.fragment_pedido_content_informacion_pagar);

            contDet = (LinearLayout) root.findViewById(R.id.fragment_pedido_content_detalle);
            contInf = (LinearLayout) root.findViewById(R.id.fragment_pedido_content_informacion);
            contPago = (LinearLayout) root.findViewById(R.id.fragment_pedido_content_pago);

            radio = (RadioGroup) root.findViewById(R.id.radio_group);
            contentPago = (LinearLayout) root.findViewById(R.id.content_pago);
            contTranferencia = (LinearLayout) root.findViewById(R.id.content_transferencia);
            fechaNac = (EditText) root.findViewById(R.id.registrar_fecha_nac);
            fecha_pago = (TextInputLayout) root.findViewById(R.id.fecha_pago);

            contTranferencia.setVisibility(View.GONE);

            fecha_pago.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.pago_efectivo) {
                        contentPago.setVisibility(View.GONE);
                        contTranferencia.setVisibility(View.GONE);
                        pago = 3;
                    }
                    else if (i == R.id.pago_transferencia) {
                        contentPago.setVisibility(View.GONE);
                        contTranferencia.setVisibility(View.VISIBLE);
                        pago = 2;
                    }
                    else if (i == R.id.pago_tarjeta){
                        contentPago.setVisibility(View.VISIBLE);
                        contTranferencia.setVisibility(View.GONE);
                        pago = 1;
                    }
                }
            });

            if (verificar()) {
                poblarVista();
            }

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos++;
                    cambiarVista();
                }
            });

            usuario = modeloGeneral.consultarUsuarioActivo();
            direccions = modeloGeneral.consultarMisDirecciones();

            nombre = (TextView) root.findViewById(R.id.fragment_pedido_content_informacion_nombre);
            nombre.setText(usuario.getNombre1() + " " + usuario.getApellido1());

            telefono = (TextView) root.findViewById(R.id.fragment_pedido_content_informacion_telefono);
            telefono.setText(usuario.getTelfCelular());

            correo = (TextView) root.findViewById(R.id.fragment_pedido_content_informacion_correo);
            correo.setText(usuario.getCorreo());

            checkbox = (CheckBox) root.findViewById(R.id.fragment_pedido_content_informacion_factura);
            checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkbox.isChecked()) {
                        dialogFactura();
                    }
                }
            });

            direccion = (MaterialBetterSpinner) root.findViewById(R.id.fragment_pedido_content_informacion_direccion);

            List<String> valores = new ArrayList<>();
            valores.add("Crear");
            for (int i = 0; i < direccions.size(); i++) {
                valores.add(direccions.get(i).getNombre());
            }

            //direccion.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, valores));
            direccion.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.textview_letter_black, valores));
            direccion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        dialogCrearDireccion();
                    }
                    else {
                        idDireccion = direccions.get(position - 1).getId();
                    }
                }
            });

            cilV = (View) root.findViewById(R.id.fragmente_productos_select_cilindros_view);
            estV = (View) root.findViewById(R.id.fragmente_productos_select_estacionario_view);
            carV = (View) root.findViewById(R.id.fragmente_productos_select_carburacion_view);

            cliT = (TextView) root.findViewById(R.id.fragmente_productos_select_cilindros_text);
            carT = (TextView) root.findViewById(R.id.fragmente_productos_select_carburacion_text);
            estT = (TextView) root.findViewById(R.id.fragmente_productos_select_estacionario_text);

            cilindro = (LinearLayout) root.findViewById(R.id.fragmente_productos_select_cilindros);
            cilindro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = 1;
                    cambiarVista();
                }
            });

            carburacion = (LinearLayout) root.findViewById(R.id.fragmente_productos_select_carburacion);
            carburacion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = 3;
                    cambiarVista();
                }
            });

            estacionario = (LinearLayout) root.findViewById(R.id.fragmente_productos_select_estacionario);
            estacionario.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = 2;
                    cambiarVista();
                }
            });

            montoText = (TextView) root.findViewById(R.id.pago_monto);
            montoText.setText(Utilidades.doubleToDollar(monto));

            parametros = new ArrayList<String>();
            valores = new ArrayList<String>();

            /* Ventana de pago */
            cvv = (EditText) root.findViewById(R.id.pago_cvv);
            mesExp = (EditText) root.findViewById(R.id.pago_exp_mes);
            anoExp = (EditText) root.findViewById(R.id.pago_exp_ano);
            nombreTDC = (EditText) root.findViewById(R.id.pago_propietario);
            numerdoTDC = (EditText) root.findViewById(R.id.pago_numero_tdc);

            Conekta.setPublicKey("key_SLcSoZrBbvvcfobhEBPSHxQ");
            //Conekta.setPublicKey("key_HxKjATrr3QNta8hrCSXyTYw");
            Conekta.setApiVersion("2.0.0");                       //optional
            Conekta.collectDevice(getActivity());

        }
    }
    /*
    public void procesarPagoOpenPay() {
        Openpay openpay = new Openpay("mvzke6g8jx5jr3ruwgxn", "pk_0bf60e2ff7174ec4988fe8983da8748c", false);

        Card card = new Card();
        card.holderName("Juan Perez Ramirez");
        card.cardNumber("4111111111111111");
        card.expirationMonth(12);
        card.expirationYear(20);
        card.cvv2("110");

        openpay.createToken(card, new OperationCallBack() {

            @Override
            public void onSuccess(OperationResult arg0) {
                //Handlo in success

            }

            @Override
            public void onError(OpenpayServiceException arg0) {
                //Handle Error
                arg0.printStackTrace();
                String desc = "";
                switch( arg0.getErrorCode()){
                    case 3001:
                        desc = "Declinado";
                        break;
                    case 3002:
                        desc = "Expiro";
                        break;
                    case 3003:
                        desc = "Fondo insuficiente";
                        break;
                    case 3004:
                        desc = "Tarjeta robada";
                        break;
                    case 3005:
                        desc = "Supuesto fraude";
                        break;

                    case 2002:
                        desc = "Ya existe";
                        break;
                    default:
                        desc = "Error creando tarjeta";
                }

                //DialogFragment fragment = MessageDialogFragment.newInstance(R.string.error, this.getString(desc));
                //fragment.show(this.getFragmentManager(), "Error");
            }

            @Override
            public void onCommunicationError(ServiceUnavailableException arg0) {
                //Handle communication error
            }
        });
    }
    */

    /*
    private void showDatePickerDialog(View view) {

        FragmentDatePicker newFragment = FragmentDatePicker.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate, d, m;
                d = (day < 10) ? "0" + day : day + "";
                m = (month < 9) ? "0" + (month+1) : (month+1) + "";

                fechaNacimiento = year + "-" + m + "-" + d;

                fechaNac.setText(d + "-" + m + "-" + year);
            }
        });
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }
    */
    public void actualizarSpinner() {

        Log.e("Pruebas", direccions.size() + "");
        List<String> valores = new ArrayList<>();
        valores.add("Crear");
        for (int i = 0; i < direccions.size(); i++) {
            valores.add(direccions.get(i).getNombre());
        }

        //direccion.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, valores));
        direccion.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.textview_letter_black, valores));
        direccion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    dialogCrearDireccion();
                }
                else {
                    idDireccion = direccions.get(position - 1).getId();
                }
            }
        });
    }

    public Boolean verificar() {
        productos = modeloGeneral.consultarItemsCarrito();

        if (productos != null) {
            if (productos.size() > 0) {
                empty.setVisibility(View.GONE);
                button.setVisibility(View.VISIBLE);
                contenedor.setVisibility(View.VISIBLE);

                return true;
            }
            else {
                contenedor.setVisibility(View.GONE);
                empty.setVisibility(View.VISIBLE);
                button.setVisibility(View.GONE);
            }
        }
        else {
            contenedor.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
            button.setVisibility(View.GONE);
        }

        return false;
    }

    public void poblarVista() {
        monto = 0.0;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 3);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2);
        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2);

        contDet.removeAllViews();

        LinearLayout ll = new LinearLayout(getContext());

        TextView tv1 = new TextView(getContext());
        tv1.setText("Producto");
        tv1.setTypeface(Typeface.DEFAULT_BOLD);
        tv1.setLayoutParams(params);

        TextView tv2 = new TextView(getContext());
        tv2.setText("Cantidad");
        tv2.setTypeface(Typeface.DEFAULT_BOLD);
        tv2.setLayoutParams(params2);
        tv2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        TextView tv3 = new TextView(getContext());
        tv3.setText("Subtotal");
        tv3.setTypeface(Typeface.DEFAULT_BOLD);
        tv3.setLayoutParams(params3);
        tv3.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);

        ll.addView(tv1);
        ll.addView(tv2);
        ll.addView(tv3);

        contDet.addView(ll);

        for (int i = 0; i < productos.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(getContext());

            TextView textView = new TextView(getContext());
            textView.setText(productos.get(i).getProducto());
            textView.setLayoutParams(params);

            TextView textView2 = new TextView(getContext());
            textView2.setText(productos.get(i).getCantidad() + "");
            textView2.setLayoutParams(params2);
            textView2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            TextView textView3 = new TextView(getContext());
            textView3.setText(Utilidades.doubleToDollar(productos.get(i).getPrecioIndependiente()));
            textView3.setLayoutParams(params3);
            textView3.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);

            linearLayout.addView(textView);
            linearLayout.addView(textView2);
            linearLayout.addView(textView3);

            contDet.addView(linearLayout);

            monto = monto + productos.get(i).getPrecioIndependiente();
        }
    }

    public void cambiarVista() {
        if (pos == 1) {
            contDet.setVisibility(View.VISIBLE);
            contInf.setVisibility(View.GONE);
            contPago.setVisibility(View.GONE);

            button.setText("Realizar Pago");

            cilV.setBackgroundResource(R.color.pumpkin_orange);
            carV.setBackgroundColor(Color.TRANSPARENT);
            estV.setBackgroundColor(Color.TRANSPARENT);

            cliT.setTypeface(Typeface.DEFAULT_BOLD);
            carT.setTypeface(Typeface.DEFAULT);
            estT.setTypeface(Typeface.DEFAULT);
        }
        else if (pos == 2){
            contInf.setVisibility(View.VISIBLE);
            contDet.setVisibility(View.GONE);
            contPago.setVisibility(View.GONE);

            button.setText("confirmar dirección");

            cilV.setBackgroundColor(Color.TRANSPARENT);
            carV.setBackgroundColor(Color.TRANSPARENT);
            estV.setBackgroundResource(R.color.pumpkin_orange);

            cliT.setTypeface(Typeface.DEFAULT);
            carT.setTypeface(Typeface.DEFAULT);
            estT.setTypeface(Typeface.DEFAULT_BOLD);
        }
        else if (pos == 3){
            contPago.setVisibility(View.VISIBLE);
            contDet.setVisibility(View.GONE);
            contInf.setVisibility(View.GONE);

            button.setText("PAGAR");

            cilV.setBackgroundColor(Color.TRANSPARENT);
            carV.setBackgroundResource(R.color.pumpkin_orange);
            estV.setBackgroundColor(Color.TRANSPARENT);

            cliT.setTypeface(Typeface.DEFAULT);
            carT.setTypeface(Typeface.DEFAULT_BOLD);
            estT.setTypeface(Typeface.DEFAULT);
        }
        else if (pos > 3) {
            if (pago == 1) {
                validarTarjeta();
            }
            else {
                enviarPedido();
            }
        }
    }

    public void validarTarjeta() {
        if (nombreTDC.getText().length() > 0) {
            if (numerdoTDC.getText().length() > 0) {
                if (mesExp.getText().length() > 0) {
                    if (anoExp.getText().length() > 0) {
                        if (cvv.getText().length() > 0) {
                            Card card = new Card(
                                    nombreTDC.getText().toString(),
                                    numerdoTDC.getText().toString(),
                                    cvv.getText().toString(),
                                    mesExp.getText().toString(),
                                    anoExp.getText().toString());

                            Token token = new Token(getActivity());

                            token.onCreateTokenListener( new Token.CreateToken(){
                                @Override
                                public void onCreateTokenReady(JSONObject data) {
                                    try {
                                        //Send the id to the webservice.
                                        if (data.getString("object").equals("error")) {
                                            Snackbar.make(root, data.get("message").toString(), Snackbar.LENGTH_LONG).show();
                                        }
                                        else if (data.getString("object").equals("token")) {
                                            llave = data.getString("id");
                                            pagoConekta();
                                        }
                                        else {
                                            Snackbar.make(root, "Problema en la conexión", Snackbar.LENGTH_LONG).show();
                                        }
                                        Log.e("Conekta", data.toString());

                                    } catch (Exception err) {
                                        //Do something on error
                                        Log.e("ConektaError", err.toString());
                                        Snackbar.make(root, "Por favor, verifique los datos de la tarjeta ingresada ya que la misma no logro afiliarse para el pago", Snackbar.LENGTH_LONG).show();
                                    }
                                }
                            });

                            token.create(card);
                        }
                        else {
                            Snackbar.make(root, "Debes ingresar el CVV de la tarjeta, son los ultimos tres digitos al reverso de la misma", Snackbar.LENGTH_LONG).show();
                            cvv.requestFocus();
                        }
                    }
                    else {
                        Snackbar.make(root, "Debes ingresar el año de vencimiento de la tarjeta", Snackbar.LENGTH_LONG).show();
                        anoExp.requestFocus();
                    }
                }
                else {
                    Snackbar.make(root, "Debes ingresar mes de vencimiento de la tarjeta", Snackbar.LENGTH_LONG).show();
                    mesExp.requestFocus();
                }
            }
            else {
                Snackbar.make(root, "Debes ingresar el numero que aparece impreso en la tarjeta", Snackbar.LENGTH_LONG).show();
                numerdoTDC.requestFocus();
            }
        }
        else {
            Snackbar.make(root, "Debes ingresar el nombre del tarjetahabiente", Snackbar.LENGTH_LONG).show();
            nombreTDC.requestFocus();
        }
    }

    public void pagoConekta() {
        if (valores == null) {
            valores = new ArrayList<>();
        }
        valores.clear();

        items = modeloGeneral.consultarItemsCarritoEnvio();
        if (items.size() > 0) {
            if (direccions != null) {
                if (direccions.size() > 0) {
                    valores.add(direccions.get(0).getId() + "");
                }
                else {
                    valores.add("1");
                }
            }
            else {
                valores.add("1");
            }

            if (pago == 3) {
                valores.add("0");
            }
            else {
                valores.add("1");
            }

            valores.add("App");
            if (pago == 3) {
                valores.add("Efectivo");
            }
            else if (pago == 1){
                valores.add("TDC");
            }
            else if (pago == 2){
                valores.add("Transferencia");
            }

            valores.add(llave);
            valores.add(nombreTDC.getText().toString());
            valores.add(usuario.getCorreo());
            valores.add(usuario.getTelfCelular());

            parametros.clear();

            parametros.add("id_direccion");
            parametros.add("bo_pagada");
            parametros.add("departamento_venta");
            parametros.add("tipo_pago");

            parametros.add("token");
            parametros.add("card[nombre]");
            parametros.add("card[email]");
            parametros.add("card[telefono]");

            items = modeloGeneral.consultarItemsCarritoEnvio();
            for (int x = 0; x < items.size(); x++) {
                parametros.add("items[" + x + "][id_articulo]");
                parametros.add("items[" + x + "][nu_cantidad]");
                parametros.add("items[" + x + "][nu_subtotal]");
                parametros.add("items[" + x + "][nu_price_unit]");
                parametros.add("items[" + x + "][tx_nombre]");

                valores.add(items.get(x).getIdProducto() + "");
                valores.add(items.get(x).getCantidad() + "");
                valores.add(items.get(x).getPrecioIndependiente() + "");
                valores.add(items.get(x).getPrecioUnitario() + "");
                valores.add(items.get(x).getNombreProducto() + "");
            }

            WebService webService = new WebService(
                    parametros,
                    valores,
                    getContext(),
                    "POST",
                    "payment",
                    null,
                    "Efectuando Pago",
                    true
            );
            webService.execute();
        }
        else {
            Toast.makeText(getContext(), "Debes de tener al menos un item en el carrito", Toast.LENGTH_LONG).show();
        }
    }

    public void enviarPedido() {
        if (valores == null) {
            valores = new ArrayList<>();
        }
        valores.clear();

        items = modeloGeneral.consultarItemsCarritoEnvio();
        if (items.size() > 0) {
            if (direccions != null) {
                if (direccions.size() > 0) {
                    valores.add(direccions.get(0).getId() + "");
                }
                else {
                    valores.add("1");
                }
            }
            else {
                valores.add("1");
            }

            if (pago == 3) {
                valores.add("0");
            }
            else {
                valores.add("1");
            }

            valores.add("App");

            if (pago == 3) {
                valores.add("Efectivo");
            }
            else if (pago == 1){
                valores.add("TDC");
            }
            else if (pago == 2){
                valores.add("Transferencia");
            }
                valores.add(usuario.getCodigo());
            parametros.clear();

            parametros.add("id_direccion");
            parametros.add("bo_pagada");
            parametros.add("departamento_venta");
            parametros.add("tipo_pago");
            parametros.add("codigo");

            for (int x = 0; x < items.size(); x++) {
                parametros.add("items[" + x + "][id_articulo]");
                parametros.add("items[" + x + "][nu_cantidad]");
                parametros.add("items[" + x + "][nu_subtotal]");

                valores.add(items.get(x).getIdProducto() + "");
                valores.add(items.get(x).getCantidad() + "");
                valores.add(items.get(x).getPrecioIndependiente() + "");
            }

            //procesarPagoOpenPay();

            WebService webService = new WebService(
                    parametros,
                    valores,
                    getContext(),
                    "POST",
                    "pedido",
                    null,
                    "Registrando pedido",
                    true
            );
            webService.execute();

        }
        else {
            Toast.makeText(getContext(), "Debes de tener al menos un item en el carrito", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            //Toast.makeText(getContext(), "Prueba 1", Toast.LENGTH_LONG).show();
            button.setVisibility(View.GONE);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            //Toast.makeText(getContext(), "Prueba 2", Toast.LENGTH_LONG).show();
            button.setVisibility(View.VISIBLE);
        }
    }

    private void dialogCrearDireccion() {

        if (ventana == null){
            ventana = new Dialog(getContext());
            ventana.setContentView(R.layout.dialog_registrar_direccion);
            ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        Button aceptar = (Button) ventana.findViewById(R.id.dialog_registrar_direccion_guardar);

        final EditText alias = (EditText) ventana.findViewById(R.id.registrar_nombre);
        final EditText referencia = (EditText) ventana.findViewById(R.id.registrar_referencia);
        final EditText numInt = (EditText) ventana.findViewById(R.id.registrar_num_int);
        final EditText numExt = (EditText) ventana.findViewById(R.id.registrar_num_ext);
        final EditText postal = (EditText) ventana.findViewById(R.id.registrar_postal);
        final EditText colonia = (EditText) ventana.findViewById(R.id.registrar_colonia);
        final EditText municipio = (EditText) ventana.findViewById(R.id.registrar_municipio);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        // Autocomplete Cliente Fisico
        if (autocompleteFragment != null) {

            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Registra tu ubicación con google map");
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    isLocation = true;
                    domicilio = place.getAddress().toString();
                    lat = place.getLatLng().latitude;
                    lng = place.getLatLng().longitude;
                }

                @Override
                public void onError(Status status) {
                    // TODO: Handle the error.
                    isLocation = false;
                }
            });
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLocation) {
                    if (alias.getText().length() > 0) {
                        if (referencia.getText().length() > 0) {
                            if (postal.getText().length() > 0) {
                                if (colonia.getText().length() > 0) {
                                    parametros.clear();

                                    parametros.add("tx_nombre");
                                    parametros.add("id_usuario");
                                    parametros.add("nu_longitud");
                                    parametros.add("nu_latitud");
                                    parametros.add("tx_direccion");
                                    parametros.add("tx_referencia");
                                    parametros.add("tx_codigo_postal");
                                    parametros.add("tx_numero_ext");
                                    parametros.add("tx_numero_int");
                                    parametros.add("tx_colonia");
                                    parametros.add("tx_municipio");

                                    if (valores == null) {
                                        valores = new ArrayList<>();
                                    }

                                    valores.clear();

                                    valores.add(alias.getText().toString());
                                    valores.add(usuario.getId() + "");
                                    valores.add(lng + "");
                                    valores.add(lat + "");
                                    valores.add(domicilio);
                                    valores.add(referencia.getText().toString());
                                    valores.add(postal.getText().toString());
                                    valores.add(numExt.getText().toString());
                                    valores.add(numInt.getText().toString());
                                    valores.add(colonia.getText().toString());
                                    valores.add(municipio.getText().toString());

                                    isLocation = false;

                                    registrarDireccion();
                            /*
                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment fragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.remove(fragment);
                            fragmentTransaction.commit();
                            */
                                    ventana.dismiss();
                                }
                                else {
                                    Toast.makeText(getContext(), "Debes ingresar el nombre de la colonia", Toast.LENGTH_LONG).show();
                                }

                            }
                            else {
                                Toast.makeText(getContext(), "Debes ingresar el codigo postal", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            Toast.makeText(getContext(), "Debes ingresar una referencia", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(getContext(), "Debes ingresar un alias", Toast.LENGTH_LONG).show();
                    }
                    //Toast.makeText(getApplicationContext(), "Se guardo", Toast.LENGTH_LONG).show();

                }
                else {
                    Toast.makeText(getContext(), "Debes ingresar una dirección valida", Toast.LENGTH_LONG).show();
                }
            }
        });

        ventana.show();
    }

    private void dialogFactura() {
//
//        if (ventana == null) {
            ventana = new Dialog(getContext());
            ventana.setContentView(R.layout.dialog_factura);
            ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ventana.setCancelable(false);
      //  }

        Button aceptar = (Button) ventana.findViewById(R.id.dialog_registrar_direccion_guardar);
        Button cancelar = (Button) ventana.findViewById(R.id.dialog_registrar_direccion_cancelar);

        final EditText alias = (EditText) ventana.findViewById(R.id.registrar_nombre);
        final EditText referencia = (EditText) ventana.findViewById(R.id.registrar_referencia);


        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkbox.setChecked(false);
                ventana.dismiss();
            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alias.getText().length() > 0) {
                    if (referencia.getText().length() > 0) {
                        Toast.makeText(getContext(), "Perfecto se emitira la factura segun los datos indicados", Toast.LENGTH_LONG).show();
                        ventana.dismiss();
                    }
                    else {
                        Toast.makeText(getContext(), "Debes ingresar el RFC", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getContext(), "Debes ingresar la razón social", Toast.LENGTH_LONG).show();
                }
            }
        });

        ventana.show();
    }

    public void registrarDireccion() {
        WebService webService = new WebService(
                parametros,
                valores,
                getContext(),
                "POST",
                "direccion/cliente",
                null,
                "Registrando dirección",
                false
        );
        try {
            if (webService.execute().get() == 0) {
                Direccion producto = new Direccion();
                producto.registrarJson((org.json.simple.JSONObject) ((org.json.simple.JSONArray)  webService.dataJson.get("direccion")).get(0));
                direccions = modeloGeneral.consultarMisDirecciones();
                direccions.add(producto);
                actualizarSpinner();
                //Log.e("Preuabs", webService.dataJson.toJSONString());
            }
            Log.e("Pruebas", webService.getStatus() +"");
        }
        catch (ExecutionException exc) {
            Log.e("Pruebas", exc.toString());
        }
        catch (InterruptedException exc) {
            Log.e("Pruebas", exc.toString());
        }

    }
}

