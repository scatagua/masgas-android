package com.masgas.otoni.masgasmovil.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.masgas.otoni.masgasmovil.object.Direccion;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.MotivoNoEntrega;
import com.masgas.otoni.masgasmovil.object.Notificacion;
import com.masgas.otoni.masgasmovil.object.NotificacionMensaje;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.object.Producto;
import com.masgas.otoni.masgasmovil.object.ProductoCategoria;
import com.masgas.otoni.masgasmovil.object.RelNotificacionRol;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.view.ViewBuzonNotificaciones;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otoni on 12/11/2017.
 */

public class ModeloGeneral extends SQLiteOpenHelper {

    // Datos de la BD
    protected static final String DB_NAME = "masgas.db";
    protected static final Integer DB_VERSION = 2;

    protected String query = "";

    public ModeloGeneral(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(new Usuario().queryCrearTabla());
        db.execSQL(new Producto().queryCrearTabla());
        db.execSQL(new Pedido().queryCrearTabla());
        db.execSQL(new Item().queryCrearTabla());
        db.execSQL(new ProductoCategoria().queryCrearTabla());
        db.execSQL(new Direccion().queryCrearTabla());
        db.execSQL(new Notificacion().queryCrearTabla());
        db.execSQL(new NotificacionMensaje().queryCrearTabla());
        db.execSQL(new RelNotificacionRol().queryCrearTabla());
        db.execSQL(new MotivoNoEntrega().queryCrearTabla());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Usuario.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Producto.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Pedido.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Item.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ProductoCategoria.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Direccion.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Notificacion.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + NotificacionMensaje.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + RelNotificacionRol.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + MotivoNoEntrega.TABLE);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    public void insertar(String table, ContentValues values) {
        SQLiteDatabase insert = this.getWritableDatabase();

        insert.insert(table, null, values);
        insert.close();
    }

    public Boolean hayDatos(String tabla) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery("SELECT * FROM " + tabla, null);

        if (cursor.moveToFirst()) {
            return true;
        }
        else {
            return false;
        }
    }

    public Boolean isLogin() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery("SELECT * FROM " + Usuario.TABLE + " WHERE " + Usuario.LOGIN + " = 1", null);

        if (cursor.moveToFirst()) {
            return true;
        }
        else {
            return false;
        }
    }

    public Boolean isUserDistribuidor() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery("SELECT * FROM " + Usuario.TABLE + " WHERE " + Usuario.ID_ROL + " = 2", null);

        if (cursor.moveToFirst()) {
            return true;
        }
        else {
            return false;
        }
    }

    public Boolean existeProductoEnCarrito(Integer idProducto) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery("SELECT * FROM " + Item.TABLE + " " +
                "WHERE " + Item.ID_PRODUCTO + " = " + idProducto, null);

        if (cursor.moveToFirst()) {
            return true;
        }
        else {
            return false;
        }
    }

    public List<Direccion> consultarMisDirecciones() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * " +
                        "FROM " + Direccion.TABLE + "  "
                , null);

        List<Direccion> objetos = new ArrayList<Direccion>();

        if (cursor.moveToFirst()) {
            do {
                Direccion nuevo = new Direccion();

                nuevo.setId(cursor.getInt(0));
                nuevo.setNombre(cursor.getString(1));
                nuevo.setLat(cursor.getDouble(2));
                nuevo.setLng(cursor.getDouble(3));
                nuevo.setDenominacion(cursor.getString(4));
                nuevo.setRefencia(cursor.getString(5));
                nuevo.setCodigoPostal(cursor.getString(6));
                nuevo.setColonia(cursor.getString(7));
                nuevo.setNumExt(cursor.getString(8));
                nuevo.setNumInt(cursor.getString(9));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<MotivoNoEntrega> consultarMotivos() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * " +
                        "FROM " + MotivoNoEntrega.TABLE + "  "
                , null);

        List<MotivoNoEntrega> objetos = new ArrayList<MotivoNoEntrega>();

        if (cursor.moveToFirst()) {
            do {
                MotivoNoEntrega nuevo = new MotivoNoEntrega();

                nuevo.setId(cursor.getInt(0));
                nuevo.setNombre(cursor.getString(1));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<Item> consultarItemsCarritoEnvio() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT i." + Item.ID + ", i." + Item.ID_PEDIDO + ", i." + Item.ID_PRODUCTO + ", i." + Item.CANTIDAD + "," +
                        "i." + Item.PRECIO_INDEPENDIENTE + ", p." + Producto.NOMBRE + ", p." + Producto.PRECIO +  " " +
                        "FROM " + Item.TABLE + " AS i " +
                        "JOIN " + Producto.TABLE + " AS p ON i." + Item.ID_PRODUCTO + " = p." + Producto.ID + " " +
                        "WHERE i." + Item.NUEVO + " = 1"
                , null);

        List<Item> objetos = new ArrayList<Item>();

        if (cursor.moveToFirst()) {
            do {
                Item nuevo = new Item();

                nuevo.setId(cursor.getInt(0));
                nuevo.setIdPedido(cursor.getInt(1));
                nuevo.setIdProducto(cursor.getInt(2));
                nuevo.setCantidad(cursor.getInt(3));
                nuevo.setPrecioIndependiente(cursor.getDouble(4));
                nuevo.setNombreProducto(cursor.getString(5));
                nuevo.setPrecioUnitario(cursor.getDouble(6));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<ViewItemCarrito> consultarItemsPorPedidoConProducto(Integer id) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT p." + Producto.ID + ", p." + Producto.NOMBRE + ", p." + Producto.PRECIO +  ", p." + Producto.CANTIDAD + ", " +
                        "i." + Item.CANTIDAD + ", i." + Item.ID + ", i." + Item.PRECIO_INDEPENDIENTE + " " +
                        "FROM " + Item.TABLE + " AS i " +
                        "JOIN " + Producto.TABLE + " AS p ON i." + Item.ID_PRODUCTO + " = p." + Producto.ID + " " +
                        "WHERE i." + Item.ID_PEDIDO + " = " + id
                , null);

        List<ViewItemCarrito> objetos = new ArrayList<ViewItemCarrito>();

        if (cursor.moveToFirst()) {
            do {
                ViewItemCarrito nuevo = new ViewItemCarrito();

                nuevo.setIdProducto(cursor.getInt(0));
                nuevo.setProducto(cursor.getString(1));
                nuevo.setPrecion(cursor.getDouble(2));
                nuevo.setInventario(cursor.getInt(3));
                nuevo.setCantidad(cursor.getInt(4));
                nuevo.setIdItem(cursor.getInt(5));
                nuevo.setPrecioIndependiente(cursor.getDouble(6));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<Producto> consultarProductos() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT p." + Producto.ID + ", p." + Producto.NOMBRE + ", p." + Producto.PRECIO +  ", " +
                "p." + Producto.CANTIDAD + " " +
                "FROM " + Producto.TABLE + " AS p "
                , null);

        List<Producto> objetos = new ArrayList<Producto>();

        if (cursor.moveToFirst()) {
            do {
                Producto nuevo = new Producto();

                nuevo.setId(cursor.getInt(0));
                nuevo.setNombre(cursor.getString(1));
                nuevo.setPrecio(cursor.getDouble(2));
                nuevo.setCantidad(cursor.getInt(3));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<Producto> consultarProductosPorCategoria(Integer idCategoria) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT p." + Producto.ID + ", p." + Producto.NOMBRE + ", p." + Producto.PRECIO +  ", " +
                        "p." + Producto.CANTIDAD + ", p." + Producto.ID_PRODUCTO_CATEGORIA + " " +
                        "FROM " + Producto.TABLE + " AS p " +
                        "WHERE " + Producto.ID_PRODUCTO_CATEGORIA + " = " + idCategoria
                , null);

        List<Producto> objetos = new ArrayList<Producto>();

        if (cursor.moveToFirst()) {
            do {
                Producto nuevo = new Producto();

                nuevo.setId(cursor.getInt(0));
                nuevo.setNombre(cursor.getString(1));
                nuevo.setPrecio(cursor.getDouble(2));
                nuevo.setCantidad(cursor.getInt(3));
                nuevo.setIdProductoCategoria(cursor.getInt(4));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<ViewItemCarrito> consultarItemsCarrito() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT p." + Producto.ID + ", p." + Producto.NOMBRE + ", p." + Producto.PRECIO +  ", p." + Producto.CANTIDAD + ", " +
                        "i." + Item.CANTIDAD + ", i." + Item.ID + ", i." + Item.PRECIO_INDEPENDIENTE + ", " + Producto.ID_PRODUCTO_CATEGORIA + " " +
                        "FROM " + Item.TABLE + " AS i " +
                        "JOIN " + Producto.TABLE + " AS p ON i." + Item.ID_PRODUCTO + " = p." + Producto.ID + " " +
                        "WHERE i." + Item.NUEVO + " = 1"
                , null);

        List<ViewItemCarrito> objetos = new ArrayList<ViewItemCarrito>();

        if (cursor.moveToFirst()) {
            do {
                ViewItemCarrito nuevo = new ViewItemCarrito();

                nuevo.setIdProducto(cursor.getInt(0));
                nuevo.setProducto(cursor.getString(1));
                nuevo.setPrecion(cursor.getDouble(2));
                nuevo.setInventario(cursor.getInt(3));
                nuevo.setCantidad(cursor.getInt(4));
                nuevo.setIdItem(cursor.getInt(5));
                nuevo.setPrecioIndependiente(cursor.getDouble(6));
                nuevo.setIdProductoTipo(cursor.getInt(7));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<ViewBuzonNotificaciones> consultarMensajesEntrada(Integer idUsuario) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT n." + Notificacion.ID + ", nm." + NotificacionMensaje.MENSAJE + ", n." + Notificacion.FECHA + ", n." + Notificacion.CUERPO + " " +
                        "FROM " + Notificacion.TABLE + " AS n " +
                        "JOIN " + NotificacionMensaje.TABLE + " AS nm ON n." + Notificacion.ID_MENSAJE + " = nm." + NotificacionMensaje.ID + " " +
                        "WHERE n." + Notificacion.ID_USUARIO_RECEPTOR + " = " + idUsuario
                , null);

        List<ViewBuzonNotificaciones> objetos = new ArrayList<ViewBuzonNotificaciones>();

        if (cursor.moveToFirst()) {
            do {
                ViewBuzonNotificaciones nuevo = new ViewBuzonNotificaciones();

                nuevo.setIdNotificacion(cursor.getInt(0));
                nuevo.setMensaje(cursor.getString(1));
                nuevo.setFecha(cursor.getString(2));
                nuevo.setCuerpo(cursor.getString(3));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<ViewBuzonNotificaciones> consultarMensajesSalida(Integer idUsuario) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT n." + Notificacion.ID + ", nm." + NotificacionMensaje.MENSAJE + ", n." + Notificacion.FECHA + ", n." + Notificacion.CUERPO + " " +
                        "FROM " + Notificacion.TABLE + " AS n " +
                        "JOIN " + NotificacionMensaje.TABLE + " AS nm ON n." + Notificacion.ID_MENSAJE + " = nm." + NotificacionMensaje.ID + " " +
                        "WHERE n." + Notificacion.ID_USUARIO_EMISOR + " = " + idUsuario
                , null);

        List<ViewBuzonNotificaciones> objetos = new ArrayList<ViewBuzonNotificaciones>();

        if (cursor.moveToFirst()) {
            do {
                ViewBuzonNotificaciones nuevo = new ViewBuzonNotificaciones();

                nuevo.setIdNotificacion(cursor.getInt(0));
                nuevo.setMensaje(cursor.getString(1));
                nuevo.setFecha(cursor.getString(2));
                nuevo.setCuerpo(cursor.getString(3));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<NotificacionMensaje> consultarMensajesParaEnviar(Integer idUsuario) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT nm." + NotificacionMensaje.ID + ", nm." + NotificacionMensaje.MENSAJE + " " +
                        "FROM " + RelNotificacionRol.TABLE + " AS rnr " +
                        "JOIN " + NotificacionMensaje.TABLE + " AS nm ON nm." + NotificacionMensaje.ID + " = rnr." + RelNotificacionRol.ID_MENSAJE + " " +
                        "WHERE rnr." + RelNotificacionRol.ID_ROL + " = " + idUsuario
                , null);

        List<NotificacionMensaje> objetos = new ArrayList<NotificacionMensaje>();

        if (cursor.moveToFirst()) {
            do {
                NotificacionMensaje nuevo = new NotificacionMensaje();

                nuevo.setId(cursor.getInt(0));
                nuevo.setMensaje(cursor.getString(1));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public Double montoPedido(Integer idProducto) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery("SELECT sum(" + Item.PRECIO_INDEPENDIENTE + ") FROM " + Item.TABLE + " " +
                "WHERE " + Item.ID_PEDIDO + " = " + idProducto, null);

        if (cursor.moveToFirst()) {
            return cursor.getDouble(0);
        }
        else {
            return 0.0;
        }
    }

    public List<ViewItemCarrito> consultarItemsCarrito2() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * FROM " + Item.TABLE
                , null);

        List<ViewItemCarrito> objetos = new ArrayList<ViewItemCarrito>();

        if (cursor.moveToFirst()) {
            do {
                ViewItemCarrito nuevo = new ViewItemCarrito();

                //Log.e("CarritoDB", cursor.getInt(5) + "");
                nuevo.setIdItem(cursor.getInt(0));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public List<Pedido> consultarPedidos() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT p.* " +
                        "FROM " + Pedido.TABLE + " AS p " +
                        "WHERE p." + Pedido.NUEVO + " = 0"
                , null);

        List<Pedido> objetos = new ArrayList<Pedido>();

        if (cursor.moveToFirst()) {
            do {
                Pedido nuevo = new Pedido();

                nuevo.setId(cursor.getInt(0));
                nuevo.setIdDireccion(cursor.getInt(1));
                nuevo.setFecha(cursor.getString(2));
                nuevo.setCodigo(cursor.getString(3));
                if (cursor.getInt(4) == 1) {
                    nuevo.setPagado(true);
                }
                else {
                    nuevo.setPagado(false);
                }
                nuevo.setEstatus(cursor.getString(5));

                objetos.add(nuevo);
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public Item consultarProductoEnCarrito(Integer idProducto) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * FROM " + Item.TABLE + " " +
                     "WHERE " + Item.ID_PRODUCTO + " = " + idProducto + " AND " + Item.NUEVO + " = 1",
                null);

        Item objetos = null;

        if (cursor.moveToFirst()) {
            do {
                objetos = new Item();

                objetos.setId(cursor.getInt(0));
                objetos.setIdProducto(cursor.getInt(2));
                objetos.setCantidad(cursor.getInt(3));
                objetos.setPrecioIndependiente(cursor.getDouble(4));
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public Producto consultarPipa() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * FROM " + Producto.TABLE + " " +
                        "WHERE " + Producto.ID + " = 6 ",
                null);

        Producto objetos = null;

        if (cursor.moveToFirst()) {
            do {
                objetos = new Producto();

                objetos.setId(cursor.getInt(0));
                objetos.setNombre(cursor.getString(1));
                objetos.setCantidad(cursor.getInt(2));
                objetos.setPrecio(cursor.getDouble(3));
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public Direccion consultarDireccion(Integer idProducto) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * FROM " + Direccion.TABLE + " " +
                        "WHERE " + Direccion.ID + " = " + idProducto + " ",
                null);

        Direccion objetos = null;

        if (cursor.moveToFirst()) {
            do {
                objetos = new Direccion();

                objetos.setId(cursor.getInt(0));
                objetos.setNombre(cursor.getString(1));
                objetos.setLat(cursor.getDouble(2));
                objetos.setLng(cursor.getDouble(3));
                objetos.setDenominacion(cursor.getString(4));
                objetos.setRefencia(cursor.getString(5));
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public Pedido consultarPedido(Integer idProducto) {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery(
                "SELECT * FROM " + Pedido.TABLE + " " +
                        "WHERE " + Pedido.ID + " = " + idProducto + " ",
                null);

        Pedido objetos = null;

        if (cursor.moveToFirst()) {
            do {
                objetos = new Pedido();

                objetos.setId(cursor.getInt(0));
                objetos.setIdDireccion(cursor.getInt(1));
                objetos.setFecha(cursor.getString(2));
                objetos.setCodigo(cursor.getString(3));
            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public Usuario consultarUsuarioActivo() {
        SQLiteDatabase consulta = this.getReadableDatabase();
        Cursor cursor = consulta.rawQuery("SELECT * FROM " + Usuario.TABLE + " " +
                "WHERE " + Usuario.LOGIN + " = 1", null);

        Usuario objetos = null;

        if (cursor.moveToFirst()) {
            do {
                objetos = new Usuario();

                objetos.setId(cursor.getInt(1));
                objetos.setIdRol(cursor.getInt(2));
                objetos.setNombre1(cursor.getString(4));
                objetos.setApellido1(cursor.getString(6));
                objetos.setIdentificacion(cursor.getString(8));
                objetos.setTelfLocal(cursor.getString(9));
                objetos.setTelfCelular(cursor.getString(10));
                objetos.setCorreo(cursor.getString(12));
                objetos.setFechaNac(cursor.getString(14));
                objetos.setCodigo(cursor.getString(15));

            }
            while (cursor.moveToNext());
        }

        return objetos;
    }

    public void actualizarProductoCarrito(Integer idItem, Integer cantidad, Double precio) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Item.CANTIDAD, cantidad);
        values.put(Item.PRECIO_INDEPENDIENTE, precio);
        values.put(Item.MODIFICADO, true);

        db.update(Item.TABLE, values, Item.ID + " = " + idItem, null);
        db.close();
    }

    public void actualizarEstatusPedido(Integer idItem, String status) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Pedido.ESTATUS, status);

        db.update(Pedido.TABLE, values, Pedido.ID + " = " + idItem, null);
        db.close();
    }

    public void actualizarUsuario(Usuario status) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Usuario.CORREO, status.getCorreo());
        values.put(Usuario.TELEFONO_LOCAL, status.getTelfLocal());
        values.put(Usuario.TELEFONO_CELULAR, status.getTelfCelular());
        values.put(Usuario.NUMERO_IDENTIFICACION, status.getIdentificacion());
        values.put(Usuario.FECHA_NACIMIENTO, status.getFechaNac());

        db.update(Usuario.TABLE, values, Usuario.ID + " = " + status.getId(), null);
        db.close();
    }

    public void actualizarDireccion(Direccion status) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Direccion.REFERENCIA, status.getRefencia());
        values.put(Direccion.DIRECCION, status.getDenominacion());
        values.put(Direccion.LONGITUD, status.getLng());
        values.put(Direccion.LATITUD, status.getLat());
        values.put(Direccion.NOMBRE, status.getNombre());

        db.update(Direccion.TABLE, values, Direccion.ID + " = " + status.getId(), null);
        db.close();
    }

    public Boolean deleteItemCarrito(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Item.TABLE, Item.ID + " = " + id, null);

        db.close();
        return true;
    }

    public Boolean deleteItemNuevo() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Item.TABLE, Item.NUEVO + " = 1", null);

        db.close();
        return true;
    }

    public Boolean deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Usuario.TABLE, null, null);
        db.delete(Producto.TABLE, null, null);
        db.delete(Pedido.TABLE, null, null);
        db.delete(Item.TABLE, null, null);
        db.delete(ProductoCategoria.TABLE, null, null);
        db.delete(Direccion.TABLE, null, null);
        db.delete(Notificacion.TABLE, null, null);
        db.delete(NotificacionMensaje.TABLE, null, null);
        db.delete(RelNotificacionRol.TABLE, null, null);
        db.delete(MotivoNoEntrega.TABLE, null, null);

        db.close();
        return true;
    }

    public Boolean deleteTable(String table) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(table, null, null);

        db.close();
        return true;
    }
}