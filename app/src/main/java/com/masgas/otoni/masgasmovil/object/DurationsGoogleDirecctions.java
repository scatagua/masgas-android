package com.masgas.otoni.masgasmovil.object;

/**
 * Created by otoni on 23/2/2018.
 */

public class DurationsGoogleDirecctions {

    public String text;
    public int value;

    public DurationsGoogleDirecctions(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
