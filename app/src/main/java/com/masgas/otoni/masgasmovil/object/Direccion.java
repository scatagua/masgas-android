package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 26/1/2018.
 */

public class Direccion {

    // Datos de la Tabla
    public static final String TABLE = "direccion";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String NOMBRE = "nombre";
    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";
    public static final String DIRECCION = "direccion";
    public static final String REFERENCIA = "referencia";
    public static final String POSTAL = "postal";
    public static final String COLONIA = "colonia";
    public static final String NUM_EXT = "numero_ext";
    public static final String NUM_INT = "numero_int";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private String nombre;
    private String denominacion;
    private String refencia;
    private String codigoPostal;
    private String numExt;
    private String numInt;
    private String colonia;
    private Double lat;
    private Double lng;

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                NOMBRE + " TEXT, " +
                LATITUD + " REAL, " +
                LONGITUD + " REAL, " +
                DIRECCION + " TEXT, " +
                REFERENCIA + " TEXT, " +
                POSTAL + " TEXT, " +
                COLONIA + " TEXT, " +
                NUM_EXT + " TEXT, " +
                NUM_INT + " TEXT, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {
        Long id_articulo_categoria = (Long) pag.get("id_direccion");
        String categoria = (String) pag.get("direccion");
        String denominacion = (String) pag.get("denominacion");
        String referencia = (String) pag.get("referencia");
        String colonia = (String) pag.get("colonia");
        String num_int = (String) pag.get("num_int");
        String num_ext = (String) pag.get("num_ext");
        String codigo_postal = (String) pag.get("codigo_postal");
        Double lat = Double.parseDouble(pag.get("latitud").toString());
        Double lng = Double.parseDouble(pag.get("longitud").toString());

        this.setId(id_articulo_categoria.intValue());
        this.setNombre(categoria);
        this.setLat(lat);
        this.setLng(lng);
        this.setDenominacion(denominacion);
        this.setRefencia(referencia);
        this.setCodigoPostal(codigo_postal);
        this.setColonia(colonia);
        this.setNumExt(num_ext);
        this.setNumInt(num_int);

    }

    public ContentValues crearRegistro() {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getNombre() != null) {
            values.put(NOMBRE, this.getNombre());
        }

        if (this.getLat() != null) {
            values.put(LATITUD, this.getLat());
        }

        if (this.getLng() != null) {
            values.put(LONGITUD, this.getLng());
        }

        if (this.getDenominacion() != null) {
            values.put(DIRECCION, this.getDenominacion());
        }

        if (this.getRefencia() != null) {
            values.put(REFERENCIA, this.getRefencia());
        }

        if (this.getCodigoPostal() != null) {
            values.put(POSTAL, this.getCodigoPostal());
        }

        if (this.getColonia() != null) {
            values.put(COLONIA, this.getColonia());
        }

        if (this.getNumExt() != null) {
            values.put(NUM_EXT, this.getNumExt());
        }

        if (this.getNumInt() != null) {
            values.put(NUM_INT, this.getNumInt());
        }

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public String getRefencia() {
        return refencia;
    }

    public void setRefencia(String refencia) {
        this.refencia = refencia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getNumExt() {
        return numExt;
    }

    public void setNumExt(String numExt) {
        this.numExt = numExt;
    }

    public String getNumInt() {
        return numInt;
    }

    public void setNumInt(String numInt) {
        this.numInt = numInt;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }
}

