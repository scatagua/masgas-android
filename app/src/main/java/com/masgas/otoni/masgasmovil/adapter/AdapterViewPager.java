package com.masgas.otoni.masgasmovil.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.masgas.otoni.masgasmovil.fragment.FragmentCarrito;
import com.masgas.otoni.masgasmovil.fragment.FragmentProductos;
import com.masgas.otoni.masgasmovil.fragment.FragmentTab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by otoni on 24/12/2017.
 */

public class AdapterViewPager extends FragmentPagerAdapter {

    //public FragmentManager fragmentManager;
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public AdapterViewPager(FragmentManager fragmentManager) {
        super(fragmentManager);
        //this.fragmentManager = fragmentManager;
        //mFragmentTags = new HashMap<Integer, Integer>();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
        /*
        if (position == 0) {
            return new FragmentProductos();
        }
        else if (position == 1) {
            return new FragmentCarrito();
        }
        else if (position == 2) {
            return new FragmentTab();
        }
        else {
            return null;
        }
        */
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
    /*
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            Integer tag = fragment.getId();
            mFragmentTags.put(position, tag);
        }
        return object;
    }

    public Fragment getFragment(int position) {
        Fragment fragment = null;
        Integer tag = mFragmentTags.get(position);
        if (tag != null) {
            fragment = fragmentManager.findFragmentById(tag);
        }
        return fragment;
    }
    */
}
