package com.masgas.otoni.masgasmovil.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterViewPager;
import com.masgas.otoni.masgasmovil.fragment.FragmentCarrito;
import com.masgas.otoni.masgasmovil.fragment.FragmentProductos;
import com.masgas.otoni.masgasmovil.fragment.FragmentTab;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Usuario;

/**
 * Created by otoni on 24/12/2017.
 */

public class ActivityPedido extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AdapterViewPager adapter;
    private NavigationView navigationView;
    TextView nombre;

    ModeloGeneral modeloGeneral;

    //FloatingActionButton floatingActionButton;
    Integer optButton = 0;

    Usuario usuario;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_menu_pedidos);

        modeloGeneral = new ModeloGeneral(this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        //adapterViewPager = new AdapterViewPager(getSupportFragmentManager());
        //viewPager.setAdapter(adapterViewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        //floatingActionButton = (FloatingActionButton) findViewById(R.id.map_marker_validar);

        // Menu Lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.menu_navegacion_menu_principal_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        usuario = modeloGeneral.consultarUsuarioActivo();

        View hview = navigationView.getHeaderView(0);
        nombre = (TextView) hview.findViewById(R.id.menu_lateral_nombre);
        nombre.setText(usuario.getNombre1() + " " + usuario.getApellido1());

        setTitle("Pedidos");

        //tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.colorPrimary));
        //tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.w));
        /*
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position){
                    case 0:
                        home.setIcon(R.drawable.list_product);
                        inbox.setIcon(R.drawable.carrito);
                        star.setIcon(R.drawable.pago);

                        //tabLayout.setupWithViewPager(viewPager);
                        break;
                    case 1:
                        viewPager.getAdapter().notifyDataSetChanged();
                        home.setIcon(R.drawable.list_product);
                        inbox.setIcon(R.drawable.carrito);
                        star.setIcon(R.drawable.pago);

                        //tabLayout.setupWithViewPager(viewPager);
                        break;
                    case 2:
                        viewPager.getAdapter().notifyDataSetChanged();
                        home.setIcon(R.drawable.list_product);
                        inbox.setIcon(R.drawable.carrito);
                        star.setIcon(R.drawable.pago);

                        //viewPager.setCurrentItem(position);
                        //tabLayout.setupWithViewPager(viewPager);

                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        */

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.list_product);
        tabLayout.getTabAt(1).setIcon(R.drawable.carrito);
        tabLayout.getTabAt(2).setIcon(R.drawable.pago);
    }

    /*
    public void changeView(View view) {
        if (optButton == 0) {
            viewPager.setCurrentItem(1, true);
            floatingActionButton.setImageResource(R.drawable.pago);
            optButton = 1;
        }
        else if (optButton == 1) {
            viewPager.setCurrentItem(2, true);
            floatingActionButton.setImageResource(R.drawable.list_product);
            optButton = 2;
        }
        else if (optButton == 2) {
            viewPager.setCurrentItem(0, true);
            floatingActionButton.setImageResource(R.drawable.carrito);
            optButton = 0;
        }
    }
    */

    public void changeView(Integer num) {
        viewPager.setCurrentItem(num, true);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new AdapterViewPager(getSupportFragmentManager());
        adapter.addFrag(new FragmentProductos(), "Productos");
        adapter.addFrag(new FragmentCarrito(), "Carrito");
        adapter.addFrag(new FragmentTab(), "Pago");
        viewPager.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_icono) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            Intent intent = new Intent(ActivityPedido.this, ActivityPerfil.class);
            startActivity(intent);
        }
        if (id == R.id.nav_pedidos_solicitar) {
            Snackbar.make(tabLayout, "Ya estas en este segmento", Snackbar.LENGTH_LONG).show();
        }
        if (id == R.id.nav_pedidos) {
            Intent intent = new Intent(ActivityPedido.this, ActivityHistorialPedidos.class);
            startActivity(intent);
        }
        if (id == R.id.nav_notificaciones) {
            Intent intent = new Intent(ActivityPedido.this, ActivityNovedades.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_logout) {
            modeloGeneral.deleteAll();
            startActivity(new Intent(getBaseContext(), ActivityLogin.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
