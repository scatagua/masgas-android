package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 22/2/2018.
 */

public class RelNotificacionRol {

    // Datos de la Tabla
    public static final String TABLE = "rel_not_men_rol";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String ID_MENSAJE = "id_mensaje";
    public static final String ID_ROL = "ir_rol";
    public static final String NUEVO = "nuevo";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private Integer idMensaje;
    private Integer idRol;

    public RelNotificacionRol() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ID_MENSAJE + " INTEGER, " +
                ID_ROL + " INTEGER, " +
                NUEVO + " BOOLEAN, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {

        Long id = (Long) pag.get("id");
        Long id_notificacion_mensaje = (Long) pag.get("id_notificacion_mensaje");
        Long id_rol = (Long) pag.get("id_rol");

        this.setId(id.intValue());
        this.setIdMensaje(id_notificacion_mensaje.intValue());
        this.setIdRol(id_rol.intValue());
    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean modificado) {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getIdMensaje() != null) {
            values.put(ID_MENSAJE, this.getIdMensaje());
        }

        if (this.getIdRol() != null) {
            values.put(ID_ROL, this.getIdRol());
        }

        values.put(NUEVO, nuevo);
        values.put(MODIFICADO, modificado);

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }
}
