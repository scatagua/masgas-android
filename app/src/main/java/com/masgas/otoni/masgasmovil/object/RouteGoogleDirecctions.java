package com.masgas.otoni.masgasmovil.object;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by otoni on 23/2/2018.
 */

public class RouteGoogleDirecctions {

    public DistanceGoogleDirecctions distance;
    public DurationsGoogleDirecctions duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}
