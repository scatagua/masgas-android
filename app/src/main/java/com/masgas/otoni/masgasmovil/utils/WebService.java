package com.masgas.otoni.masgasmovil.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.activity.ActivityGestionPedidos;
import com.masgas.otoni.masgasmovil.activity.ActivityLogin;
import com.masgas.otoni.masgasmovil.activity.ActivityNovedades;
import com.masgas.otoni.masgasmovil.activity.ActivityPedido;
import com.masgas.otoni.masgasmovil.activity.ActivityPerfil;
import com.masgas.otoni.masgasmovil.activity.ActivityRegistrar;
import com.masgas.otoni.masgasmovil.fragment.FragmentTab;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Direccion;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.MotivoNoEntrega;
import com.masgas.otoni.masgasmovil.object.Notificacion;
import com.masgas.otoni.masgasmovil.object.NotificacionMensaje;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.object.Producto;
import com.masgas.otoni.masgasmovil.object.ProductoCategoria;
import com.masgas.otoni.masgasmovil.object.RelNotificacionRol;
import com.masgas.otoni.masgasmovil.object.Usuario;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * Created by otoni on 13/11/2017.
 */
public class WebService extends AsyncTask<Vector, Integer, Integer> {

    private Context context;
    private Class<?> contextNext;
    private AlertDialog dialog;
    private List<String> parametros, valores;
    private String metodo, endPoint;
    private ModeloGeneral modeloGeneral;
    private String mensajeEspera, mensajeRespuesta;
    private Boolean canPrevFinisih, isComplete;
    public JSONObject dataJson;
    //private String urlString = "http://66.228.48.131/api/v1/";
    private String urlString = "http://66.228.32.39/api/v1/";
    //private String urlString = "http://192.168.69.103/MasGasApi/public/api/v1/";
    //private String urlString = "http://192.168.69.125/api_masgas/public/api/v1/";

    public WebService(
            List<String> parametros,
            List<String> valores,
            Context context,
            String metodo,
            String endPoint,
            Class<?> contextNext,
            String mensajeEspera,
            Boolean canPrevFinisih
    ) {
        this.parametros = parametros;
        this.valores = valores;
        this.context = context;
        this.metodo = metodo;
        this.endPoint = endPoint;
        this.contextNext = contextNext;
        this.mensajeEspera = mensajeEspera;
        this.canPrevFinisih = canPrevFinisih;

        this.isComplete = false;
        this.modeloGeneral = new ModeloGeneral(context);
    }

    @Override
    protected void onPreExecute() {
        //dialog.show();
        showAlertDialog(mensajeEspera, false);
        Log.e("WebService", "Iniciado el servicio");
    }

    @Override
    protected Integer doInBackground(Vector... var) {

        try {

            URL url = new URL(urlString + endPoint);

            Log.e("Ruta", urlString + endPoint);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod(metodo);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            connection.connect();

            String charset = "UTF-8";
            String s = "";

            Log.e("Arrays", parametros.size() + " " + valores.size());
            for (int i = 0; i < parametros.size(); i++) {
                if (i == 0) {
                    s += parametros.get(i) + "=" + URLEncoder.encode(valores.get(i), charset);
                }
                else {
                    s += "&" + parametros.get(i) + "=" + URLEncoder.encode(valores.get(i), charset);
                }
            }
            //String s = "tx_alias=" + URLEncoder.encode("otonielmax", charset);
            //s += "&tx_password=" + URLEncoder.encode("123", charset);

            Log.e("URL", s);
            /*
            connection.setFixedLengthStreamingMode(s.getBytes().length);
            PrintWriter out = new PrintWriter(connection.getOutputStream());
            out.print(s);
            out.close();
            */

            OutputStream os = connection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

            osw.write(s);
            osw.flush();
            osw.close();

            Log.e("Conectando", "...");

            if (connection.getResponseCode() != 200) {
                throw new RuntimeException("Failed: Http error code " + connection.getResponseCode());
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String output2;

            JSONParser parser = new JSONParser();

            Log.e("Recibiendo", "...");

            while ((output2 = bufferedReader.readLine()) != null) {
//                Log.e("WebService", "Retorno " + output2);
                if (output2.contains("status")) {
                    Object object = parser.parse(output2);
                    JSONObject jsonObject = (JSONObject) object;

                    mensajeRespuesta = (String) jsonObject.get("mensaje");
                    isComplete = (Boolean) jsonObject.get("status");

                    if (isComplete) {
                        dataJson = (JSONObject) jsonObject.get("data");

                        if (endPoint.equals("login")) {
                            modeloGeneral.deleteAll();

                            // Almacenar usuario logeado
                            Usuario usuario = new Usuario();
                            //usuario.setCorreo(valores.get(0));
                            usuario.registrarJson((JSONObject) ((JSONArray) dataJson.get("usuario")).get(0));
                            modeloGeneral.insertar(usuario.TABLE, usuario.crearRegistro(false, false, true));

                            JSONArray articulos = (JSONArray) dataJson.get("articulos");

                            for (int i = 0; i < articulos.size(); i++) {
                                Producto producto = new Producto();
                                producto.registrarJson((JSONObject) articulos.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro());
                            }

                            JSONArray articulosCategoria = (JSONArray) dataJson.get("articulosCategoria");

                            for (int i = 0; i < articulosCategoria.size(); i++) {
                                ProductoCategoria producto = new ProductoCategoria();
                                producto.registrarJson((JSONObject) articulosCategoria.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro());
                            }

                            JSONArray direccion = (JSONArray) dataJson.get("direccion");

                            for (int i = 0; i < direccion.size(); i++) {
                                Direccion producto = new Direccion();
                                producto.registrarJson((JSONObject) direccion.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro());
                            }

                            JSONArray pedido = (JSONArray) dataJson.get("pedidos");

                            for (int i = 0; i < pedido.size(); i++) {
                                Pedido producto = new Pedido();
                                producto.registrarJson((JSONObject) pedido.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }

                            JSONArray items = (JSONArray) dataJson.get("items");

                            for (int i = 0; i < items.size(); i++) {
                                Item producto = new Item();
                                producto.registrarJson((JSONObject) items.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }

                            JSONArray mensajes = (JSONArray) dataJson.get("mensajes");

                            for (int i = 0; i < mensajes.size(); i++) {
                                Log.e("notification object",mensajes.get(i)+"");
                                NotificacionMensaje producto = new NotificacionMensaje();
                                producto.registrarJson((JSONObject) mensajes.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }

                            JSONArray notificacion = (JSONArray) dataJson.get("notificacion");

                            for (int i = 0; i < notificacion.size(); i++) {
                                Notificacion producto = new Notificacion();
                                producto.registrarJson((JSONObject) notificacion.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }

                            JSONArray relNotRol = (JSONArray) dataJson.get("relNotRol");

                            for (int i = 0; i < relNotRol.size(); i++) {
                                RelNotificacionRol producto = new RelNotificacionRol();
                                producto.registrarJson((JSONObject) relNotRol.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }

                            if (dataJson.containsKey("clientes")) {
                                JSONArray clientes = (JSONArray) dataJson.get("clientes");

                                for (int i = 0; i < clientes.size(); i++) {
                                    Usuario producto = new Usuario();
                                    producto.registrarJson((JSONObject) clientes.get(i));
                                    modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false, false));
                                }
                            }

                            if (dataJson.containsKey("motivosNoEntrega")) {
                                JSONArray motivosNoEntrega = (JSONArray) dataJson.get("motivosNoEntrega");

                                for (int i = 0; i < motivosNoEntrega.size(); i++) {
                                    MotivoNoEntrega producto = new MotivoNoEntrega();
                                    producto.registrarJson((JSONObject) motivosNoEntrega.get(i));
                                    modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                                }
                            }
                        }
                        else if (endPoint.equals("direccion/cliente")) {
                            JSONArray direccion = (JSONArray) dataJson.get("direccion");

                            for (int i = 0; i < direccion.size(); i++) {
                                Direccion producto = new Direccion();
                                producto.registrarJson((JSONObject) direccion.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro());
                            }
                        }
                        else if (endPoint.equals("direccion/update")) {
                            JSONArray direccion = (JSONArray) dataJson.get("direccion");

                            for (int i = 0; i < direccion.size(); i++) {
                                Direccion producto = new Direccion();
                                producto.registrarJson((JSONObject) direccion.get(i));
                                modeloGeneral.actualizarDireccion(producto);
                            }
                        }
                        else if (endPoint.equals("pedido")) {
                            modeloGeneral.deleteItemNuevo();

                            JSONArray pedido = (JSONArray) dataJson.get("pedidos");

                            for (int i = 0; i < pedido.size(); i++) {
                                Pedido producto = new Pedido();
                                producto.registrarJson((JSONObject) pedido.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }

                            JSONArray items = (JSONArray) dataJson.get("items");

                            for (int i = 0; i < items.size(); i++) {
                                Item producto = new Item();
                                producto.registrarJson((JSONObject) items.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }
                        }
                        else if (endPoint.equals("pedidos_status")) {
                            modeloGeneral.actualizarEstatusPedido(Integer.parseInt(valores.get(0)), valores.get(1));
                        }
                        else if (endPoint.equals("notificacion")) {
                            JSONArray notificacion = (JSONArray) dataJson.get("notificacion");

                            modeloGeneral.deleteTable(Notificacion.TABLE);

                            for (int i = 0; i < notificacion.size(); i++) {
                                Notificacion producto = new Notificacion();
                                producto.registrarJson((JSONObject) notificacion.get(i));
                                modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                            }
                        }
                        else if (endPoint.equals("usuario/update")) {
                            JSONObject usuario = (JSONObject) dataJson.get("usuario");

                            Usuario producto = new Usuario();
                            producto.registrarJson(usuario);
                            modeloGeneral.actualizarUsuario(producto);
                        }
                        else if (endPoint.equals("payment")) {
                            JSONObject order = (JSONObject) dataJson.get("order");
                            JSONObject pedido = (JSONObject) dataJson.get("pedido");

                            if (((String) order.get("payment_status")).equals("paid")) {
                                modeloGeneral.deleteItemNuevo();

                                JSONObject data = (JSONObject) pedido.get("data");

                                JSONArray pedidos = (JSONArray) data.get("pedidos");

                                for (int i = 0; i < pedidos.size(); i++) {
                                    Pedido producto = new Pedido();
                                    producto.registrarJson((JSONObject) pedidos.get(i));
                                    modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                                }

                                JSONArray items = (JSONArray) data.get("items");

                                for (int i = 0; i < items.size(); i++) {
                                    Item producto = new Item();
                                    producto.registrarJson((JSONObject) items.get(i));
                                    modeloGeneral.insertar(producto.TABLE, producto.crearRegistro(false, false));
                                }
                            }
                        }
                    }
                    else {
                        //mensajeRespuesta = "Error: " + jsonObject.get("errors").toString();
                        if (jsonObject.get("errors") != null) {
//                            Log.e("WebServices", jsonObject.get("errors").toString());
                            dataJson = jsonObject;
                        }

                        /*
                        JSONArray array = (JSONArray) jsonObject.get("errors");
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject object1 = (JSONObject) array.get(i);
                        }
                        */
                    }
                }

            }
            connection.disconnect();

            return 0;

        }
        catch (Exception exc) {
            Log.e("Error", exc.toString());
            //Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_SHORT).show();
            return 4;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... progreso) {

    }

    @Override
    protected void onPostExecute(Integer num) {
        if (num == 0) {
            if (isComplete) {
                if (endPoint.equals("login")) {
                    Usuario usuario = new Usuario();
                    usuario.registrarJson((JSONObject) ((JSONArray) dataJson.get("usuario")).get(0));

                    if (usuario.getIdRol() == 2) {
                        Intent intent = new Intent(context, ActivityGestionPedidos.class);
                        context.startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(context, contextNext);
                        context.startActivity(intent);
                    }
                }
                else if (endPoint.equals("cliente") || endPoint.equals("cliente_moral")) {
                    dialogConfirmacion();
                }
                else if (endPoint.equals("direccion/cliente")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                }
                else if (endPoint.equals("direccion/update")) {
                    ((ActivityPerfil) context).onResume();

                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                }
                else if (endPoint.equals("pedido")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                    ((ActivityPedido) context).changeView(0);
                }
                else if (endPoint.equals("pedidos_status")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                }
                else if (endPoint.equals("usuario/recuperar_password")) {
                    onDialogRecueperarClave();
                }
                else if (endPoint.equals("usuario/nuevo_password")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                }
                else if (endPoint.equals("notificacion")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                }
                else if (endPoint.equals("usuario/update")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                    ((ActivityPerfil) context).onResume();
                    ((Activity) context).finish();
                }
                else if (endPoint.equals("payment")) {
                    Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
                    //JSONObject customer = (JSONObject) dataJson.get("customer");
                    JSONObject order = (JSONObject) dataJson.get("order");
                    JSONObject pedido = (JSONObject) dataJson.get("pedido");

                    if (((String) order.get("payment_status")).equals("paid")) {
                        ((ActivityPedido) context).changeView(0);
                    }
                }
            }
            else {
                if (dataJson != null && dataJson.containsKey("errors")) {
                    JSONObject array = (JSONObject) dataJson.get("errors");
                    mensajeRespuesta = "";
                    for(Iterator iterator = array.keySet().iterator(); iterator.hasNext();) {
                        String key = (String) iterator.next();
                        String replace = array.get(key).toString().replace("[\"", "");
                        replace = replace.replace("\"]", "");
                        mensajeRespuesta = mensajeRespuesta.concat(replace);
                        //System.out.println(object1.get(key));
                    }
                    /*
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject object1 = (JSONObject) array.get(i);
                        for(Iterator iterator = object1.keySet().iterator(); iterator.hasNext();) {
                            String key = (String) iterator.next();
                            mensajeRespuesta = mensajeRespuesta.concat(object1.get(key).toString()) + ". ";
                            //System.out.println(object1.get(key));
                        }
                    }
                    */
                }

                Toast.makeText(context, mensajeRespuesta, Toast.LENGTH_LONG).show();
            }
        }
        else if (num == 1) {
            Toast.makeText(context, "Los datos ingresados no corresponden a ningun usuario", Toast.LENGTH_LONG).show();
        }
        else if (num == 2) {
            Toast.makeText(context, "El usuario se encuentra eliminado del sistema", Toast.LENGTH_LONG).show();
        }
        else if (num == 3) {
            Toast.makeText(context, "El usuario no se encuentra activo", Toast.LENGTH_LONG).show();
        }
        else if (num == 4) {
            Toast.makeText(context, "Error en el procesamiento de datos", Toast.LENGTH_LONG).show();
        }

        dismissAlertDialog();
        //super.onPostExecute(num);
    }

    private void showAlertDialog(String message, boolean isCancelable){
        dialog = Utilidades.buildAlertDialog("Verificando",message,isCancelable, context);
        dialog.show();
    }

    private void dismissAlertDialog() {
        dialog.dismiss();
    }

    public void onDialogRecueperarClave() {
        final Dialog ventana = new Dialog(context);
        ventana.setContentView(R.layout.dialog_recuperar_validar);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final JSONObject obj = (JSONObject) ((JSONArray) dataJson.get("usuario")).get(0);

        final EditText correo = (EditText) ventana.findViewById(R.id.dialog_fecha);

        TextView title = (TextView) ventana.findViewById(R.id.text_tile);

        //final EditText identificacion = (EditText) ventana.findViewById(R.id.dialog_identificacion);
        final Integer id_rol = obj.get("id_rol") != null ? ((Long) obj.get("id_rol")).intValue() : 0;

        if (id_rol == 5) {
            title.setText("Por favor ingrese su RFC");
            correo.setHint("Ingresa aquí su RFC");
            correo.setInputType(InputType.TYPE_CLASS_TEXT);
        }

        TextView scanner = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (correo.getText().length() > 0) {
                    if (id_rol == 5) {
                        String fecha = (String) obj.get("nu_identificacion");

                        if (correo.getText().toString().equals(fecha)) {
                            ventana.dismiss();
                            onDialogCambiarClave();
                        }
                        else {
                            Toast.makeText(context, "El RFC ingresado no corresponde al resgistrado", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Date date = null;
                        String fecha = "";
                        try {
                            date = new SimpleDateFormat("dd-MM-yyyy").parse((String) obj.get("dt_fecha_nacimiento"));
                            fecha = new SimpleDateFormat("dd-MM-yyyy").format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.e("Error", "no se pudo la fecha");
                        }

                        if (correo.getText().toString().equals(fecha)) {
                            ventana.dismiss();
                            onDialogCambiarClave();
                        /*
                        if (identificacion.getText().length() > 0) {
                            if (Integer.parseInt(identificacion.getText().toString()) == ((Long) obj.get("nu_identificacion")).intValue()) {
                                ventana.dismiss();
                                onDialogCambiarClave();
                            }
                            else {
                                Toast.makeText(context, "El número de identificación no corresponde al registrado", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            Toast.makeText(context, "Debes ingresar tu numero de identificación", Toast.LENGTH_LONG).show();
                        }
                        */
                        }
                        else {
                            Toast.makeText(context, "La fecha de nacimiento no corresponde a la registrada", Toast.LENGTH_LONG).show();
                        }
                    }

                }
                else {
                    if (id_rol == 5) {
                        Toast.makeText(context, "Debes ingresar tu RFC", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(context, "Debes ingresar tu fecha de nacimiento", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        ventana.show();
    }

    public void onDialogCambiarClave() {
        final Dialog ventana = new Dialog(context);
        ventana.setContentView(R.layout.dialog_cambiar_clave);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final JSONObject obj = (JSONObject) ((JSONArray) dataJson.get("usuario")).get(0);

        final EditText c1 = (EditText) ventana.findViewById(R.id.dialog_c1);

        final EditText c2 = (EditText) ventana.findViewById(R.id.dialog_c2);

        TextView scanner = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (c1.getText().length() > 0) {
                    if (Utilidades.isPasswordValid(c1.getText().toString())) {
                        if (c2.getText().length() > 0) {
                            if (Utilidades.isPasswordValid(c2.getText().toString())) {
                                if (c1.getText().toString().equals(c2.getText().toString())) {
                                    parametros.clear();
                                    parametros.add("password");
                                    parametros.add("id_usuario");

                                    valores.clear();
                                    valores.add(c1.getText().toString());
                                    valores.add(((Long) obj.get("id")).intValue() + "");

                                    new WebService(
                                            parametros,
                                            valores,
                                            context,
                                            "POST",
                                            "usuario/nuevo_password",
                                            ActivityPedido.class,
                                            "Cambiando contraseña",
                                            false
                                    ).execute();

                                    ventana.dismiss();
                                }
                                else {
                                    Toast.makeText(context, "Las contraseñas deben ser iguales", Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                Toast.makeText(context, "La contraseña debe tener entre 6 a 8 caracteres", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {
                            Toast.makeText(context, "Debes ingresar la confirmación de contraseña", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(context, "La contraseña debe tener entre 6 a 8 caracteres", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(context, "Debes ingresar la contraseña", Toast.LENGTH_LONG).show();
                }
            }
        });

        ventana.show();
    }

    private void dialogConfirmacion() {
        final Dialog ventana = new Dialog(context);

        ventana.setContentView(R.layout.dialog_mensaje_confimacion);
        ventana.setCancelable(false);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        TextView codigo = (TextView) ventana.findViewById(R.id.dialog_one_accion_codigo_qr);
        ImageView qr = (ImageView) ventana.findViewById(R.id.dialog_one_accion_qr);

        if (dataJson != null) {
            codigo.setText((String) dataJson.get("codigo"));
        }

        try {
            if (dataJson != null) {
                qr.setImageBitmap(Utilidades.generarQR((String) dataJson.get("codigo"), context));
            }
            else {
                qr.setImageBitmap(Utilidades.generarQR("Prueba", context));
            }

        } catch (WriterException e) {
            Log.e("WebService", e.toString());
            Toast.makeText(context, "Hubo un error al intentar generar el QR", Toast.LENGTH_LONG).show();
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();

                Intent intent = new Intent(context, contextNext);
                context.startActivity(intent);

                ((Activity) context).finish();
            }
        });

        ventana.show();
    }
}


