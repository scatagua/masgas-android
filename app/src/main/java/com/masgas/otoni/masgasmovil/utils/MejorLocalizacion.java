package com.masgas.otoni.masgasmovil.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by otoni on 23/2/2018.
 */

public class MejorLocalizacion implements LocationListener {
    private Context context;
    LocationManager locationManager;
    String proveedor;
    private boolean networkOn, gpsOn, isDialog = false;
    AlertDialog alert;
    Location location;

    public MejorLocalizacion(Context context) {
        this.context = context;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        proveedor = LocationManager.NETWORK_PROVIDER;

        ejecutarProceso();
    }
    /*
    public void solicitarPermisosGPSyNetwork() {

    }
    */
    public boolean getGPSOn() {
        return this.gpsOn;
    }

    public void ejecutarProceso() {
        actualizarStatusProveedor();
        updateLocation();
    }

    private void updateLocation() {
        if (isNetworkOn()) {
            try {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            catch (SecurityException exc) {
                Log.v("Ubicacion", exc.toString());
            }

        }
        else if (isGpsOn()) {
            gpsOn = true;
            try {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            catch (SecurityException exc) {
                Log.v("Ubicacion", exc.toString());
            }

        }
        else {
            gpsOn = false;
            if (!isDialog) {
                AlertNoGps();
            }
        }
    }

    public void actualizarStatusProveedor() {
        if (isNetworkOn()) {
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
            }
            catch (SecurityException exc) {
                Log.e("Ubicacion", exc.toString());
            }
        }
        else {
            Log.e("Red", "No hay conexion");
        }
        if (!isGpsOn()) {
            if (!isDialog) {
                AlertNoGps();
            }
            gpsOn = false;
        }
        else {
            gpsOn = true;
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
            }
            catch (SecurityException exc) {
                Log.e("Ubicacion", exc.toString());
            }
        }
    }

    public Boolean isNetworkOn() {
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public Boolean isGpsOn() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public Location getLocation() {
        if (isNetworkOn()) {
            try {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    return location;
                }
            }
            catch (SecurityException exc) {
                Log.v("Ubicacion", exc.toString());
            }

        }
        else if (isGpsOn()) {
            gpsOn = true;
            try {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    return location;
                }
            }
            catch (SecurityException exc) {
                Log.v("Ubicacion", exc.toString());
            }

        }
        else {
            if (!isDialog) {
                AlertNoGps();
            }
            gpsOn = false;
        }

        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void AlertNoGps() {
        isDialog = true;
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("El GPS esta desactivado se requiere que este activo para el correcto funcionamiento de la aplicación, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        isDialog = false;
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        isDialog = false;
                    }
                });
        alert = builder.create();
        alert.show();
    }
}
