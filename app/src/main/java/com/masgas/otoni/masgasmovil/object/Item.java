package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 17/1/2018.
 */

public class Item {

    // Datos de la Tabla
    public static final String TABLE = "item";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String ID_PRODUCTO = "id_producto";
    public static final String ID_PEDIDO = "id_pedido";
    public static final String CANTIDAD = "codigo";
    public static final String PRECIO_INDEPENDIENTE = "precio_independiente";
    public static final String NUEVO = "nuevo";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private Integer idProducto;
    private Integer idPedido;
    private Integer cantidad;
    private Double precioIndependiente;
    private Double precioUnitario;
    private String nombreProducto;

    public Item() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ID_PEDIDO + " INTEGER, " +
                ID_PRODUCTO + " INTEGER, " +
                CANTIDAD + " INTEGER, " +
                PRECIO_INDEPENDIENTE + " REAL, " +
                NUEVO + " BOOLEAN, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {

        Long id_pedido_grupo = (Long) pag.get("id_pedido_grupo");
        Long id_item = (Long) pag.get("id_item");
        Long id_producto = (Long) pag.get("id_producto");
        Long cantidad = (Long) pag.get("cantidad");
        Long subtotal = (Long) pag.get("subtotal");

        this.setId(id_item.intValue());
        this.setIdPedido(id_pedido_grupo.intValue());
        this.setIdProducto(id_producto.intValue());
        this.setPrecioIndependiente(subtotal * 1.0);
        this.setCantidad(cantidad.intValue());

    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean modificado) {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getCantidad() != null) {
            values.put(CANTIDAD, this.getCantidad());
        }

        if (this.getIdPedido() != null) {
            values.put(ID_PEDIDO, this.getIdPedido());
        }

        if (this.getIdProducto() != null) {
            values.put(ID_PRODUCTO, this.getIdProducto());
        }

        if (this.getPrecioIndependiente() != null) {
            values.put(PRECIO_INDEPENDIENTE, this.getPrecioIndependiente());
        }

        values.put(NUEVO, nuevo);
        values.put(MODIFICADO, modificado);

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioIndependiente() {
        return precioIndependiente;
    }

    public void setPrecioIndependiente(Double precioIndependiente) {
        this.precioIndependiente = precioIndependiente;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
}
