package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;
import android.util.Log;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 6/11/2017.
 */

public class Usuario {

    // Datos de la Tabla
    public static final String TABLE = "usuario";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String ID_ROL = "id_rol";
    public static final String ID_TIPO_IDENTIFICACION = "id_tipo_identificacion";
    public static final String NUMERO_IDENTIFICACION = "numero_identificacion";
    public static final String NOMBRE1 = "nombre1";
    public static final String NOMBRE2 = "nombre2";
    public static final String APELLIDO1 = "apellido1";
    public static final String APELLIDO2 = "apellido2";
    public static final String SEXO = "sexo";
    public static final String TELEFONO_LOCAL = "telefono_local";
    public static final String TELEFONO_CELULAR = "telefono_celular";
    public static final String FECHA_NACIMIENTO = "fecha_nac";
    public static final String CORREO = "correo";
    public static final String ALIAS = "alias";
    public static final String CODIGO = "codigo";
    public static final String ID_USUARIO = "id_usuario";
    public static final String MODIFICADO = "modificado";
    public static final String NUEVO = "nuevo";
    public static final String LOGIN = "login";

    private Integer id;
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private String identificacion;
    private String sexo;
    private String fechaNac;
    private Integer idRol;
    private String telfCelular;
    private String telfLocal;
    private String correo;
    private String codigo;

    public Usuario() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ID_USUARIO + " INTEGER," +
                ID_ROL + " INTEGER, " +
                ID_TIPO_IDENTIFICACION + " INTEGER, " +
                NOMBRE1 + " TEXT, " +
                NOMBRE2 + " TEXT, " +
                APELLIDO1 + " TEXT, " +
                APELLIDO2 + " TEXT, " +
                NUMERO_IDENTIFICACION + " TEXT, " +
                TELEFONO_LOCAL + " TEXT, " +
                TELEFONO_CELULAR + " TEXT, " +
                SEXO + " TEXT, " +
                CORREO + " TEXT, " +
                ALIAS + " TEXT, " +
                FECHA_NACIMIENTO + " TEXT, " +
                CODIGO + " TEXT, " +
                MODIFICADO + " BOOLEAN, " +
                NUEVO + " BOOLEAN, " +
                LOGIN + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {
        Long id_rol = (Long) pag.get("id_rol");
        Long id;
        if (pag.containsKey("id_usuario")) {
            id = (Long) pag.get("id_usuario");
        }
        else {
            id = (Long) pag.get("id");
        }
        String nombre;
        if (pag.containsKey("nombre1")) {
            nombre = (String) pag.get("nombre1");
        }
        else {
            nombre = (String) pag.get("tx_nombre1");
        }
        String apellido1;
        if (pag.containsKey("apellido1")) {
            apellido1 = (String) pag.get("apellido1");
        }
        else {
            apellido1 = (String) pag.get("tx_apellido1");
        }
        String fecha_nacimiento;
        if (pag.containsKey("fecha_nacimiento")) {
            fecha_nacimiento = (String) pag.get("fecha_nacimiento");
        }
        else {
            fecha_nacimiento = (String) pag.get("dt_fecha_nacimiento");
        }
        String telefono_celular;
        if (pag.containsKey("telefono_celular")) {
            telefono_celular = (String) pag.get("telefono_celular");
        }
        else {
            telefono_celular = (String) pag.get("tx_telefono_celular");
        }
        String telefono_local;
        if (pag.containsKey("telefono_local")) {
            telefono_local = (String) pag.get("telefono_local");
        }
        else {
            telefono_local = (String) pag.get("tx_telefono_local");
        }
        String correo;
        if (pag.containsKey("correo")) {
            correo = (String) pag.get("correo");
        }
        else {
            correo = (String) pag.get("tx_correo");
        }
        String sexo;
        if (pag.containsKey("sexo")) {
            sexo = (String) pag.get("sexo");
        }
        else {
            sexo = (String) pag.get("tx_sexo");
        }
        String codigo;
        if (pag.containsKey("codigo")) {
            codigo = (String) pag.get("codigo");
        }
        else {
            codigo = (String) pag.get("tx_codigo");
        }

        String identificacion;
        if (pag.containsKey("identificacion")) {
            identificacion = (String) pag.get("identificacion");
        }
        else {
            if (pag.containsKey("nu_identificacion")) {
                identificacion = (String) pag.get("nu_identificacion");
            }
            else {
                identificacion = null;
            }
        }

        this.setId(id.intValue());
        this.setNombre1(nombre);
        this.setApellido1(apellido1);
        if (id_rol != null) {
            this.setIdRol(id_rol.intValue());
        }
        this.setFechaNac(fecha_nacimiento);
        this.setSexo(sexo);
        this.setTelfCelular(telefono_celular);
        this.setTelfLocal(telefono_local);
        if (identificacion != null) {
            this.setIdentificacion(identificacion);
        }
        this.setCorreo(correo);
        this.setCodigo(codigo);
    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean update, Boolean activo) {
        ContentValues values = new ContentValues();

        if (this.getApellido1() != null) {
            values.put(APELLIDO1, this.getApellido1());
        }

        if (this.getCorreo() != null) {
            values.put(CORREO, this.getCorreo());
        }

        if (this.getNombre1() != null) {
            values.put(NOMBRE1, this.getNombre1());
        }

        if (this.getCodigo() != null) {
            values.put(CODIGO, this.getCodigo());
        }

        if (this.getFechaNac() != null) {
            values.put(FECHA_NACIMIENTO, this.getFechaNac());
        }

        if (this.getIdentificacion() != null) {
            values.put(NUMERO_IDENTIFICACION, this.getIdentificacion());
        }

        if (this.getId() != null) {
            values.put(ID_USUARIO, this.getId());
        }

        if (this.getIdRol() != null) {
            values.put(ID_ROL, this.getIdRol());
        }

        if (this.getSexo() != null) {
            values.put(SEXO, this.getSexo());
        }

        if (this.getTelfCelular() != null) {
            values.put(TELEFONO_CELULAR, this.getTelfCelular());
        }

        if (this.getTelfLocal() != null) {
            values.put(TELEFONO_LOCAL, this.getTelfLocal());
        }

        values.put(NUEVO, nuevo);
        values.put(LOGIN, activo);

        return values;
    }

    public Integer getId() {
        return id;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public String getNombre1() {
        return nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }


    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelfLocal() {
        return telfLocal;
    }

    public String getTelfCelular() {
        return telfCelular;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public void setTelfCelular(String telfCelular) {
        this.telfCelular = telfCelular;
    }

    public void setTelfLocal(String telfLocal) {
        this.telfLocal = telfLocal;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
