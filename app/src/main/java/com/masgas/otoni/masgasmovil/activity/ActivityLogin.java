package com.masgas.otoni.masgasmovil.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.utils.WebService;

import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Vector;


/**
 * Created by otoni on 6/11/2017.
 */

public class ActivityLogin extends AppCompatActivity {

    private AlertDialog dialog;

    protected static final int PERMISOS_CAMARA = 1 ;
    protected static final int PERMISOS_ALMACENAMIENTO_EXTERNO = 2;
    protected static final int PERMISOS_UBICACION = 3;
    protected static final int PERMISOS_LLAMADAS = 4;
    protected static final int PERMISOS_SMS = 5;

    RelativeLayout contenedor;
    EditText email, pass;
    TextInputLayout eE, ep;

    List<String> parametros, valores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //chequearPermisos();

        parametros = new ArrayList<>();
        parametros.add("tx_correo");
        parametros.add("tx_password");

        valores = new ArrayList<>();
        //valores.add("otonielmax");
        //valores.add("123");

        contenedor = (RelativeLayout) findViewById(R.id.login_contenedor_principal);
        email = (EditText) findViewById(R.id.login_alias);
        eE = (TextInputLayout) findViewById(R.id.login_alias_error);

        pass = (EditText) findViewById(R.id.login_password);
        ep = (TextInputLayout) findViewById(R.id.login_password_error);

        /*
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new WebService(parametros, valores, ActivityLogin.this, "POST", "login", ActivityPedido.class).execute();
            }
        });
        */
    }

    public void mostrarSnac(String mensaje) {
        Snackbar.make(contenedor, mensaje, Snackbar.LENGTH_LONG).show();
    }

    public void validarDatos(View view) {
        if (email.getText().length() > 0) {
            if (Utilidades.isEmail(email.getText().toString()) || Utilidades.isNumeric(email.getText().toString())) {
                if (pass.getText().length() > 0) {
                    if (Utilidades.isPasswordValid(pass.getText().toString())) {
                        parametros.clear();
                        parametros.add("tx_correo");
                        parametros.add("tx_password");

                        valores.clear();

                        valores.add(email.getText().toString());
                        valores.add(pass.getText().toString());

                        login();
                    }
                    else {
                        mostrarSnac("La longitud del password no corresponde");
                        pass.requestFocus();
                        ep.setError("El password debe tener entre 6 a 8 caracteres");
                    }
                }
                else {
                    mostrarSnac("Debes ingresar tu password");
                    pass.requestFocus();
                    ep.setError("No dejes este campo vacio");
                }
            }
            else {
                mostrarSnac("El formato ingresado no corresponde al de un correo o numero teléfonico");
                email.requestFocus();
                eE.setError("El formato de correo o numero teléfonico no es el correcto.");
            }
        }
        else {
            mostrarSnac("Debes ingresar tu correo o número teléfonico");
            email.requestFocus();
            eE.setError("No dejes este campo vacio");
        }
    }

    public void login() {
        new WebService(
                parametros,
                valores,
                ActivityLogin.this,
                "POST",
                "login",
                ActivityPedido.class,
                "Verificando datos de acceso",
                false
        ).execute();
        //Intent intent = new Intent(ActivityLogin.this, ActivityPedido.class);
        //startActivity(intent);
    }

    public void chequearPermisos() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

            }
            else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISOS_CAMARA);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            }
            else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISOS_ALMACENAMIENTO_EXTERNO);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

            }
            else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISOS_UBICACION);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {

            }
            else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISOS_LLAMADAS);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {

            }
            else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISOS_SMS);
            }
        }
    }

    public void onDialogRecueperarClave(View view) {
        final Dialog ventana = new Dialog(this);
        ventana.setContentView(R.layout.dialog_recuperar_clave);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText correo = (EditText) ventana.findViewById(R.id.dialog_correos);

        TextView scanner = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (correo.getText().length() > 0) {
                    parametros.clear();
                    parametros.add("correo");

                    valores.clear();
                    valores.add(correo.getText().toString());

                    new WebService(
                            parametros,
                            valores,
                            ActivityLogin.this,
                            "POST",
                            "usuario/recuperar_password",
                            ActivityPedido.class,
                            "Verificando datos para recuperación",
                            false
                    ).execute();

                    ventana.dismiss();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes ingresar tu correo", Toast.LENGTH_LONG).show();
                }
            }
        });

        ventana.show();
    }

    private void showAlertDialog(String message, boolean isCancelable){
        dialog = Utilidades.buildAlertDialog("Verificando", message, isCancelable, ActivityLogin.this);
        dialog.show();
    }

    private void dismissAlertDialog() {
        dialog.dismiss();
    }
    /*
    private class WebService extends AsyncTask<Vector, Integer, Integer> {

        String email, pass;

        public WebService(String email, String pass) {
            this.email = email;
            this.pass = pass;
        }

        @Override
        protected void onPreExecute() {
            //dialog.show();
            showAlertDialog("Verificando datos de usuario...", false);
            Log.e("WebService", "Iniciado el servicio");
        }

        @Override
        protected Integer doInBackground(Vector... var) {

            try {

                String urlString = "http://192.168.69.104/MasGasApi/public/api/v1/login";
                URL url = new URL(urlString);

                Log.e("Ruta", urlString);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setDoInput(true);
                connection.setDoOutput(true);

                connection.connect();

                String charset = "UTF-8";
                String s = "tx_alias=" + URLEncoder.encode("otonielmax", charset);
                s += "&tx_password=" + URLEncoder.encode("123", charset);

                Log.e("URL", s);


                OutputStream os = connection.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

                osw.write(s);
                osw.flush();
                osw.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String output2;

                while ((output2 = bufferedReader.readLine()) != null) {
                    Log.e("WebService", "Retorno " + output2);
                }

                connection.disconnect();

                return 0;

            }
            catch (Exception exc) {
                Log.e("Error", exc.toString());
                //Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_SHORT).show();
                return 4;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... progreso) {

        }

        @Override
        protected void onPostExecute(Integer num) {
            dismissAlertDialog();
            if (num == 0) {
                Toast.makeText(getApplicationContext(), "Lo logramos", Toast.LENGTH_LONG).show();
            }
            else if (num == 1) {
                Toast.makeText(getApplicationContext(), "Los datos ingresados no corresponden a ningun usuario", Toast.LENGTH_LONG).show();
            }
            else if (num == 2) {
                Toast.makeText(getApplicationContext(), "El usuario se encuentra eliminado del sistema", Toast.LENGTH_LONG).show();
            }
            else if (num == 3) {
                Toast.makeText(getApplicationContext(), "El usuario no se encuentra activo", Toast.LENGTH_LONG).show();
            }
            else if (num == 4) {
                Toast.makeText(getApplicationContext(), "Error en el procesamiento de datos", Toast.LENGTH_LONG).show();
            }

            //button.setVisibility(View.GONE);
        }
    }
    */

}
