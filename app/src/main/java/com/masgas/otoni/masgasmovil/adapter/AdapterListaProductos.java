package com.masgas.otoni.masgasmovil.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.Producto;
import com.masgas.otoni.masgasmovil.utils.RepeatListener;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.util.List;

/**
 * Created by otoni on 10/1/2018.
 */

public class AdapterListaProductos extends RecyclerView.Adapter<AdapterListaProductos.ViewHolderProducto>
        implements ItemClickListener {

    private List<Producto> items;
    private Context context;
    public ModeloGeneral modeloRelacional;

    public static class ViewHolderProducto extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public ImageView imagen;
        public TextView nombre, contenido;
        public EditText cantidad;
        public Button min, mas;

        // Interfaz de comunicación
        public ItemClickListener listener;

        public ViewHolderProducto(View v, ItemClickListener listener) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            nombre = (TextView) v.findViewById(R.id.titulo);
            contenido = (TextView) v.findViewById(R.id.contenido);
            cantidad = (EditText) v.findViewById(R.id.cantidad);
            min = (Button) v.findViewById(R.id.litros_min);
            mas = (Button) v.findViewById(R.id.litros_mas);

            v.setOnClickListener(this);

            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }

    public AdapterListaProductos(List<Producto> items, Context context) {
        this.items = items;
        this.context = context;
        modeloRelacional = new ModeloGeneral(context);
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<Producto> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {
        // Imagen a compartir entre transiciones
        //View sharedImage = view.findViewById(R.id.imagen);
        //ActivityCuentasPorCobrar.launch((Activity) context, items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public AdapterListaProductos.ViewHolderProducto onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_producto, viewGroup, false);
        return new AdapterListaProductos.ViewHolderProducto(v, this);
    }

    @Override
    public void onBindViewHolder(final AdapterListaProductos.ViewHolderProducto viewHolder, final int position) {

        viewHolder.nombre.setText(items.get(position).getNombre());
        viewHolder.contenido.setText(Utilidades.doubleToDollar(items.get(position).getPrecio()));
        viewHolder.cantidad
                .addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {

                    Item item = modeloRelacional.consultarProductoEnCarrito(items.get(position).getId());
                    if (item != null) {
                        item.setCantidad(Integer.parseInt(charSequence.toString()));
                        item.setPrecioIndependiente(item.getCantidad() * items.get(position).getPrecio());

                        modeloRelacional.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                    }
                    else {
                        item = new Item();

                        item.setCantidad(Integer.parseInt(charSequence.toString()));
                        item.setIdProducto(items.get(position).getId());
                        item.setPrecioIndependiente(item.getCantidad() * items.get(position).getPrecio());

                        modeloRelacional.insertar(item.TABLE, item.crearRegistro(true, false));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Toast.makeText(context, "Valor guardado " + editable.toString(), Toast.LENGTH_LONG).show();
            }
        });

        viewHolder.min.setOnTouchListener(new RepeatListener(400, 100, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer num;
                if (viewHolder.cantidad.getText().length() > 0) {
                    num = Integer.parseInt(viewHolder.cantidad.getText().toString());
                    if (num > 0) {
                        num--;
                    }
                    else {
                        Toast.makeText(context, "La cantidad no puede ser menor a 0", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    num = null;
                    Toast.makeText(context, "La cantidad no puede ser menor a 0", Toast.LENGTH_LONG).show();
                }

                if (num != null) {
                    viewHolder.cantidad.setText("" + num);

                    Item item = modeloRelacional.consultarProductoEnCarrito(items.get(position).getId());
                    if (item != null) {
                        item.setCantidad(num);
                        item.setPrecioIndependiente(item.getCantidad() * items.get(position).getPrecio());

                        modeloRelacional.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                    }
                    else {
                        item = new Item();

                        item.setCantidad(num);
                        item.setIdProducto(items.get(position).getId());
                        item.setPrecioIndependiente(item.getCantidad() * items.get(position).getPrecio());

                        modeloRelacional.insertar(item.TABLE, item.crearRegistro(true, false));
                    }
                }
            }
        }));

        viewHolder.mas.setOnTouchListener(new RepeatListener(400, 100, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer num;
                if (viewHolder.cantidad.getText().length() > 0) {
                    num = Integer.parseInt(viewHolder.cantidad.getText().toString());
                    num++;
                }
                else {
                    num = 1;
                }

                if (num != null) {
                    viewHolder.cantidad.setText("" + num);

                    Item item = modeloRelacional.consultarProductoEnCarrito(items.get(position).getId());
                    if (item != null) {
                        item.setCantidad(num);
                        item.setPrecioIndependiente(item.getCantidad() * items.get(position).getPrecio());

                        modeloRelacional.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                    }
                    else {
                        item = new Item();

                        item.setCantidad(num);
                        item.setIdProducto(items.get(position).getId());
                        item.setPrecioIndependiente(item.getCantidad() * items.get(position).getPrecio());

                        modeloRelacional.insertar(item.TABLE, item.crearRegistro(true, false));
                    }
                }
            }
        }));

               /* .setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                if (textView.getText().length() > 0) {
                    if (action == EditorInfo.IME_ACTION_DONE || action == EditorInfo.IME_ACTION_NEXT) {
                        //Snackbar.make(new View(context), "Valor guardado " + textView.getText().toString(), Snackbar.LENGTH_INDEFINITE).show();
                        //Toast.makeText(context, "Valor guardado " + textView.getText().toString(), Toast.LENGTH_LONG).show();

                        Item item = modeloRelacional.consultarProductoEnCarrito(items.get(i).getId());
                        if (item != null) {
                            item.setCantidad(Integer.parseInt(textView.getText().toString()));
                            item.setPrecioIndependiente(item.getCantidad() * items.get(i).getPrecio());

                            modeloRelacional.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                        }
                        else {
                            item = new Item();

                            item.setCantidad(Integer.parseInt(textView.getText().toString()));
                            item.setIdProducto(items.get(i).getId());
                            item.setPrecioIndependiente(item.getCantidad() * items.get(i).getPrecio());

                            modeloRelacional.insertar(item.TABLE, item.crearRegistro(true, false));
                        }

                        return true;
                    }
                }

                return false;
            }
        });
        */
    }
}
