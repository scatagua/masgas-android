package com.masgas.otoni.masgasmovil.view;

/**
 * Created by otoni on 5/3/2018.
 */

public class ViewOrdenesPago {

    private String fecha;
    private String numero;
    private String estatus;
    private String precio;

    public ViewOrdenesPago() {

    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
