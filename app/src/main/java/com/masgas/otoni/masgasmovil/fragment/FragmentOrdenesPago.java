package com.masgas.otoni.masgasmovil.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaMensajes;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaOrdenesPAgo;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.NotificacionMensaje;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.view.ViewBuzonNotificaciones;
import com.masgas.otoni.masgasmovil.view.ViewOrdenesPago;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otoni on 5/3/2018.
 */

public class FragmentOrdenesPago extends Fragment {

    private RecyclerView recycler;
    private AdapterListaOrdenesPAgo adapter;
    private LinearLayoutManager lManager;
    private LinearLayout empty;
    private View root;
    private ModeloGeneral modeloGeneral;

    List<ViewOrdenesPago> objects;
    Usuario usuario;
    Integer tipo;
    /*
    public FragmentOrdenesPago(Integer tipo) {
        this.tipo = tipo;
    }
    */

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        modeloGeneral = new ModeloGeneral(getContext());
        usuario = modeloGeneral.consultarUsuarioActivo();

        if (getArguments().containsKey("tipo")) {
            tipo = getArguments().getInt("tipo");
            Log.e("Tipo", tipo + "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        root = inflater.inflate(R.layout.fragment_ordenes_pago, viewGroup, false);
        cargarVista();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        llenarDatos();
    }

    public void cargarVista() {
        if (root != null) {
            empty = (LinearLayout) root.findViewById(R.id.empty_state_container);

            recycler = (RecyclerView) root.findViewById(R.id.reciclador);
            recycler.setHasFixedSize(true);

            lManager = new LinearLayoutManager(getContext());
            lManager.setOrientation(LinearLayoutManager.VERTICAL);
            recycler.setLayoutManager(lManager);

            llenarDatos();
        }
    }

    public void llenarDatos() {
        if (usuario == null) {
            usuario = modeloGeneral.consultarUsuarioActivo();
        }

        ViewOrdenesPago o1 = new ViewOrdenesPago();
        o1.setFecha("12 Dec");
        o1.setEstatus("Pendiente");
        o1.setNumero("PED-01251");
        o1.setPrecio("$ 5.000,00");

        ViewOrdenesPago o2 = new ViewOrdenesPago();
        o2.setFecha("15 Dec");
        o2.setEstatus("Procesada");
        o2.setNumero("PED-01551");
        o2.setPrecio("$ 4.800,00");

        ViewOrdenesPago o3 = new ViewOrdenesPago();
        o3.setFecha("19 Dec");
        o3.setEstatus("Pendiente");
        o3.setNumero("PED-01748");
        o3.setPrecio("$ 12.100,00");

        ViewOrdenesPago o4 = new ViewOrdenesPago();
        o4.setFecha("19 Dec");
        o4.setEstatus("Procesada");
        o4.setNumero("PED-02465");
        o4.setPrecio("$ 6.750,00");

        objects = new ArrayList<>();

        if (tipo == 1) {
            objects.add(o1);
            objects.add(o2);
            objects.add(o3);
            objects.add(o4);
        }
        else if (tipo == 2) {
            objects.add(o2);
            objects.add(o4);
        }
        else {
            objects.add(o1);
            objects.add(o3);
        }

        if (objects.size() > 0) {
            empty.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            if (adapter != null) {
                adapter.clear();
                adapter.addAll(objects);
            }
            else {
                adapter = new AdapterListaOrdenesPAgo(objects, getContext());
                recycler.setAdapter(adapter);
            }
        }
        else {
            recycler.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
            if (adapter != null) {
                adapter.clear();
                adapter.addAll(objects);
            }
            else {
                adapter = new AdapterListaOrdenesPAgo(objects, getContext());
                recycler.setAdapter(adapter);
            }
        }

    }
}
