package com.masgas.otoni.masgasmovil.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaItemsCarrito;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaProductos;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Producto;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.util.List;

/**
 * Created by otoni on 17/1/2018.
 */

public class FragmentCarrito extends Fragment {

    private RecyclerView recycler;
    private AdapterListaItemsCarrito adapter;
    private LinearLayoutManager lManager;
    private LinearLayout empty;
    private View root;
    private ModeloGeneral modeloGeneral;

    List<ViewItemCarrito> productos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        root = inflater.inflate(R.layout.fragment_carrito, viewGroup, false);
        modeloGeneral = new ModeloGeneral(getContext());
        cargarVista();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        llenarDatos();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            onResume();
        }
    }

    public void cargarVista() {
        if (root != null) {
            empty = (LinearLayout) root.findViewById(R.id.empty_state_container);

            recycler = (RecyclerView) root.findViewById(R.id.reciclador);
            recycler.setHasFixedSize(true);

            lManager = new LinearLayoutManager(getContext());
            lManager.setOrientation(LinearLayoutManager.VERTICAL);
            recycler.setLayoutManager(lManager);

            llenarDatos();
        }
    }

    public void llenarDatos() {
        productos = modeloGeneral.consultarItemsCarrito();
        //Log.e("Carrito", modeloGeneral.consultarItemsCarrito2().size() + "");
        //Log.e("Carrito", modeloGeneral.consultarItemsCarrito().size() + "");
        if (productos.size() > 0) {
            empty.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            if (adapter != null) {
                adapter.clear();
                adapter.addAll(productos);
            }
            else {
                adapter = new AdapterListaItemsCarrito(productos, getContext());
                recycler.setAdapter(adapter);
            }
        }
        else {
            recycler.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
            if (adapter != null) {
                adapter.clear();
                adapter.addAll(productos);
            }
            else {
                adapter = new AdapterListaItemsCarrito(productos, getContext());
                recycler.setAdapter(adapter);
            }
        }

    }
}
