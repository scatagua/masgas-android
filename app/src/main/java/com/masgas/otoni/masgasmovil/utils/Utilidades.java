package com.masgas.otoni.masgasmovil.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.masgas.otoni.masgasmovil.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by otoni on 13/11/2017.
 */

public class Utilidades {

    public static AlertDialog buildAlertDialog(String title, String message, boolean isCancelable, Context context){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setTitle(title);

        if(isCancelable){
            builder.setPositiveButton(android.R.string.ok, null);
        }else {
            builder.setCancelable(false);
        }
        return builder.create();
    }

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat dayFormat = new SimpleDateFormat("dd");
    private static DateFormat monthFormat = new SimpleDateFormat("MMM");
    private static DateFormat timeFormat = new SimpleDateFormat("hh:mm a");

    public static String getCurrentTime() {

        Date today = Calendar.getInstance().getTime();
        return timeFormat.format(today);
    }

    public static String getCurrentDate() {

        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getFechaCorta(String fecha) {

        try {
            Date date = dateFormat.parse(fecha);
            return dayFormat.format(date) + " " + monthFormat.format(date);
        }
        catch (ParseException exc) {
            Log.e("Utilidades", exc.toString());
        }

        return null;
    }

    public static String doubleToDollar(Double valor) {
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        return "$ " + formateador.format(valor);
    }

    public static Boolean isEmail(String text) {
        if (text.contains("@") && text.contains(".") && text.length() > 4) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Boolean isNumeric(String text) {
        if (text.contains("0")) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Boolean isPasswordValid(String obj) {
        if (obj.length() > 5 && obj.length() < 9) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Boolean isDateValid(String obj) {
        if (obj.length() == 10 && obj.contains("-")) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Bitmap generarQR(String Value, Context context) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    500, 500, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        context.getResources().getColor(R.color.midnithg):context.getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
}
