package com.masgas.otoni.masgasmovil.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaProductos;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.Producto;
import com.masgas.otoni.masgasmovil.utils.RepeatListener;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;

import java.util.List;

/**
 * Created by otoni on 10/1/2018.
 */

public class FragmentProductos extends Fragment {

    private RecyclerView recycler;
    private AdapterListaProductos adapter;
    private RecyclerView.LayoutManager lManager;
    private View root;
    private ModeloGeneral modeloGeneral;
    private ListView listView;
    private LinearLayout cilindro, estacionario, carburacion, vacia, pipa;
    private View cilV, estV, carV;
    private TextView cliT, estT, carT/*, cantLits*/;
    private FloatingActionButton floatingActionButton;
    private Integer litros = 0;
    private Button mas, min;
    private EditText cantLits;

    List<Producto> productos;
    Producto pipaProd;

    public FragmentProductos() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        root = inflater.inflate(R.layout.fragment_productos, viewGroup, false);
        modeloGeneral = new ModeloGeneral(getContext());
        cargarVista();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (productos != null) {
            llenarDatos();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            onResume();
        }
    }

    public void llenarDatos() {
        productos = modeloGeneral.consultarProductosPorCategoria(1);
        pipaProd = modeloGeneral.consultarPipa();

        adapter = new AdapterListaProductos(productos, getContext());
        recycler.setAdapter(adapter);

        if (pipaProd != null) {
            Item pipaItem = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
            if (pipaItem != null) {
                cantLits.setText("" + pipaItem.getCantidad());
            }
        }
    }

    public void cargarVista() {
        if (root != null) {
            recycler = (RecyclerView) root.findViewById(R.id.reciclador);
            recycler.setHasFixedSize(true);

            lManager = new LinearLayoutManager(getContext());
            recycler.setLayoutManager(lManager);

            cilV = (View) root.findViewById(R.id.fragmente_productos_select_cilindros_view);
            estV = (View) root.findViewById(R.id.fragmente_productos_select_estacionario_view);
            //carV = (View) root.findViewById(R.id.fragmente_productos_select_carburacion_view);

            cliT = (TextView) root.findViewById(R.id.fragmente_productos_select_cilindros_text);
            //carT = (TextView) root.findViewById(R.id.fragmente_productos_select_carburacion_text);
            estT = (TextView) root.findViewById(R.id.fragmente_productos_select_estacionario_text);

            pipa = (LinearLayout) root.findViewById(R.id.pipa);
            cantLits = (EditText) root.findViewById(R.id.cantidad_litros);
            //cantLits.setText(litros + " Lts");

            llenarDatos();

            cantLits.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (cantLits.length() > 1) {
                        if (Integer.parseInt(cantLits.getText().toString()) > 0 && Integer.parseInt(cantLits.getText().toString()) <= 5000) {
                            Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                            if (item != null) {
                                item.setCantidad(Integer.parseInt(cantLits.getText().toString()));
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                            }
                            else {
                                item = new Item();

                                item.setCantidad(Integer.parseInt(cantLits.getText().toString()));
                                item.setIdProducto(pipaProd.getId());
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                            }
                        }
                        else {
                            cantLits.setText("0");

                            Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                            if (item != null) {
                                item.setCantidad(0);
                                item.setPrecioIndependiente(0 * pipaProd.getPrecio());

                                modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                            }
                            else {
                                item = new Item();

                                item.setCantidad(0);
                                item.setIdProducto(pipaProd.getId());
                                item.setPrecioIndependiente(0 * pipaProd.getPrecio());

                                modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                            }

                            Toast.makeText(getContext(), "La cantidad debe estar comprendida entre 50 y 5000 litros", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });

            mas = (Button) root.findViewById(R.id.litros_mas);
            mas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cantLits.getText().length() > 0) {
                        litros = Integer.parseInt(cantLits.getText().toString());
                        if (litros < 5000) {
                            litros++;
                            cantLits.setText(litros + "");

                            Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                            if (item != null) {
                                item.setCantidad(litros);
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                            }
                            else {
                                item = new Item();

                                item.setCantidad(litros);
                                item.setIdProducto(pipaProd.getId());
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                            }
                        }
                        else {
                            Toast.makeText(getContext(), "No se puede exceder los 5.000 litros", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        litros = 1;
                        cantLits.setText(litros + "");

                        Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                        if (item != null) {
                            item.setCantidad(litros);
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                        }
                        else {
                            item = new Item();

                            item.setCantidad(litros);
                            item.setIdProducto(pipaProd.getId());
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                        }
                    }
                }
            });
            mas.setOnTouchListener(new RepeatListener(400, 100, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cantLits.getText().length() > 0) {
                                litros = Integer.parseInt(cantLits.getText().toString());
                                if (litros < 5000) {
                                    litros++;
                                    cantLits.setText(litros + "");

                                    Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                                    if (item != null) {
                                        item.setCantidad(litros);
                                        item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                        modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                                    }
                                    else {
                                        item = new Item();

                                        item.setCantidad(litros);
                                        item.setIdProducto(pipaProd.getId());
                                        item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                        modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                                    }
                                }
                                else {
                                    Toast.makeText(getContext(), "No se puede exceder los 5.000 litros", Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                litros = 0;
                                cantLits.setText(litros + "");

                                Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                                if (item != null) {
                                    item.setCantidad(litros);
                                    item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                    modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                                }
                                else {
                                    item = new Item();

                                    item.setCantidad(litros);
                                    item.setIdProducto(pipaProd.getId());
                                    item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                    modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                                }
                            }
                        }
            }));

            min = (Button) root.findViewById(R.id.litros_menos);
            min.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cantLits.getText().length() > 0) {
                        litros = Integer.parseInt(cantLits.getText().toString());
                        if (litros > 0) {
                            litros--;
                            cantLits.setText(litros + "");

                            Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                            if (item != null) {
                                item.setCantidad(litros);
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                            }
                            else {
                                item = new Item();

                                item.setCantidad(litros);
                                item.setIdProducto(pipaProd.getId());
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                            }
                        }
                        else {
                            Toast.makeText(getContext(), "No puede ser menor a 50 litros", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(getContext(), "No puede ser menor a 50 litros", Toast.LENGTH_LONG).show();
                    }
                    /*
                    if (litros - 1 >= 0) {
                        litros--;
                        cantLits.setText(litros + " Lts");

                        Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                        if (item != null) {
                            item.setCantidad(litros);
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                        }
                        else {
                            item = new Item();

                            item.setCantidad(litros);
                            item.setIdProducto(pipaProd.getId());
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                        }
                    }
                    else {
                        Toast.makeText(getContext(), "No puede ser menor a 0", Toast.LENGTH_LONG).show();
                    }*/
                }
            });
            min.setOnTouchListener(new RepeatListener(400, 100, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cantLits.getText().length() > 0) {
                        litros = Integer.parseInt(cantLits.getText().toString());
                        if (litros >0) {
                            litros--;
                            cantLits.setText(litros + "");

                            Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                            if (item != null) {
                                item.setCantidad(litros);
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                            }
                            else {
                                item = new Item();

                                item.setCantidad(litros);
                                item.setIdProducto(pipaProd.getId());
                                item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                                modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                            }
                        }
                        else {
                            Toast.makeText(getContext(), "No puede ser menor a 0 litros", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        litros = 0;
                        cantLits.setText(litros + "");

                        Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                        if (item != null) {
                            item.setCantidad(litros);
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                        }
                        else {
                            item = new Item();

                            item.setCantidad(litros);
                            item.setIdProducto(pipaProd.getId());
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                        }
                    }
                    /*
                    if (litros - 1 >= 0) {
                        litros--;
                        cantLits.setText(litros + "");

                        Item item = modeloGeneral.consultarProductoEnCarrito(pipaProd.getId());
                        if (item != null) {
                            item.setCantidad(litros);
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                        }
                        else {
                            item = new Item();

                            item.setCantidad(litros);
                            item.setIdProducto(pipaProd.getId());
                            item.setPrecioIndependiente(item.getCantidad() * pipaProd.getPrecio());

                            modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                        }
                    }
                    else {
                        Toast.makeText(getContext(), "No puede ser menor a 0", Toast.LENGTH_LONG).show();
                    }
                    */
                }
            }));

            cilindro = (LinearLayout) root.findViewById(R.id.fragmente_productos_select_cilindros);
            cilindro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    actualizarDatos(1);

                    cilV.setBackgroundResource(R.color.pumpkin_orange);
                    //carV.setBackgroundColor(Color.TRANSPARENT);
                    estV.setBackgroundColor(Color.TRANSPARENT);

                    cliT.setTypeface(Typeface.DEFAULT_BOLD);
                    //carT.setTypeface(Typeface.DEFAULT);
                    estT.setTypeface(Typeface.DEFAULT);
                }
            });
            /*
            carburacion = (LinearLayout) root.findViewById(R.id.fragmente_productos_select_carburacion);
            carburacion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    actualizarDatos(3);

                    cilV.setBackgroundColor(Color.TRANSPARENT);
                    carV.setBackgroundResource(R.color.pumpkin_orange);
                    estV.setBackgroundColor(Color.TRANSPARENT);

                    cliT.setTypeface(Typeface.DEFAULT);
                    carT.setTypeface(Typeface.DEFAULT_BOLD);
                    estT.setTypeface(Typeface.DEFAULT);
                }
            });
            */

            estacionario = (LinearLayout) root.findViewById(R.id.fragmente_productos_select_estacionario);
            estacionario.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    actualizarDatos(2);

                    cilV.setBackgroundColor(Color.TRANSPARENT);
                    //carV.setBackgroundColor(Color.TRANSPARENT);
                    estV.setBackgroundResource(R.color.pumpkin_orange);

                    cliT.setTypeface(Typeface.DEFAULT);
                    //carT.setTypeface(Typeface.DEFAULT);
                    estT.setTypeface(Typeface.DEFAULT_BOLD);
                }
            });

            vacia = (LinearLayout) root.findViewById(R.id.empty_state_container);
        }
    }

    public void actualizarDatos(Integer idCategoria) {
        adapter.clear();
        if (idCategoria != 2) {
            productos = modeloGeneral.consultarProductosPorCategoria(idCategoria);
            if (productos.size() > 0) {
                adapter.addAll(productos);
                recycler.setVisibility(View.VISIBLE);
                vacia.setVisibility(View.GONE);
            }
            else {
                recycler.setVisibility(View.GONE);
                vacia.setVisibility(View.VISIBLE);
            }
        }
        else {
            recycler.setVisibility(View.GONE);
            vacia.setVisibility(View.GONE);
            pipa.setVisibility(View.VISIBLE);
        }

    }

    public void onChangeCategoriaProducto(View view) {

        switch (view.getId()) {
            case R.id.fragmente_productos_select_cilindros:

                break;

            case R.id.fragmente_productos_select_estacionario:

                break;

            case R.id.fragmente_productos_select_carburacion_view:

                break;
        }
    }

    public void actualizarVista() {
        if (listView != null) {
            productos = modeloGeneral.consultarProductos();
            /*
            MyListAdapter adapter = new MyListAdapter(getContext(), productos);

            //SwingBottomInAnimationAdapter animationAdapter = new SwingBottomInAnimationAdapter(adapter);
            //animationAdapter.setAbsListView(listView);
            listView.setAdapter(adapter);
            */
        }
    }
    /*
    private class MyListAdapter extends com.nhaarman.listviewanimations.ArrayAdapter<Producto>  {

        private Context context;
        protected List<Producto> clientesOriginales;

        private class MiViewHolder {
            public ImageView imagen;
            public TextView nombre, contenido;
            public EditText cantidad;
        }

        public MyListAdapter(Context context, List<Producto> items) {
            super(items);
            this.clientesOriginales = items;
            this.context = context;
        }

        @Override
        public View getView(final int posicion, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                // Se infla el layout de la vista principal de las rutas en caso de que esta se null
                v = LayoutInflater.from(this.context).inflate(R.layout.card_view_producto, parent, false);
                // Instanciamos el objeto que contiene las propiedades de nuestra vista ruta
                MiViewHolder viewHolder = new MiViewHolder();

                viewHolder.imagen = (ImageView) v.findViewById(R.id.imagen);
                viewHolder.nombre = (TextView) v.findViewById(R.id.titulo);
                viewHolder.contenido = (TextView) v.findViewById(R.id.contenido);
                viewHolder.cantidad = (EditText) v.findViewById(R.id.cantidad);

                v.setTag(viewHolder);
            }

            final MiViewHolder holder = (MiViewHolder) v.getTag();

            final Producto key = (Producto) getItem(posicion);

            holder.nombre.setText(key.getNombre());
            holder.contenido.setText(Utilidades.doubleToDollar(key.getPrecio()));

            holder.cantidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        Log.e("Producto", "" + v.getId());
                    }
                }
            });

            holder.cantidad.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() > 0) {
                        Item item = modeloGeneral.consultarProductoEnCarrito(key.getId());
                        if (item != null) {
                            item.setCantidad(Integer.parseInt(charSequence.toString()));
                            item.setPrecioIndependiente(item.getCantidad() * key.getPrecio());

                            modeloGeneral.actualizarProductoCarrito(item.getId(), item.getCantidad(), item.getPrecioIndependiente());
                        }
                        else {
                            item = new Item();

                            item.setCantidad(Integer.parseInt(charSequence.toString()));
                            item.setIdProducto(key.getId());
                            item.setPrecioIndependiente(item.getCantidad() * key.getPrecio());

                            modeloGeneral.insertar(item.TABLE, item.crearRegistro(true, false));
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    //Toast.makeText(context, "Valor guardado " + editable.toString(), Toast.LENGTH_LONG).show();
                }
            });

            return v;

        }
    }
    */
}
