package com.masgas.otoni.masgasmovil.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.activity.ActivityLogin;
import com.masgas.otoni.masgasmovil.activity.ActivityPedido;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaItemsCarrito;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaMensajes;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.NotificacionMensaje;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.utils.WebService;
import com.masgas.otoni.masgasmovil.view.ViewBuzonNotificaciones;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otoni on 22/2/2018.
 */

public class FragmentBuzonEntrada extends Fragment {

    private RecyclerView recycler;
    private AdapterListaMensajes adapter;
    private LinearLayoutManager lManager;
    private LinearLayout empty;
    private View root;
    private ModeloGeneral modeloGeneral;
    Button redactar;

    List<ViewBuzonNotificaciones> objects;
    List<NotificacionMensaje> mensajes;
    Usuario usuario;
    Integer idMensaje = -1;
    List<String> parametros, valores;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.e("Crear", "");
        modeloGeneral = new ModeloGeneral(getContext());
        usuario = modeloGeneral.consultarUsuarioActivo();
        mensajes = modeloGeneral.consultarMensajesParaEnviar(usuario.getIdRol());

        parametros = new ArrayList<>();
        parametros.add("id_usuario");
        parametros.add("id_mensaje");
        parametros.add("tx_cuerpo");

        valores = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        Log.e("CrearView", "");
        root = inflater.inflate(R.layout.fragment_buzon, viewGroup, false);
        cargarVista();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Resume", "");
        llenarDatos();
    }

    public void cargarVista() {
        if (root != null) {
            empty = (LinearLayout) root.findViewById(R.id.empty_state_container);

            redactar = (Button) root.findViewById(R.id.fragment_buzon_redactar);
            redactar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogConfirmacion();
                }
            });

            recycler = (RecyclerView) root.findViewById(R.id.reciclador);
            recycler.setHasFixedSize(true);

            lManager = new LinearLayoutManager(getContext());
            lManager.setOrientation(LinearLayoutManager.VERTICAL);
            recycler.setLayoutManager(lManager);

            llenarDatos();
        }
    }

    public void llenarDatos() {
        if (usuario == null) {
            usuario = modeloGeneral.consultarUsuarioActivo();
        }
        objects = modeloGeneral.consultarMensajesEntrada(usuario.getId());
        Log.e("objects data",objects+"");
        if (objects.size() > 0) {
            empty.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            if (adapter != null) {
                adapter.clear();
                adapter.addAll(objects);
            }
            else {
                adapter = new AdapterListaMensajes(objects, getContext());
                recycler.setAdapter(adapter);
            }
        }
        else {
            recycler.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
            if (adapter != null) {
                adapter.clear();
                adapter.addAll(objects);
            }
            else {
                adapter = new AdapterListaMensajes(objects, getContext());
                recycler.setAdapter(adapter);
            }
        }

    }

    private void dialogConfirmacion() {

        final Dialog ventana = new Dialog(getContext());

        ventana.setContentView(R.layout.dialog_redactar_novedad);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        MaterialBetterSpinner codigo = (MaterialBetterSpinner) ventana.findViewById(R.id.motino_novedad);
        final TextInputLayout qr = (TextInputLayout) ventana.findViewById(R.id.registrar_nombre_error);
        final EditText editText = (EditText) ventana.findViewById(R.id.registrar_nombre);

        final List<String> modos = new ArrayList<>();
        //modos.add("Selecciona el motivo");
        for (int i = 0; i < mensajes.size(); i++) {
            modos.add(mensajes.get(i).getMensaje());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.textview_letter_black, modos);
        arrayAdapter.setDropDownViewResource(R.layout.textview_letter_black);

        codigo.setAdapter(arrayAdapter);
        codigo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                idMensaje = i;
            }
        });
        /*
        codigo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    idMensaje = position;
                }
                else if (position == 0) {
                    idMensaje = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        */
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (idMensaje != -1) {
                    if (editText.getText().toString().length() > 0){
                        ventana.dismiss();

                        valores.clear();
                        valores.add(usuario.getId() + "");
                        valores.add(mensajes.get(idMensaje).getId() + "");
                        valores.add(editText.getText().toString());

                        new WebService(
                                parametros,
                                valores,
                                getContext(),
                                "POST",
                                "notificacion",
                                ActivityPedido.class,
                                "Enviando notificación",
                                false
                        ).execute();
                    }
                    else {
                        qr.setError("Debes agregar algun comentario para poder enviar la novedad");
                    }
                }
                else {
                    Toast.makeText(getContext(), "Debes seleccionar un motivo para tu novedad", Toast.LENGTH_LONG).show();
                }
            }
        });

        ventana.show();
    }

}
