package com.masgas.otoni.masgasmovil.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterViewPager;
import com.masgas.otoni.masgasmovil.adapter.AdapterViewPagerText;
import com.masgas.otoni.masgasmovil.fragment.FragmentBuzonEntrada;
import com.masgas.otoni.masgasmovil.fragment.FragmentBuzonSalida;
import com.masgas.otoni.masgasmovil.fragment.FragmentCarrito;
import com.masgas.otoni.masgasmovil.fragment.FragmentProductos;
import com.masgas.otoni.masgasmovil.fragment.FragmentTab;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Usuario;

/**
 * Created by otoni on 22/2/2018.
 */

public class ActivityNovedades extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AdapterViewPagerText adapterViewPager;
    private NavigationView navigationView;
    TextView nombre;

    ModeloGeneral modeloGeneral;

    Usuario usuario;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_menu_novedades);

        modeloGeneral = new ModeloGeneral(this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        // Menu Lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.menu_navegacion_menu_principal_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        usuario = modeloGeneral.consultarUsuarioActivo();

        //Log.e("Usuario", usuario.getId() + " " + usuario.getTelfCelular());

        View hview = navigationView.getHeaderView(0);
        nombre = (TextView) hview.findViewById(R.id.menu_lateral_nombre);
        nombre.setText(usuario.getNombre1() + " " + usuario.getApellido1());

        setTitle("Novedades");

        tabLayout.setupWithViewPager(viewPager);

        if (usuario.getIdRol() == 2) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_lateral_distribuidor);
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        AdapterViewPagerText adapter = new AdapterViewPagerText(getSupportFragmentManager());
        adapter.addFrag(new FragmentBuzonEntrada(), "BUZÓN DE ENTRADA");
        adapter.addFrag(new FragmentBuzonSalida(), "ENVIADOS");
        viewPager.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_icono) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            Intent intent = new Intent(ActivityNovedades.this, ActivityPerfil.class);
            startActivity(intent);
        }
        if (id == R.id.nav_pedidos) {
            Intent intent = new Intent(ActivityNovedades.this, ActivityHistorialPedidos.class);
            startActivity(intent);
        }
        if (id == R.id.nav_notificaciones) {
            Snackbar.make(tabLayout, "Ya estas en este segmento", Snackbar.LENGTH_LONG).show();
        }
        if (id == R.id.nav_pedidos_solicitar) {
            Intent intent = new Intent(ActivityNovedades.this, ActivityPedido.class);
            startActivity(intent);
        }
        /*
        if (id == R.id.nav_historial) {
            Intent intent = new Intent(ActivityPedido.this, ActivityMenu.class);
            startActivity(intent);
        }
        */
        else if (id == R.id.nav_logout) {
            modeloGeneral.deleteAll();
            startActivity(new Intent(getBaseContext(), ActivityLogin.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
