package com.masgas.otoni.masgasmovil.view;

/**
 * Created by otoni on 22/2/2018.
 */

public class ViewBuzonNotificaciones {

    private String mensaje;
    private String fecha;
    private String cuerpo;
    private Integer idNotificacion;

    public ViewBuzonNotificaciones() {

    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Integer idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
}
