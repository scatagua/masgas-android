package com.masgas.otoni.masgasmovil.activity;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;

/**
 * Created by otoni on 29/12/2017.
 */

public class ActivitySplash extends AppCompatActivity {

    Thread splashThread;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash_init);

        StartAnimations();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    private void StartAnimations() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        animation.reset();

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.splash_content);
        linearLayout.clearAnimation();
        linearLayout.startAnimation(animation);

        animation = AnimationUtils.loadAnimation(this, R.anim.traslate);
        animation.reset();

        ImageView imageView = (ImageView) findViewById(R.id.splash_imagen);
        imageView.clearAnimation();
        imageView.startAnimation(animation);

        TextView bienvenido = (TextView) findViewById(R.id.splash_bienvenido);
        bienvenido.clearAnimation();
        bienvenido.startAnimation(animation);

        TextView mensaje = (TextView) findViewById(R.id.splash_mensaje);
        mensaje.clearAnimation();
        mensaje.startAnimation(animation);

        splashThread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;

                    while (waited < 3500) {
                        sleep(100);
                        waited += 100;
                    }

                    Intent mainActivity = new Intent(ActivitySplash.this, MainActivity.class);
                    mainActivity.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(mainActivity);

                    finish();
                }
                catch (InterruptedException exc) {
                    //Log.v(MENSAJE_CONSOLA, exc.toString());
                    Snackbar.make(linearLayout, "Hubo un problema", Snackbar.LENGTH_LONG).show();
                }
                finally {
                    finish();
                }
            }
        };

        splashThread.start();
    }
}
