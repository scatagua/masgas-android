package com.masgas.otoni.masgasmovil.activity;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.zxing.Result;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.MotivoNoEntrega;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.utils.WebService;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by otoni on 4/3/2018.
 */

public class ActivityCambiarEstatus extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    public String tokenOld = "";
    Dialog ventana;
    LinearLayout contenedor;

    List<String> parametros, valores;
    Integer idPedido, opcDev, opcRetra;
    Pedido pedido;

    List<MotivoNoEntrega> motivoNoEntregas;

    ModeloGeneral modeloGeneral;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_cambiar_startus_pedidos);

        Bundle extra = getIntent().getExtras();
        if (!extra.containsKey("id_pedido")) {
            Toast.makeText(getApplicationContext(), "No hay direccion para este pedido" , Toast.LENGTH_LONG).show();
            finish();
        }

        contenedor = (LinearLayout) findViewById(R.id.contenedor_motivos);

        idPedido = extra.getInt("id_pedido");

        parametros = new ArrayList<>();
        parametros.add("id_pedido");
        parametros.add("status");
        parametros.add("motivo");

        valores = new ArrayList<>();

        modeloGeneral = new ModeloGeneral(this);
        pedido = modeloGeneral.consultarPedido(idPedido);
        motivoNoEntregas = modeloGeneral.consultarMotivos();

        setToolbar();

        setupWindowAnimations();

        setTitle("Cambiar estatus de pedido");
    }

    public void crearVistas() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 8);
        for (int i = 0; i < motivoNoEntregas.size(); i++) {
            Button button = new Button(this);
            button.setText(motivoNoEntregas.get(i).getNombre());
            button.setTextColor(getResources().getColor(R.color.white));
            button.setBackgroundColor(getResources().getColor(R.color.pumpkin_orange));
            button.setLayoutParams(params);

            if (motivoNoEntregas.get(i).getNombre().equals("En ruta")) {
                onDialogEnRuta(button);
            }
            else if (motivoNoEntregas.get(i).getNombre().equals("Entregado")) {

            }

            contenedor.addView(button);

        }
    }

    private void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT > 20) {
            getWindow().setReenterTransition(new Explode());
            getWindow().setExitTransition(new Explode().setDuration(500));
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void escanearQR(View view) {
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    public void ingresarCodigo(View view) {
        dialogConfirmarPedido();
    }

    @Override
    public void handleResult(Result result) {
        if (!tokenOld.equals(result.getText())) {
            tokenOld = result.getText();
            if (URLUtil.isValidUrl(result.getText())) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(result.getText()));
                startActivity(browserIntent);
            }
            else {
                if (!result.getText().equals(pedido.getCodigo())) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setTitle("Error");
                    dialog.setMessage("Este codigo QR no es el correcto");

                    AlertDialog alertDialog = dialog.create();
                    alertDialog.show();
                    scannerView.resumeCameraPreview(this);
                }
                else {
                    //Snackbar.make(scannerView, "Se completo con exito", Snackbar.LENGTH_INDEFINITE).show();
                    //finish();
                    valores.clear();
                    valores.add("" + idPedido);
                    valores.add("Entregado");
                    valores.add("Codigo QR");

                    cambiarStatus();

                }

            }
        }

    }

    private void dialogConfirmarPedido() {
        if (ventana == null) {
            ventana = new Dialog(this);
            ventana.setContentView(R.layout.dialog_numero_pedido);
            ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        final EditText num = (EditText) ventana.findViewById(R.id.entrada);
        final TextInputLayout pE = (TextInputLayout) ventana.findViewById(R.id.entrada_error);

        Button aceptar = (Button) ventana.findViewById(R.id.dialog_registrar_direccion_guardar);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (num.getText().length() > 0) {
                    Log.e("check","1,"+num.getText().toString()+","+pedido.getId());
                    String number= num.getText().toString();

                    if (number.contains("PED-"))
                    {
                        number=number.replace("PED-","");
                    }
                    if (number.equals(pedido.getId().toString())) {
                        valores.clear();
                        valores.add("" + idPedido);
                        valores.add("Entregado");
                        valores.add("Manual");

                        cambiarStatus();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "El codigo ingresado no es el correcto", Toast.LENGTH_LONG).show();
                    }
                    //Toast.makeText(getApplicationContext(), "Se guardo", Toast.LENGTH_LONG).show();
                    /*
                    valores.clear();

                    valores.add(domicilio);
                    valores.add(usuario.getId() + "");
                    valores.add(lng + "");
                    valores.add(lat + "");

                    isLocation = false;

                    registrarDireccion();
                    */

                    ventana.dismiss();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes ingresar un numero de pedido valido", Toast.LENGTH_LONG).show();
                    pE.setError("Debes ingresar un numero de pedido valido");
                }
            }
        });

        ventana.show();
    }

    public void onDialogSeleccionEntrega(View view) {
        final Dialog ventana = new Dialog(this);
        ventana.setContentView(R.layout.dialog_pedido_entregado);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView scanner = (TextView) ventana.findViewById(R.id.escanear_qr);
        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
                escanearQR(view);
            }
        });

        TextView codigo = (TextView) ventana.findViewById(R.id.codigo_pedido);
        codigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
                ingresarCodigo(view);
            }
        });

        ventana.show();
    }

    public void onDialogEnRuta(View view) {
        final Dialog ventana = new Dialog(this);
        ventana.setContentView(R.layout.dialog_confirmacion);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView scanner = (TextView) ventana.findViewById(R.id.escanear_qr);
        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valores.clear();
                valores.add("" + idPedido);
                valores.add("En Ruta");
                valores.add("");

                cambiarStatus();
                ventana.dismiss();
            }
        });

        TextView codigo = (TextView) ventana.findViewById(R.id.codigo_pedido);
        codigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
            }
        });

        ventana.show();
    }

    public void onDialogRetraso(View view) {
        final Dialog ventana = new Dialog(this);
        ventana.setContentView(R.layout.dialog_pedido_retraso);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        opcRetra = -1;

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        MaterialBetterSpinner codigo = (MaterialBetterSpinner) ventana.findViewById(R.id.motino_novedad);

        final List<String> modos = new ArrayList<>();
        modos.add("Retraso por tráfico");
        modos.add("Accesos a la zona cerrado");
        modos.add("Motopipa accidentada");
        modos.add("Motopipa choco");
        modos.add("Motopipa la robaron");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.textview_letter_black, modos);
        arrayAdapter.setDropDownViewResource(R.layout.textview_letter_black);

        codigo.setAdapter(arrayAdapter);
        codigo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                opcRetra = i;
            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (opcRetra != -1) {
                    valores.clear();
                    valores.add("" + idPedido);
                    valores.add("Retraso");
                    valores.add(modos.get(opcRetra));

                    cambiarStatus();

                    ventana.dismiss();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes seleccionar el motivo", Toast.LENGTH_LONG).show();
                }

            }
        });

        ventana.show();
    }


    public void onDialogDevuelto(View view) {
        final Dialog ventana = new Dialog(this);
        ventana.setContentView(R.layout.dialog_pedido_retraso);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        TextView titulo = (TextView) ventana.findViewById(R.id.titulo);
        titulo.setText("Indicar Devolución");
        TextView label = (TextView) ventana.findViewById(R.id.label);
        label.setText("Motivo de la devolución");
        MaterialBetterSpinner codigo = (MaterialBetterSpinner) ventana.findViewById(R.id.motino_novedad);

        opcDev = -1;

        final List<String> modos = new ArrayList<>();
        for (int i = 0; i < motivoNoEntregas.size(); i++) {
            modos.add(motivoNoEntregas.get(i).getNombre());
        }
        /*
        modos.add("No se encontro al cliente");
        modos.add("No se encontro el domicilio");
            modos.add("No se pudo entrar al domicilio");
        modos.add("Ya compro a otro proveedor");
        */

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.textview_letter_black, modos);
        arrayAdapter.setDropDownViewResource(R.layout.textview_letter_black);

        codigo.setAdapter(arrayAdapter);
        codigo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                opcDev = i;
            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (opcDev != -1) {
                    valores.clear();
                    valores.add("" + idPedido);
                    valores.add("Devolución");
                    valores.add(modos.get(opcDev));

                    cambiarStatus();

                    ventana.dismiss();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes seleccionar el motivo", Toast.LENGTH_LONG).show();
                }

            }
        });

        ventana.show();
    }

    public void cambiarStatus() {
        new WebService(
                parametros,
                valores,
                ActivityCambiarEstatus.this,
                "POST",
                "pedidos_status",
                null,
                "Verificando datos de acceso",
                false
        ).execute();
    }

}
