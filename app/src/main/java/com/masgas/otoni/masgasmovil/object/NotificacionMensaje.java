package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 22/2/2018.
 */

public class NotificacionMensaje {

    // Datos de la Tabla
    public static final String TABLE = "notificacion_mensaje";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String MENSAJE = "mensaje";
    public static final String NUEVO = "nuevo";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private String mensaje;

    public NotificacionMensaje() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                MENSAJE + " TEXT, " +
                NUEVO + " BOOLEAN, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {

        Long id = (Long) pag.get("id");
        String tx_mensaje = (String) pag.get("tx_mensaje");

        this.setId(id.intValue());
        this.setMensaje(tx_mensaje);
    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean modificado) {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getMensaje() != null) {
            values.put(MENSAJE, this.getMensaje());
        }

        values.put(NUEVO, nuevo);
        values.put(MODIFICADO, modificado);

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
