package com.masgas.otoni.masgasmovil.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Item;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.util.List;

/**
 * Created by otoni on 1/2/2018.
 */

public class AdapterListaPedidos extends RecyclerView.Adapter<AdapterListaPedidos.ViewHolder>
        implements ItemClickListener {

    private List<Pedido> items;
    private Context context;
    public ModeloGeneral modeloRelacional;

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView id, fecha, estatus;

        // Interfaz de comunicación
        public ItemClickListener listener;

        public ViewHolder(View v, ItemClickListener listener) {
            super(v);
            id = (TextView) v.findViewById(R.id.pedido_identificador);
            fecha = (TextView) v.findViewById(R.id.pedido_fecha);
            estatus = (TextView) v.findViewById(R.id.pedido_estado);

            v.setOnClickListener(this);

            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }

    public AdapterListaPedidos(List<Pedido> items, Context context) {
        this.items = items;
        this.context = context;
        modeloRelacional = new ModeloGeneral(context);
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<Pedido> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {
        // Imagen a compartir entre transiciones
        //View sharedImage = view.findViewById(R.id.imagen);
        //ListaCuentasPorCobrar.launch((Activity) context, items.get(position));
        onDialogDetalle(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public AdapterListaPedidos.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_pedido, viewGroup, false);
        return new AdapterListaPedidos.ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(AdapterListaPedidos.ViewHolder viewHolder, int i) {
        //viewHolder.imagen.setImageResource(items.get(i).getImagen());
        viewHolder.id.setText("PED-" + items.get(i).getId());
        viewHolder.fecha.setText(Utilidades.getFechaCorta(items.get(i).getFecha()));
        viewHolder.estatus.setText(items.get(i).getEstatus());

        if (items.get(i).equals("Asignación")) {
            viewHolder.estatus.setTextColor(context.getResources().getColor(R.color.red));
        }
        else {
            viewHolder.estatus.setTextColor(context.getResources().getColor(R.color.grassy_green));
        }
    }

    public void onDialogDetalle(final Pedido pedido) {
        final Dialog ventana = new Dialog(context);
        ventana.setContentView(R.layout.dialog_pedido_detallado);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView fecha = (TextView) ventana.findViewById(R.id.dialog_pedido_fecha);
        TextView monto = (TextView) ventana.findViewById(R.id.dialog_pedido_monto);
        TextView tipo_pago = (TextView) ventana.findViewById(R.id.dialog_pedido_tipo_pago);
        TextView numero = (TextView) ventana.findViewById(R.id.dialog_pedido_numero);
        TextView motivo = (TextView) ventana.findViewById(R.id.dialog_pedido_motivo);
        TextView estatus = (TextView) ventana.findViewById(R.id.dialog_pedido_estatus);
        TextView recibir = (TextView) ventana.findViewById(R.id.dialog_recibir);

        if (!pedido.getEstatus().equals("Por entregar")) {
            recibir.setVisibility(View.GONE);
        }

        recibir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogQR(pedido);
            }
        });

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 3);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout linearLayout = (LinearLayout) ventana.findViewById(R.id.dialog_pedido_item_content);

        List<ViewItemCarrito> items = modeloRelacional.consultarItemsPorPedidoConProducto(pedido.getId());

        for (int i = 0; i < items.size(); i++) {
            LinearLayout linearLayout1 = new LinearLayout(context);

            TextView textView = new TextView(context);
            textView.setLayoutParams(params);
            textView.setTextSize(16);
            textView.setText(items.get(i).getProducto());

            TextView textView1 = new TextView(context);
            textView1.setLayoutParams(params2);
            textView1.setTextSize(16);
            textView1.setText(items.get(i).getCantidad() + "");

            linearLayout1.addView(textView);
            linearLayout1.addView(textView1);

            linearLayout.addView(linearLayout1);

        }

        fecha.setText(Utilidades.getFechaCorta(pedido.getFecha()));
        numero.setText("PED-" + pedido.getId());
        estatus.setText(pedido.getEstatus());
        if (pedido.getPagado()) {
            tipo_pago.setText("PayPal");
        }
        else {
            tipo_pago.setText("Por cobrar");
        }
        monto.setText("" + Utilidades.doubleToDollar(modeloRelacional.montoPedido(pedido.getId())));
        motivo.setText("Sin definir");

        ventana.show();
    }

    private void dialogQR(Pedido pedido) {
        final Dialog ventana = new Dialog(context);

        ventana.setContentView(R.layout.dialog_mensaje_confimacion);
        ventana.setCancelable(false);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        TextView codigo = (TextView) ventana.findViewById(R.id.dialog_one_accion_codigo_qr);
        TextView titulo = (TextView) ventana.findViewById(R.id.dialog_one_accion_titulo);
        TextView contenido = (TextView) ventana.findViewById(R.id.dialog_one_accion_contenido);
        ImageView qr = (ImageView) ventana.findViewById(R.id.dialog_one_accion_qr);

        titulo.setText("Código de Pedido");
        contenido.setText("Muestra el código QR al repartidor de tu pedido y permite que lo escaneé para concluir la entrega");

        if (pedido.getCodigo() != null) {
            codigo.setText(pedido.getCodigo());
        }

        try {
            if (pedido.getCodigo() != null) {
                qr.setImageBitmap(Utilidades.generarQR(pedido.getCodigo(), context));
            }
            else {
                //qr.setImageBitmap(Utilidades.generarQR("Prueba", context));
                Toast.makeText(context, "No hay codigo para generar QR", Toast.LENGTH_LONG).show();
            }

        } catch (WriterException e) {
            Log.e("WebService", e.toString());
            Toast.makeText(context, "Hubo un error al intentar generar el QR", Toast.LENGTH_LONG).show();
        }

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
                /*
                Intent intent = new Intent(context, contextNext);
                context.startActivity(intent);

                ((Activity) context).finish();
                */
            }
        });

        ventana.show();
    }
}
