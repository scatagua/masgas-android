package com.masgas.otoni.masgasmovil.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.zxing.WriterException;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.fragment.FragmentDatePicker;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.utils.WebService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.ParseException;

/**
 * Created by otoni on 3/1/2018.
 */

public class ActivityRegistrar extends AppCompatActivity {

    public static String TAG = "Registrar Cliente";

    LinearLayout contenedor, contFisico, contMoral;
    // Vistas cliente fisico
    EditText nombre, apellido, telfCel, telfLocal, correo, fechaNac, pass1, pass2, documentoIden, referencia, /*dir,*/ post, numInt, numExt, colonia, calle, municipio;
    TextInputLayout eN, eA, eTC, eTL, eC, eD, eFN, eP1, eP2, eDI, eP, eNI, eNE, eCol, eCll, eM;
    // Vistas cliente moral
    EditText efc, empresa, empTelfCel, empTelLocal, empCorreo, pass3, pass4, referencia2, /*dir2,*/ post2, numInt2, numExt2, colonia2, calle2, municipio2;
    TextInputLayout eEfc, eE, eER, eED, eETC, eETL, eEC, eP3, eP4, eEP, eENI, eENE, eECol, eCll2, eM2;

    Boolean isClienteFisico, isMasculino, isLocation = false;
    List<String> parametros, valores;

    // Manejo de Places
    PlaceAutocompleteFragment autocompleteFragment, autocompleteFragment2 /*, autocompleteFragment3, autocompleteFragment4*/;

    String domicilio, fechaNacimiento;
    Double lat, lng;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_registrar_cliente);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        dialogTerminosYCondiciones();

        contenedor = (LinearLayout) findViewById(R.id.registrar_contenedor_principal);
        contFisico = (LinearLayout) findViewById(R.id.registrar_contenedor_cliente_fisico);
        contMoral = (LinearLayout) findViewById(R.id.registrar_contenedor_cliente_moral);

        isClienteFisico = true;
        isMasculino = true;

        // Vistas Cliente fisico
        nombre = (EditText) findViewById(R.id.registrar_nombre);
        apellido = (EditText) findViewById(R.id.registrar_apellido);
        fechaNac = (EditText) findViewById(R.id.registrar_fecha_nac);
        telfCel = (EditText) findViewById(R.id.registrar_telefono_celular);
        telfLocal = (EditText) findViewById(R.id.registrar_telefono_local);
        pass1 = (EditText) findViewById(R.id.registrar_password1);
        pass2 = (EditText) findViewById(R.id.registrar_password2);
        correo = (EditText) findViewById(R.id.registrar_correo);
        documentoIden = (EditText) findViewById(R.id.registrar_documento_identidad);
        referencia = (EditText) findViewById(R.id.registrar_punto_referencia);
        //dir = (EditText) findViewById(R.id.registrar_domicilio);
        post = (EditText) findViewById(R.id.registrar_postal);
        numExt = (EditText) findViewById(R.id.registrar_num_ext);
        numInt = (EditText) findViewById(R.id.registrar_num_int);
        colonia = (EditText) findViewById(R.id.registrar_colonia);
        calle = (EditText) findViewById(R.id.registrar_calle);
        municipio = (EditText) findViewById(R.id.registrar_municipio);

        eN = (TextInputLayout) findViewById(R.id.registrar_nombre_error);
        eA = (TextInputLayout) findViewById(R.id.registrar_apellido_error);
        eFN = (TextInputLayout) findViewById(R.id.registrar_fecha_nac_error);
        eTC = (TextInputLayout) findViewById(R.id.registrar_telefono_celular_error);
        eTL = (TextInputLayout) findViewById(R.id.registrar_telefono_local_error);
        eP1 = (TextInputLayout) findViewById(R.id.registrar_password1_error);
        eP2 = (TextInputLayout) findViewById(R.id.registrar_password2_error);
        eC = (TextInputLayout) findViewById(R.id.registrar_correo_error);
        eDI = (TextInputLayout) findViewById(R.id.registrar_documento_identidad_error);
        eP = (TextInputLayout) findViewById(R.id.registrar_postal_error);
        eNE = (TextInputLayout) findViewById(R.id.registrar_num_ext_error);
        eNI = (TextInputLayout) findViewById(R.id.registrar_num_int_error);
        //eD = (TextInputLayout) findViewById(R.id.registrar_domicilio_error);
        eCol = (TextInputLayout) findViewById(R.id.registrar_colonia_error);
        eCll = (TextInputLayout) findViewById(R.id.registrar_calle_error);
        eM = (TextInputLayout) findViewById(R.id.registrar_municipio_error);

        // Vistas cliente moral
        empresa = (EditText) findViewById(R.id.registrar_empresa);
        empCorreo = (EditText) findViewById(R.id.registrar_correo2);
        empTelfCel = (EditText) findViewById(R.id.registrar_telefono_celular2);
        empTelLocal = (EditText) findViewById(R.id.registrar_telefono_local2);
        efc = (EditText) findViewById(R.id.registrar_rfc);
        pass3 = (EditText) findViewById(R.id.registrar_password3);
        pass4 = (EditText) findViewById(R.id.registrar_password4);
        referencia2 = (EditText) findViewById(R.id.registrar_punto_referencia2);
        //dir2 = (EditText) findViewById(R.id.registrar_domicilio2);
        post2 = (EditText) findViewById(R.id.registrar_postal2);
        numExt2 = (EditText) findViewById(R.id.registrar_num_ext2);
        numInt2 = (EditText) findViewById(R.id.registrar_num_int2);
        colonia2 = (EditText) findViewById(R.id.registrar_colonia2);
        calle2 = (EditText) findViewById(R.id.registrar_calle2);
        municipio2 = (EditText) findViewById(R.id.registrar_municipio2);

        eE = (TextInputLayout) findViewById(R.id.registrar_empresa_error);
        eEC = (TextInputLayout) findViewById(R.id.registrar_correo2_error);
        eETC = (TextInputLayout) findViewById(R.id.registrar_telefono_celular2_error);
        eETL = (TextInputLayout) findViewById(R.id.registrar_telefono_local2_error);
        eEfc = (TextInputLayout) findViewById(R.id.registrar_rfc_error);
        eP3 = (TextInputLayout) findViewById(R.id.registrar_password3_error);
        eP4 = (TextInputLayout) findViewById(R.id.registrar_password4_error);
        eEP = (TextInputLayout) findViewById(R.id.registrar_postal2_error);
        eENE = (TextInputLayout) findViewById(R.id.registrar_num_ext2_error);
        eENI = (TextInputLayout) findViewById(R.id.registrar_num_int2_error);
        //eED = (TextInputLayout) findViewById(R.id.registrar_domicilio2_error);
        eECol = (TextInputLayout) findViewById(R.id.registrar_colonia2_error);
        eCll2 = (TextInputLayout) findViewById(R.id.registrar_calle_error2);
        eM2 = (TextInputLayout) findViewById(R.id.registrar_municipio_error2);

        parametros = new ArrayList<>();

        valores = new ArrayList<>();

        efc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 13) {
                    Toast.makeText(getApplicationContext(), "El RFC no puede tener mas de 13 caracteres", Toast.LENGTH_LONG).show();
                    efc.setText(efc.getText().toString().substring(0, charSequence.length() - 1));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        // Autocomplete Cliente Fisico
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();
        autocompleteFragment.setFilter(typeFilter);

        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Registra tu ubicación con google map");
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                Log.i(TAG, "Place: " + place.getAddress());
                Log.i(TAG, "Place: " + place.getLatLng().latitude + ", " + place.getLatLng().longitude);
                isLocation = true;
                domicilio = place.getAddress().toString();
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
                isLocation = false;
            }
        });

        // Autocomplete Cliente Moral
        autocompleteFragment2 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment2);

        autocompleteFragment2.setFilter(typeFilter);

        ((EditText) autocompleteFragment2.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
        ((EditText) autocompleteFragment2.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Registra tu ubicación con google map");
        ((EditText) autocompleteFragment2.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

        autocompleteFragment2.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                Log.i(TAG, "Place: " + place.getAddress());
                Log.i(TAG, "Place: " + place.getLatLng().latitude + ", " + place.getLatLng().longitude);
                isLocation = true;
                domicilio = place.getAddress().toString();
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
                isLocation = false;
            }
        });
        /*
        autocompleteFragment3 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment3);

        ((EditText) autocompleteFragment3.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
        ((EditText) autocompleteFragment3.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Ingresa un punto de referencia");
        ((EditText) autocompleteFragment3.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

        autocompleteFragment3.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                Log.i(TAG, "Place: " + place.getAddress());
                Log.i(TAG, "Place: " + place.getLatLng().latitude + ", " + place.getLatLng().longitude);

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
                isLocation = false;
            }
        });

        autocompleteFragment4 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment4);

        ((EditText) autocompleteFragment4.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
        ((EditText) autocompleteFragment4.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Ingresa un punto de referencia");
        ((EditText) autocompleteFragment4.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

        autocompleteFragment4.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                Log.i(TAG, "Place: " + place.getAddress());
                Log.i(TAG, "Place: " + place.getLatLng().latitude + ", " + place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
                isLocation = false;
            }
        });
        */
    }

    public void redirectLogin(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityRegistrar.this, ActivityLogin.class);
                startActivity(intent);
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean marcado = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.registrar_tipo_cliente_fisico:
                if (marcado) {
                    contMoral.setVisibility(View.GONE);
                    contFisico.setVisibility(View.VISIBLE);
                    cambiarBackground(true);
                    isClienteFisico = true;
                }
                break;

            case R.id.registrar_tipo_cliente_moral:
                if (marcado) {
                    contFisico.setVisibility(View.GONE);
                    contMoral.setVisibility(View.VISIBLE);
                    cambiarBackground(false);
                    isClienteFisico = false;
                }
                break;
        }
    }

    public void cambiarBackground(Boolean fisico) {
        if (fisico) {
            contenedor.setBackgroundResource(R.drawable.gradient_morado);
        }
        else {
            contenedor.setBackgroundResource(R.drawable.gradient_naranja);
        }
    }

    public void onCambiarSexo(View view) {
        boolean marcado = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.registrar_sexo_masculino:
                if (marcado) {
                    isMasculino = true;
                }
                break;

            case R.id.registrar_sexo_femenino:
                if (marcado) {
                    isMasculino = false;
                }
                break;
        }
    }

    public void onShowDateDialog(View view) {

        switch (view.getId()) {
            case R.id.registrar_fecha_nac:
                showDatePickerDialog();
                break;

            case R.id.registrar_fecha_nac_img:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {
        FragmentDatePicker newFragment = FragmentDatePicker.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int day, int month, int year) {
                // +1 because january is zero
                final String selectedDate, d, m, ya;
                d = (day < 10) ? "0" + day : day + "";
                ya = (year < 9) ? "0" + (year+1) : (year+1) + "";
                m = (month < 9) ? "0" + (month+1) : (month+1) + "";

                fechaNacimiento = d + "-" + m + "-" + ya;

                fechaNac.setText(  ya + "-" +   m  + "-" + d);
            }
        });
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public void mostrarSnac(String mensaje) {
        Snackbar.make(contenedor, mensaje, Snackbar.LENGTH_LONG).show();
    }

    public void validarDatos(View view) {
        if (isClienteFisico) {
            if (nombre.getText().length() > 0) {
                if (apellido.getText().length() > 0) {
                    if (fechaNac.getText().length() > 0) {
                        if (Utilidades.isDateValid(fechaNac.getText().toString())) {
                            if (telfCel.getText().length() > 0 || telfLocal.getText().length() > 0) {
                                if (correo.getText().length() > 0) {
                                    if (pass1.getText().length() > 0) {
                                        if (Utilidades.isPasswordValid(pass1.getText().toString())) {
                                            if (pass2.getText().length() > 0) {
                                                if (Utilidades.isPasswordValid(pass2.getText().toString())) {
                                                    if (pass1.getText().toString().equals(pass2.getText().toString())) {
                                                        if (colonia.getText().length() > 0) {
                                                            if (calle.getText().length() > 0) {
                                                                if (municipio.getText().length() > 0) {
                                                                    if (numExt.getText().length() > 0) {
                                                                        parametros.clear();

                                                                        parametros.add("password");
                                                                        parametros.add("nombre1");
                                                                        parametros.add("apellido1");
                                                                        parametros.add("telefono_celular");
                                                                        parametros.add("telefono_local");
                                                                        parametros.add("correo");
                                                                        parametros.add("sexo");
                                                                        parametros.add("fecha_nacimiento");
                                                                        parametros.add("nu_identificacion");
                                                                        parametros.add("tipo_identificacion");
                                                                        parametros.add("direccion");
                                                                        parametros.add("direccion_longitud");
                                                                        parametros.add("direccion_latitud");
                                                                        parametros.add("punto_referencia");
                                                                        parametros.add("codigo_postal");
                                                                        parametros.add("numero_ext");
                                                                        parametros.add("numero_int");
                                                                        parametros.add("colonia");
                                                                        parametros.add("tx_calle");
                                                                        parametros.add("tx_municipio");

                                                                        valores.clear();

                                                                        valores.add(pass1.getText().toString());
                                                                        valores.add(nombre.getText().toString());
                                                                        valores.add(apellido.getText().toString());
                                                                        valores.add(telfCel.getText().toString());
                                                                        valores.add(telfLocal.getText().toString());
                                                                        valores.add(correo.getText().toString());
                                                                        if (isMasculino) {
                                                                            valores.add("Masculino");
                                                                        }
                                                                        else {
                                                                            valores.add("Femenino");
                                                                        }
                                                                        if (fechaNacimiento == null){
                                                                            fechaNacimiento = fechaNac.getText().toString();

                                                                            SimpleDateFormat fmt =  new SimpleDateFormat("dd-MM-yyyy");
                                                                            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd" );
                                                                            try {
                                                                                Date newDate = fmt.parse(fechaNacimiento);
                                                                                fechaNacimiento = targetFormat.format(newDate);
                                                                            } catch (ParseException ex) {
                                                                                // Handle Exception.
                                                                            }
                                                                        }
                                                                        valores.add(fechaNacimiento);
                                                                        valores.add(documentoIden.getText().toString());
                                                                        valores.add("1");
                                                                        if (isLocation) {
                                                                            valores.add(domicilio);
                                                                            //valores.add(dir.getText().toString());
                                                                            valores.add(lng + "");
                                                                            valores.add(lat + "");
                                                                        }
                                                                        else {
                                                                            valores.add(calle.getText().toString() + " Ext:" + numExt.getText().toString() + " " + numInt.getText().toString()  + ", " + colonia.getText().toString() + " " +  referencia.getText().toString());
                                                                            //valores.add(dir.getText().toString());
                                                                            valores.add(0.00 + "");
                                                                            valores.add(0.00 + "");
                                                                        }

                                                                        valores.add(referencia.getText().toString());
                                                                        valores.add(post.getText().toString());
                                                                        valores.add(numExt.getText().toString());
                                                                        valores.add(numInt.getText().toString());
                                                                        valores.add(colonia.getText().toString());
                                                                        valores.add(calle.getText().toString());
                                                                        valores.add(municipio.getText().toString());

                                                                        registrar();
                                                                    }
                                                                    else {
                                                                        mostrarSnac("Debes ingresar el número exterior");
                                                                        numExt.requestFocus();
                                                                    }
                                                                }
                                                                else {
                                                                    mostrarSnac("Debes ingresar el municipio");
                                                                    municipio.requestFocus();
                                                                }

                                                                //if (dir.getText().length() > 0) {
                                                                /*
                                                                if (referencia.getText().length() > 0) {

                                                                }
                                                                else {
                                                                    mostrarSnac("Debes indicar un punto de referencia");
                                                                    referencia.requestFocus();
                                                                }
                                                                */
                                                                    /*
                                                                    }
                                                                    else {
                                                                        mostrarSnac("Debes ingresar tu dirección");
                                                                        dir.requestFocus();
                                                                    }
                                                                    */
                                                            }
                                                            else {
                                                                mostrarSnac("Debes ingresar la calle");
                                                                calle.requestFocus();
                                                            }
                                                        }
                                                        else {
                                                            mostrarSnac("Debes indicar la colonia a la que perteneces");
                                                            colonia.requestFocus();
                                                        }

                                                    }
                                                    else {
                                                        mostrarSnac("Los password deben coincidir");
                                                        pass1.requestFocus();
                                                        eP1.setError("Los password deben coincidir");
                                                    }
                                                }
                                                else {
                                                    mostrarSnac("El password debe tener entre 6 a 8 caracteres");
                                                    pass2.requestFocus();
                                                    eP2.setError("El password debe tener entre 6 a 8 caracteres");
                                                }
                                            }
                                            else {
                                                mostrarSnac("Debes confirmar tu password");
                                                pass2.requestFocus();
                                                eP2.setError("Debes confirmar tu password");
                                            }
                                        }
                                        else {
                                            mostrarSnac("El password debe tener entre 6 a 8 caracteres");
                                            pass1.requestFocus();
                                            eP1.setError("El password debe tener entre 6 a 8 caracteres");
                                        }
                                    }
                                    else {
                                        mostrarSnac("Debes ingresar tu password");
                                        pass1.requestFocus();
                                        eP1.setError("Debes ingresar tu password");
                                    }
                                    /*
                                    if (isLocation) {

                                    }
                                    else {
                                        mostrarSnac("Debes ingresar tu domicilio");
                                    }
                                    */
                                }
                                else {
                                    mostrarSnac("Debes ingresar tu correo");
                                    correo.requestFocus();
                                    eC.setError("Debes ingresar tu correo");
                                }
                                /*
                                if (telfLocal.getText().length() > 0) {

                                }
                                else {
                                    mostrarSnac("Debes ingresar el numero de tu telefono fijo");
                                    telfLocal.requestFocus();
                                    eTL.setError("Debes ingresar el numero de tu telefono fijo");
                                }
                                */
                            }
                            else {
                                mostrarSnac("Debes ingresar al menos un telefono");
                                telfCel.requestFocus();
                                eTC.setError("Debes ingresar el numero de tu celular");
                            }
                        }
                        else {
                            mostrarSnac("El formato correcto para las fecha de nacimiento es 30-11-1999");
                        }

                    }
                    else {
                        mostrarSnac("Debes ingresar tu fecha de nacimiento");
                        fechaNac.requestFocus();
                        eFN.setError("Debes ingresar tu fecha de nacimiento");
                    }
                    /*
                    if (documentoIden.getText().length() > 0) {

                    }
                    else {
                        mostrarSnac("Debes ingresar tu documento de identidad");
                        documentoIden.requestFocus();
                        eDI.setError("Debes ingresar tu documento de identidad");
                    }
                    */
                }
                else {
                    mostrarSnac("Debes ingresar tu apellido");
                    apellido.requestFocus();
                    eA.setError("Debes ingresar tu apellido");
                }
            }
            else {
                mostrarSnac("Debes ingresar tu nombre");
                nombre.requestFocus();
                eN.setError("Debes ingresar tu nombre");
            }
        }
        else {
            if (empresa.getText().length() > 0) {
                if (efc.getText().length() > 0) {
                    if (efc.getText().length() > 11 && efc.getText().length() < 14) {
                        if (empTelfCel.getText().length() > 0 || empTelLocal.getText().length() > 0) {
                            if (empCorreo.getText().length() > 0) {
                                if (pass3.getText().length() > 0) {
                                    if (Utilidades.isPasswordValid(pass3.getText().toString())) {
                                        if (pass4.getText().length() > 0) {
                                            if (Utilidades.isPasswordValid(pass4.getText().toString())) {
                                                if (pass3.getText().toString().equals(pass4.getText().toString())) {
                                                    if (colonia2.getText().length() > 0) {
                                                        if (calle2.getText().length() > 0) {
                                                            if (municipio2.getText().length() > 0) {
                                                                if (numExt2.getText().length() > 0) {
                                                                    parametros.clear();

                                                                    parametros.add("password");
                                                                    parametros.add("empresa");
                                                                    parametros.add("telefono_celular");
                                                                    parametros.add("telefono_local");
                                                                    parametros.add("correo");
                                                                    parametros.add("nu_identificacion");
                                                                    parametros.add("direccion");
                                                                    parametros.add("direccion_longitud");
                                                                    parametros.add("direccion_latitud");
                                                                    parametros.add("punto_referencia");
                                                                    parametros.add("codigo_postal");
                                                                    parametros.add("numero_ext");
                                                                    parametros.add("numero_int");
                                                                    parametros.add("colonia");
                                                                    parametros.add("tx_calle");
                                                                    parametros.add("tx_municipio");

                                                                    valores.clear();

                                                                    valores.add(pass3.getText().toString());
                                                                    valores.add(empresa.getText().toString());
                                                                    valores.add(empTelfCel.getText().toString());
                                                                    valores.add(empTelLocal.getText().toString());
                                                                    valores.add(empCorreo.getText().toString());
                                                                    valores.add(efc.getText().toString());
                                                                    if (isLocation) {
                                                                        valores.add(domicilio);
                                                                        //valores.add(dir2.getText().toString());
                                                                        valores.add(lng + "");
                                                                        valores.add(lat + "");
                                                                    }
                                                                    else {
                                                                        valores.add("No se indico");
                                                                        //valores.add(dir2.getText().toString());
                                                                        valores.add(0.00 + "");
                                                                        valores.add(0.00 + "");
                                                                    }

                                                                    valores.add(referencia2.getText().toString());
                                                                    valores.add(post2.getText().toString());
                                                                    valores.add(numExt2.getText().toString());
                                                                    valores.add(numInt2.getText().toString());
                                                                    valores.add(colonia2.getText().toString());
                                                                    valores.add(calle2.getText().toString());
                                                                    if (municipio2.getText().length() > 0) {
                                                                        valores.add(municipio2.getText().toString());
                                                                    }
                                                                    else {
                                                                        valores.add("");
                                                                    }

                                                                    registrar();
                                                                    //if (dir2.getText().length() > 0) {
                                                            /*
                                                            if (referencia2.getText().length() > 0) {

                                                            }
                                                            else {
                                                                mostrarSnac("Debes ingresar un punto de referencia");
                                                                referencia2.requestFocus();
                                                            }
                                                            */
                                                        /*
                                                        }
                                                        else {
                                                            mostrarSnac("Debes ingresar tu dirección");
                                                            dir2.requestFocus();
                                                        }
                                                        */
                                                                }
                                                                else {
                                                                    mostrarSnac("Debes ingresar el número exterior");
                                                                    numExt.requestFocus();
                                                                }
                                                            }
                                                            else {
                                                                mostrarSnac("Debes ingresar el municipio");
                                                                municipio.requestFocus();
                                                            }
                                                        }
                                                        else {
                                                            mostrarSnac("Debes ingresar la calle");
                                                            calle2.requestFocus();
                                                        }

                                                    }
                                                    else {
                                                        mostrarSnac("Debes ingresar la colonia a la que perteneces");
                                                        post2.requestFocus();
                                                    }
                                                }
                                                else {
                                                    mostrarSnac("Los password ingresados no coinciden");
                                                    pass3.requestFocus();
                                                    eP3.setError("Los password ingresados no coinciden");
                                                }
                                            }
                                            else {
                                                mostrarSnac("El password debe tener una longitud de 6 a 8 caracteres");
                                                pass4.requestFocus();
                                                eP4.setError("El password debe tener una longitud de 6 a 8 caracteres");
                                            }
                                        }
                                        else {
                                            mostrarSnac("Debes confirmar el password");
                                            pass4.requestFocus();
                                            eP4.setError("Debes confirmar el password");
                                        }
                                    }
                                    else {
                                        mostrarSnac("El password debe tener una longitud de 6 a 8 caracteres");
                                        pass3.requestFocus();
                                        eP3.setError("El password debe tener una longitud de 6 a 8 caracteres");
                                    }
                                }
                                else {
                                    mostrarSnac("Debes ingresar el password");
                                    pass3.requestFocus();
                                    eP3.setError("Debes ingresar el password");
                                }
                            }
                            else {
                                mostrarSnac("Debes ingresar el correo de la empresa");
                                empCorreo.requestFocus();
                                eEC.setError("Debes ingresar el correo de la empresa");
                            }
                            /*
                            if (empTelLocal.getText().length() > 0) {

                            }
                            else {
                                mostrarSnac("Debes ingresar el numero fijo de la empresa");
                                empTelLocal.requestFocus();
                                eETL.setError("Debes ingresar el numero fijo de la empresa");
                            }
                            */
                        }
                        else {
                            mostrarSnac("Debes ingresar al menos un telefono");
                            empTelfCel.requestFocus();
                            eETC.setError("Debes ingresar el numero celular");
                        }
                    }
                    else {
                        mostrarSnac("El RFC debe tener entre 12 y 13 caracteres");
                        efc.requestFocus();
                        eEfc.setError("El RFC debe tener entre 12 y 13 caracteres");
                    }

                    /*
                    if (isLocation) {

                    }
                    else {
                        mostrarSnac("Debes ingresar el domicilio");
                    }
                    */
                }
                else {
                    mostrarSnac("Debes ingresar el RFC");
                    efc.requestFocus();
                    eEfc.setError("Debes ingresar el RFC");
                }
            }
            else {
                mostrarSnac("Debes ingresar el nombre de la empresa");
                empresa.requestFocus();
                eE.setError("Debes ingresar el nombre de la empresa");
            }
        }
    }

    public void registrar() {
        if (isClienteFisico) {
            WebService webService = new WebService(
                    parametros,
                    valores,
                    ActivityRegistrar.this,
                    "POST",
                    "cliente",
                    ActivityLogin.class,
                    "Registrando cliente",
                    true
            );
            webService.execute();
        }
        else {
            WebService webService = new WebService(
                    parametros,
                    valores,
                    ActivityRegistrar.this,
                    "POST",
                    "cliente_moral",
                    ActivityLogin.class,
                    "Registrando cliente",
                    true
            );
            webService.execute();
        }
    }

    public void dialogConfirmacion() {
        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.dialog_mensaje_confimacion);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) dialog.findViewById(R.id.dialog_aceptar);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent intent = new Intent(ActivityRegistrar.this, ActivityLogin.class);
                startActivity(intent);

                finish();
            }
        });

        dialog.show();
    }

    private void dialogTerminosYCondiciones() {
        final Dialog ventana = new Dialog(this);

        ventana.setContentView(R.layout.dialog_terminos_condiciones);
        ventana.setCancelable(false);
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView aceptar = (TextView) ventana.findViewById(R.id.dialog_aceptar);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
            }
        });

        TextView cancelar = (TextView) ventana.findViewById(R.id.dialog_cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ventana.dismiss();
                Toast.makeText(getApplicationContext(), "Debes aceptar los terminos y condiciones para poder registrarte en el sistema", Toast.LENGTH_LONG).show();
                finish();
            }
        });

        ventana.show();
    }
}
