package com.masgas.otoni.masgasmovil.view;

/**
 * Created by otoni on 17/1/2018.
 */

public class ViewItemCarrito {

    private Integer idItem;
    private Integer idProducto;
    private Integer idProductoTipo;
    private String producto;
    private Double precion;
    private Double precioIndependiente;
    private Integer cantidad;
    private Integer inventario;

    public ViewItemCarrito() {

    }

    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Double getPrecion() {
        return precion;
    }

    public void setPrecion(Double precion) {
        this.precion = precion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioIndependiente() {
        return precioIndependiente;
    }

    public void setPrecioIndependiente(Double precioIndependiente) {
        this.precioIndependiente = precioIndependiente;
    }

    public Integer getInventario() {
        return inventario;
    }

    public void setInventario(Integer inventario) {
        this.inventario = inventario;
    }

    public Integer getIdProductoTipo() {
        return idProductoTipo;
    }

    public void setIdProductoTipo(Integer idProductoTipo) {
        this.idProductoTipo = idProductoTipo;
    }
}
