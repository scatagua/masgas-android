package com.masgas.otoni.masgasmovil.object;

/**
 * Created by otoni on 23/2/2018.
 */

public class DistanceGoogleDirecctions {

    public String text;
    public int value;

    public DistanceGoogleDirecctions(String text, int value) {
        this.text = text;
        this.value = value;
    }

}
