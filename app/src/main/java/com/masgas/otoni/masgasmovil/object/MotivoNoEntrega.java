package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

public class MotivoNoEntrega {

    // Datos de la Tabla
    public static final String TABLE = "motivc_no_entrega";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String NOMBRE = "nombre";

    private Integer id;
    private String nombre;

    public MotivoNoEntrega() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY," +
                NOMBRE + " TEXT " +
                ")";
    }

    public void registrarJson(JSONObject pag) {

        Long id = (Long) pag.get("id");
        String nombre = (String) pag.get("nombre");

        this.setId(id.intValue());
        this.setNombre(nombre);
    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean modificado) {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }


        if (this.getNombre() != null) {
            values.put(NOMBRE, this.getNombre());
        }

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
