package com.masgas.otoni.masgasmovil.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.fragment.FragmentDatePicker;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.utils.WebService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otoni on 31/1/2018.
 */

public class ActivityRegistrarDistribuidor extends AppCompatActivity {

    public static String TAG = "Registrar Repartidor";

    LinearLayout contenedor, contFisico, contMoral;
    // Vistas cliente fisico
    EditText nombre, apellido, telfCel, telfLocal, correo, fechaNac, pass1, pass2, documentoIden;
    TextInputLayout eN, eA, eTC, eTL, eC, eD, eFN, eP1, eP2, eDI;

    Boolean isMasculino, isLocation = false;
    List<String> parametros, valores;

    // Manejo de Places
    PlaceAutocompleteFragment autocompleteFragment;

    String domicilio;
    Double lat, lng;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_registrar_cliente);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        contenedor = (LinearLayout) findViewById(R.id.registrar_contenedor_principal);
        contFisico = (LinearLayout) findViewById(R.id.registrar_contenedor_cliente_fisico);
        contMoral = (LinearLayout) findViewById(R.id.registrar_contenedor_cliente_moral);

        isMasculino = true;

        // Vistas Cliente fisico
        nombre = (EditText) findViewById(R.id.registrar_nombre);
        apellido = (EditText) findViewById(R.id.registrar_apellido);
        fechaNac = (EditText) findViewById(R.id.registrar_fecha_nac);
        telfCel = (EditText) findViewById(R.id.registrar_telefono_celular);
        telfLocal = (EditText) findViewById(R.id.registrar_telefono_local);
        pass1 = (EditText) findViewById(R.id.registrar_password1);
        pass2 = (EditText) findViewById(R.id.registrar_password2);
        correo = (EditText) findViewById(R.id.registrar_correo);
        documentoIden = (EditText) findViewById(R.id.registrar_documento_identidad);

        eN = (TextInputLayout) findViewById(R.id.registrar_nombre_error);
        eA = (TextInputLayout) findViewById(R.id.registrar_apellido_error);
        eFN = (TextInputLayout) findViewById(R.id.registrar_fecha_nac_error);
        eTC = (TextInputLayout) findViewById(R.id.registrar_telefono_celular_error);
        eTL = (TextInputLayout) findViewById(R.id.registrar_telefono_local_error);
        eP1 = (TextInputLayout) findViewById(R.id.registrar_password1_error);
        eP2 = (TextInputLayout) findViewById(R.id.registrar_password2_error);
        eC = (TextInputLayout) findViewById(R.id.registrar_correo_error);
        eDI = (TextInputLayout) findViewById(R.id.registrar_documento_identidad_error);

        parametros = new ArrayList<>();

        valores = new ArrayList<>();

        // Autocomplete Cliente Fisico
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Ingresa tu domicilio");
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                isLocation = true;
                domicilio = place.getAddress().toString();
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
                isLocation = false;
            }
        });
    }

    public void redirectLogin(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityRegistrarDistribuidor.this, ActivityLogin.class);
                startActivity(intent);
            }
        });
    }

    public void onCambiarSexo(View view) {
        boolean marcado = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.registrar_sexo_masculino:
                if (marcado) {
                    isMasculino = true;
                }
                break;

            case R.id.registrar_sexo_femenino:
                if (marcado) {
                    isMasculino = false;
                }
                break;
        }
    }

    public void onShowDateDialog(View view) {

        switch (view.getId()) {
            case R.id.registrar_fecha_nac:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {
        FragmentDatePicker newFragment = FragmentDatePicker.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate;
                if (month < 9) {
                    selectedDate = year + "-0" + (month+1) + "-" + day;
                }
                else {
                    selectedDate = year + "-" + (month+1) + "-" + day;
                }

                fechaNac.setText(selectedDate);
            }
        });
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public void mostrarSnac(String mensaje) {
        Snackbar.make(contenedor, mensaje, Snackbar.LENGTH_LONG).show();
    }

    public void validarDatos(View view) {
        if (nombre.getText().length() > 0) {
            if (apellido.getText().length() > 0) {
                if (documentoIden.getText().length() > 0) {
                    if (fechaNac.getText().length() > 0) {
                        if (telfCel.getText().length() > 0) {
                            if (telfLocal.getText().length() > 0) {
                                if (correo.getText().length() > 0) {
                                    if (isLocation) {
                                        if (pass1.getText().length() > 0) {
                                            if (Utilidades.isPasswordValid(pass1.getText().toString())) {
                                                if (pass2.getText().length() > 0) {
                                                    if (Utilidades.isPasswordValid(pass2.getText().toString())) {
                                                        if (pass1.getText().toString().equals(pass2.getText().toString())) {
                                                            parametros.clear();

                                                            parametros.add("password");
                                                            parametros.add("nombre1");
                                                            parametros.add("apellido1");
                                                            parametros.add("telefono_celular");
                                                            parametros.add("telefono_local");
                                                            parametros.add("correo");
                                                            parametros.add("sexo");
                                                            parametros.add("fecha_nacimiento");
                                                            parametros.add("identificacion");
                                                            parametros.add("tipo_identificacion");
                                                            parametros.add("direccion");
                                                            parametros.add("direccion_longitud");
                                                            parametros.add("direccion_latitud");

                                                            valores.clear();

                                                            valores.add(pass1.getText().toString());
                                                            valores.add(nombre.getText().toString());
                                                            valores.add(apellido.getText().toString());
                                                            valores.add(telfCel.getText().toString());
                                                            valores.add(telfLocal.getText().toString());
                                                            valores.add(correo.getText().toString());
                                                            if (isMasculino) {
                                                                valores.add("Masculino");
                                                            }
                                                            else {
                                                                valores.add("Femenino");
                                                            }
                                                            valores.add(fechaNac.getText().toString());
                                                            valores.add(documentoIden.getText().toString());
                                                            valores.add("1");
                                                            valores.add(domicilio);
                                                            valores.add(lng + "");
                                                            valores.add(lat + "");

                                                            registrar();
                                                        }
                                                        else {
                                                            mostrarSnac("Los password deben coincidir");
                                                            pass1.requestFocus();
                                                            eP1.setError("Los password deben coincidir");
                                                        }
                                                    }
                                                    else {
                                                        mostrarSnac("El password debe tener entre 6 a 8 caracteres");
                                                        pass2.requestFocus();
                                                        eP2.setError("El password debe tener entre 6 a 8 caracteres");
                                                    }
                                                }
                                                else {
                                                    mostrarSnac("Debes confirmar tu password");
                                                    pass2.requestFocus();
                                                    eP2.setError("Debes confirmar tu password");
                                                }
                                            }
                                            else {
                                                mostrarSnac("El password debe tener entre 6 a 8 caracteres");
                                                pass1.requestFocus();
                                                eP1.setError("El password debe tener entre 6 a 8 caracteres");
                                            }
                                        }
                                        else {
                                            mostrarSnac("Debes ingresar tu password");
                                            pass1.requestFocus();
                                            eP1.setError("Debes ingresar tu password");
                                        }
                                    }
                                    else {
                                        mostrarSnac("Debes ingresar tu domicilio");
                                    }
                                }
                                else {
                                    mostrarSnac("Debes ingresar tu correo");
                                    correo.requestFocus();
                                    eC.setError("Debes ingresar tu correo");
                                }
                            }
                            else {
                                mostrarSnac("Debes ingresar el numero de tu telefono fijo");
                                telfLocal.requestFocus();
                                eTL.setError("Debes ingresar el numero de tu telefono fijo");
                            }
                        }
                        else {
                            mostrarSnac("Debes ingresar el numero de tu celular");
                            telfCel.requestFocus();
                            eTC.setError("Debes ingresar el numero de tu celular");
                        }
                    }
                    else {
                        mostrarSnac("Debes ingresar tu fecha de nacimiento");
                        fechaNac.requestFocus();
                        eFN.setError("Debes ingresar tu fecha de nacimiento");
                    }
                }
                else {
                    mostrarSnac("Debes ingresar tu documento de identidad");
                    documentoIden.requestFocus();
                    eDI.setError("Debes ingresar tu documento de identidad");
                }
            }
            else {
                mostrarSnac("Debes ingresar tu apellido");
                apellido.requestFocus();
                eA.setError("Debes ingresar tu apellido");
            }
        }
        else {
            mostrarSnac("Debes ingresar tu nombre");
            nombre.requestFocus();
            eN.setError("Debes ingresar tu nombre");
        }
    }

    public void registrar() {
        WebService webService = new WebService(
                parametros,
                valores,
                ActivityRegistrarDistribuidor.this,
                "POST",
                "cliente",
                ActivityLogin.class,
                "Registrando cliente",
                true
        );
        webService.execute();
    }
}
