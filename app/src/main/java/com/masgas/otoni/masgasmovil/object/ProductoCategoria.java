package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 26/1/2018.
 */

public class ProductoCategoria {

    // Datos de la Tabla
    public static final String TABLE = "producto_categoria";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String NOMBRE = "nombre";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private String nombre;

    public ProductoCategoria() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                NOMBRE + " TEXT, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {
        Long id_articulo_categoria = (Long) pag.get("id_articulo_categoria");
        String categoria = (String) pag.get("categoria");

        this.setId(id_articulo_categoria.intValue());
        this.setNombre(categoria);
    }

    public ContentValues crearRegistro() {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getNombre() != null) {
            values.put(NOMBRE, this.getNombre());
        }

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
