package com.masgas.otoni.masgasmovil.interfaz;

import com.masgas.otoni.masgasmovil.object.RouteGoogleDirecctions;

import java.util.List;

/**
 * Created by otoni on 23/2/2018.
 */

public interface DirecctionsListener {

    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<RouteGoogleDirecctions> route);
}
