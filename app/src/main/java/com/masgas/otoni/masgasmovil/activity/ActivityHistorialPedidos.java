package com.masgas.otoni.masgasmovil.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaPedidos;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.object.Usuario;

import java.util.List;

/**
 * Created by otoni on 1/2/2018.
 */

public class ActivityHistorialPedidos extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private RecyclerView recycler;
    private AdapterListaPedidos adapter;
    private RecyclerView.LayoutManager lManager;
    private SwipeRefreshLayout refreshLayout;
    private NavigationView navigationView;
    Toolbar toolbar;
    TextView nombre;

    ModeloGeneral modeloRelacional;

    List<Pedido> pedidos;

    Usuario usuario;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_menu_historial_pedidos);

        modeloRelacional = new ModeloGeneral(this);

        setToolbar();

        setupWindowAnimations();

        // Menu Lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.menu_navegacion_menu_principal_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        usuario = modeloRelacional.consultarUsuarioActivo();

        //Log.e("Usuario", usuario.getId() + " " + usuario.getTelfCelular());

        View hview = navigationView.getHeaderView(0);
        nombre = (TextView) hview.findViewById(R.id.menu_lateral_nombre);
        nombre.setText(usuario.getNombre1() + " " + usuario.getApellido1());

        setTitle("Historial");

        if (usuario.getIdRol() == 2) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_lateral_distribuidor);
        }

        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        pedidos = modeloRelacional.consultarPedidos();

        adapter = new AdapterListaPedidos(pedidos, this);
        recycler.setAdapter(adapter);
    }

    private void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT > 20) {
            getWindow().setReenterTransition(new Explode());
            getWindow().setExitTransition(new Explode().setDuration(500));
        }
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_icono) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            Intent intent = new Intent(ActivityHistorialPedidos.this, ActivityPerfil.class);
            startActivity(intent);
        }
        if (id == R.id.nav_pedidos) {
            //Intent intent = new Intent(ActivityHistorialPedidos.this, ActivityHistorialPedidos.class);
            //startActivity(intent);
        }
        if (id == R.id.nav_novedades) {
            Intent intent = new Intent(ActivityHistorialPedidos.this, ActivityNovedades.class);
            startActivity(intent);
            //Snackbar.make(tabLayout, "Ya estas en este segmento", Snackbar.LENGTH_LONG).show();
        }
        if (id == R.id.nav_pedidos_solicitar) {
            Intent intent = new Intent(ActivityHistorialPedidos.this, ActivityPedido.class);
            startActivity(intent);
        }
        /*
        if (id == R.id.nav_historial) {
            Intent intent = new Intent(ActivityPedido.this, ActivityMenu.class);
            startActivity(intent);
        }
        */
        else if (id == R.id.nav_logout) {
            modeloRelacional.deleteAll();
            startActivity(new Intent(getBaseContext(), ActivityLogin.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
