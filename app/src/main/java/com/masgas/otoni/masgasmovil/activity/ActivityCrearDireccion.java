package com.masgas.otoni.masgasmovil.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.masgas.otoni.masgasmovil.R;

/**
 * Created by otoni on 28/1/2018.
 */

public class ActivityCrearDireccion extends AppCompatActivity {

    // Manejo de Places
    PlaceAutocompleteFragment autocompleteFragment;
    Button crear;
    LinearLayout content;

    String domicilio;
    Double lat, lng;
    Boolean isLocation = false;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.dialog_registrar_direccion);

        //content = (LinearLayout) findViewById(R.id.dialog_registrar_direccion_content);
        //content.setBackgroundColor(Color.TRANSPARENT);

        crear = (Button) findViewById(R.id.dialog_registrar_direccion_guardar);
        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLocation) {
                    Toast.makeText(getApplicationContext(), "Se guardo", Toast.LENGTH_LONG).show();
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Debes ingresar una direccion", Toast.LENGTH_LONG).show();
                }
            }
        });

        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16);
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Ingresa tu domicilio");
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setPadding(4, 8, 8, 8);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                isLocation = true;
                domicilio = place.getAddress().toString();
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                isLocation = false;
            }
        });
    }
}
