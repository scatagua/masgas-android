package com.masgas.otoni.masgasmovil.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.view.ViewBuzonNotificaciones;
import com.masgas.otoni.masgasmovil.view.ViewOrdenesPago;

import java.util.List;

/**
 * Created by otoni on 5/3/2018.
 */

public class AdapterListaOrdenesPAgo extends RecyclerView.Adapter<AdapterListaOrdenesPAgo.ViewHolder> {

    private List<ViewOrdenesPago> items;
    private Context context;
    public ModeloGeneral modeloRelacional;

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView nombre, contenido, fecha, numero;

        // Interfaz de comunicación
        public ItemClickListener listener;

        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.pedido_identificador);
            contenido = (TextView) v.findViewById(R.id.pedido_ruta);
            fecha = (TextView) v.findViewById(R.id.pedido_fecha);
            numero = (TextView) v.findViewById(R.id.pedido_estado);
        }

        @Override
        public void onClick(View v) {
            //listener.onItemClick(v, getAdapterPosition());
        }
    }

    public AdapterListaOrdenesPAgo(List<ViewOrdenesPago> items, Context context) {
        this.items = items;
        this.context = context;
        modeloRelacional = new ModeloGeneral(context);
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<ViewOrdenesPago> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public AdapterListaOrdenesPAgo.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_ordenes_pago, viewGroup, false);
        return new AdapterListaOrdenesPAgo.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final AdapterListaOrdenesPAgo.ViewHolder viewHolder, final int position) {
        viewHolder.nombre.setText(items.get(position).getNumero());
        viewHolder.contenido.setText(items.get(position).getPrecio());
        viewHolder.fecha.setText(items.get(position).getFecha());
        viewHolder.numero.setText(items.get(position).getEstatus());
        if (items.get(position).getEstatus().equals("Pendiente")) {
            viewHolder.numero.setTextColor(this.context.getResources().getColor(R.color.red));
        }
        else if (items.get(position).getEstatus().equals("Procesada")) {
            viewHolder.numero.setTextColor(this.context.getResources().getColor(R.color.grassy_green));
        }

    }
}
