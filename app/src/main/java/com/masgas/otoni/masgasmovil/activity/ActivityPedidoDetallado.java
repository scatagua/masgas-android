package com.masgas.otoni.masgasmovil.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.interfaz.DirecctionsListener;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.ConstantsGoogleDirection;
import com.masgas.otoni.masgasmovil.object.Direccion;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.object.RouteGoogleDirecctions;
import com.masgas.otoni.masgasmovil.utils.DirectionsGoogleMaps;
import com.masgas.otoni.masgasmovil.utils.MejorLocalizacion;
import com.masgas.otoni.masgasmovil.utils.Utilidades;
import com.masgas.otoni.masgasmovil.view.ViewItemCarrito;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otoni on 22/2/2018.
 */

public class ActivityPedidoDetallado extends AppCompatActivity implements OnMapReadyCallback,
        DirecctionsListener {

    private ProgressDialog progressDialog;

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();

    GoogleMap map;

    MapFragment mapFragment;
    Marker marker;

    Double mapLat, mapLng;
    Integer idDireccion, idPedido;

    TextView dir, monto, cliente, referencia;
    LinearLayout content;

    ModeloGeneral modeloGeneral;
    Direccion direccion;
    Pedido pedido;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_pedido_detallado);

        Bundle extra = getIntent().getExtras();
        if (!extra.containsKey("id_direccion")) {
            Toast.makeText(getApplicationContext(), "No hay direccion para este pedido" , Toast.LENGTH_LONG).show();
            finish();
        }

        modeloGeneral = new ModeloGeneral(this);
        idDireccion = extra.getInt("id_direccion");
        idPedido = extra.getInt("id_pedido");

        direccion = modeloGeneral.consultarDireccion(idDireccion);

        MejorLocalizacion mejorLocalizacion = new MejorLocalizacion(ActivityPedidoDetallado.this);
        Location location = mejorLocalizacion.getLocation();

        if (location != null) {
            mapLat = location.getLatitude();
            mapLng = location.getLongitude();
        }
        else {
            mejorLocalizacion.ejecutarProceso();
            location = mejorLocalizacion.getLocation();
            if (location != null) {
                //wait = false;
                Toast.makeText(getApplicationContext(), "Salimos del bucle", Toast.LENGTH_LONG).show();
            }
            /*
            Boolean wait = true;
            while (wait) {
                try {
                    Thread.sleep(3000);
                    mejorLocalizacion.ejecutarProceso();
                    location = mejorLocalizacion.getLocation();
                    if (location != null) {
                        wait = false;
                        Toast.makeText(getApplicationContext(), "Salimos del bucle", Toast.LENGTH_LONG).show();
                    }
                }
                catch (InterruptedException exp) {
                    Log.e("Error", exp.toString());
                }
            }
            */
        }


        pedido = modeloGeneral.consultarPedido(idPedido);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

        final SupportMapFragment supportMapFragment;
        if (Build.VERSION.SDK_INT < 21) {
            supportMapFragment = (SupportMapFragment) this
                    .getSupportFragmentManager().findFragmentById(R.id.map);
        } else {
            supportMapFragment = (SupportMapFragment) this
                    .getSupportFragmentManager().findFragmentById(R.id.map);
        }
        supportMapFragment.getMapAsync(this);

        dir = (TextView) findViewById(R.id.contenido);
        referencia = (TextView) findViewById(R.id.referencia);
        monto = (TextView) findViewById(R.id.monto);
        cliente = (TextView) findViewById(R.id.cliente);
        content = (LinearLayout) findViewById(R.id.content_item);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 3);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        List<ViewItemCarrito> itemCarritos = modeloGeneral.consultarItemsPorPedidoConProducto(pedido.getId());

        for (int i = 0; i < itemCarritos.size(); i++) {
            LinearLayout linearLayout1 = new LinearLayout(this);

            TextView textView = new TextView(this);
            textView.setLayoutParams(params);
            textView.setTextSize(16);
            textView.setText(itemCarritos.get(i).getProducto());

            TextView textView1 = new TextView(this);
            textView1.setLayoutParams(params2);
            textView1.setTextSize(16);
            textView1.setText(itemCarritos.get(i).getCantidad() + "");

            linearLayout1.addView(textView);
            linearLayout1.addView(textView1);

            content.addView(linearLayout1);
        }

        monto.setText(Utilidades.doubleToDollar(modeloGeneral.montoPedido(pedido.getId())));
        cliente.setText("Otoniel Marquez");

        if (direccion != null) {
            dir.setText(direccion.getDenominacion());
            if (direccion.getDenominacion() != null) {
                referencia.setText(direccion.getRefencia());
            }
            else {
                referencia.setText("Sin referencia");
            }
        }

        if (location != null) {
            sendRequest();
        }

        setTitle("Detalle de pedido");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (mapLat != null && mapLng != null) {
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.logo_color);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);

            marker = map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                    .title("Posición del Cliente")
                    .draggable(true)
                    .position(new LatLng(mapLat, mapLng)));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mapLat, mapLng))
                    .tilt(45)
                    .zoom(16)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    mapLng = marker.getPosition().longitude;
                    mapLat = marker.getPosition().latitude;
                }
            });
        }
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Por favor espere.",
                "Estamos buscando la dirección ingresada!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }

        }
    }

    @Override
    public void onDirectionFinderSuccess(List<RouteGoogleDirecctions> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        MejorLocalizacion localizacion = new MejorLocalizacion(this);
        Location l = localizacion.getLocation();

        if (l != null) {
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.pointer);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, 80, 80, false);

            for (RouteGoogleDirecctions route : routes) {
                //map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));

                marker.setPosition(new LatLng(l.getLatitude(), l.getLongitude()));

                destinationMarkers.add(
                        map.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                                .title(route.endAddress)
                                .position(route.endLocation)
                        )
                );

            /*
            destinoCoordenadas = route.endLocation;
            origenCoordenads = route.startLocation;

            orig = route.startAddress;
            dest = route.endAddress;

            distancia.setText("Distancia: " + route.distance.text);
            duracion.setText("Duración: " + route.duration.text);

            contentHeader.setVisibility(View.VISIBLE);
            */
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.endLocation, 16));

                PolylineOptions polylineOptions = new PolylineOptions().
                        geodesic(true).
                        color(Color.BLUE).
                        width(10);

                for (int i = 0; i < route.points.size(); i++)
                    polylineOptions.add(route.points.get(i));

                polylinePaths.add(map.addPolyline(polylineOptions));
            }
        }
    }

    private void sendRequest() {
        MejorLocalizacion mejorLocalizacion = new MejorLocalizacion(this);
        Location location = mejorLocalizacion.getLocation();

        String origin = location.getLatitude() + "," + location.getLongitude();
        String destination;

        if (direccion.getDenominacion() != null) {
            destination = direccion.getDenominacion();
        }
        else {
            destination = direccion.getLat() + "," + direccion.getLng();
        }

        if (destination.isEmpty()) {
            Toast.makeText(this, "Error con las coordenadas!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            new DirectionsGoogleMaps(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public class ActivityDetectionBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int type = intent.getIntExtra(ConstantsGoogleDirection.ACTIVITY_KEY, -1);
        }
    }

    public void reportarNovedad(View view) {
        Intent intent = new Intent(ActivityPedidoDetallado.this, ActivityNovedades.class);
        startActivity(intent);
    }

    public void cambiarStatus(View view) {
        Intent intent = new Intent(ActivityPedidoDetallado.this, ActivityCambiarEstatus.class);
        intent.putExtra("id_pedido", idPedido);
        startActivity(intent);
    }

    public void navigate(View view) {
        if (direccion != null) {
            if (direccion.getDenominacion() != null) {
                Uri google_maps = Uri.parse("google.navigation:q=" + direccion.getDenominacion());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, google_maps);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
            else {
                Toast.makeText(getApplicationContext(), "No se consiguieron las coordenas del cliente", Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "No se consiguieron las coordenas del cliente", Toast.LENGTH_LONG).show();
        }

    }

}
