package com.masgas.otoni.masgasmovil.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.masgas.otoni.masgasmovil.R;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaPedidos;
import com.masgas.otoni.masgasmovil.adapter.AdapterListaPedidosPorDespachar;
import com.masgas.otoni.masgasmovil.model.ModeloGeneral;
import com.masgas.otoni.masgasmovil.object.Pedido;
import com.masgas.otoni.masgasmovil.object.Usuario;
import com.masgas.otoni.masgasmovil.utils.MejorLocalizacion;

import java.util.List;

/**
 * Created by otoni on 31/1/2018.
 */

public class ActivityGestionPedidos extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recycler;
    private AdapterListaPedidosPorDespachar adapter;
    private RecyclerView.LayoutManager lManager;
    private SwipeRefreshLayout refreshLayout;
    Toolbar toolbar;

    private NavigationView navigationView;
    TextView nombre, entregados, sinEntregar, devolucion;

    ModeloGeneral modeloRelacional;

    List<Pedido> pedidos;
    Usuario usuario;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_menu_distribuidor);

        modeloRelacional = new ModeloGeneral(this);

        setToolbar();

        setupWindowAnimations();

        cargarMenu();

        entregados = findViewById(R.id.pedidos_entregados);
        sinEntregar = findViewById(R.id.pedidos_por_entregar);
        devolucion = findViewById(R.id.pedidos_devolucion);

        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        actualizarVista();
    }

    @Override
    public void onResume() {
        super.onResume();
        actualizarVista();
    }

    public void actualizarVista() {
        Integer ent = 0, noE = 0, dev = 0;

        pedidos = modeloRelacional.consultarPedidos();

        for (int i = 0; i < pedidos.size(); i++) {
            if (pedidos.get(i).getEstatus().equals("Por entregar")) {
                noE++;
            }
            else if (pedidos.get(i).getEstatus().equals("Entregado")) {
                ent++;
            }
            else if (pedidos.get(i).getEstatus().equals("Devolución")) {
                dev++;
            }
        }

        entregados.setText("" + ent);
        sinEntregar.setText("" + noE);
        devolucion.setText("" + dev);

        adapter = new AdapterListaPedidosPorDespachar(pedidos, this);
        recycler.setAdapter(adapter);

        MejorLocalizacion mejorLocalizacion = new MejorLocalizacion(this);
        if (!mejorLocalizacion.isGpsOn()) {
            mejorLocalizacion.AlertNoGps();
        }
    }

    private void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT > 20) {
            getWindow().setReenterTransition(new Explode());
            getWindow().setExitTransition(new Explode().setDuration(500));
        }
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Control de pedidos");
    }


    private void cargarMenu() {
        // Menu Lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.menu_navegacion_menu_principal_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        usuario = modeloRelacional.consultarUsuarioActivo();

        View hview = navigationView.getHeaderView(0);
        nombre = (TextView) hview.findViewById(R.id.menu_lateral_nombre);
        nombre.setText(usuario.getNombre1() + " " + usuario.getApellido1());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_icono) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            Intent intent = new Intent(ActivityGestionPedidos.this, ActivityPerfil.class);
            startActivity(intent);
        }
        if (id == R.id.nav_novedades) {
            Intent intent = new Intent(ActivityGestionPedidos.this, ActivityNovedades.class);
            startActivity(intent);
        }
        /*
        if (id == R.id.nav_ordenes) {
            Intent intent = new Intent(ActivityGestionPedidos.this, ActivityOrdenesPago.class);
            startActivity(intent);
        }
        */
        else if (id == R.id.nav_logout) {
            modeloRelacional.deleteAll();
            startActivity(new Intent(getBaseContext(), ActivityLogin.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_navegacion_menu_principal_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
