package com.masgas.otoni.masgasmovil.object;

import android.content.ContentValues;

import org.json.simple.JSONObject;

/**
 * Created by otoni on 17/1/2018.
 */

public class Pedido {

    // Datos de la Tabla
    public static final String TABLE = "pedido";

    // Nombre de las Columnas
    public static final String ID = "id";
    public static final String ID_DIRECCION = "id_direccion";
    public static final String FECHA = "fecha";
    public static final String CODIGO = "codigo";
    public static final String PAGADO = "pagado";
    public static final String ESTATUS = "estatus";
    public static final String NUEVO = "nuevo";
    public static final String MODIFICADO = "modificado";

    private Integer id;
    private Integer idDireccion;
    private String fecha;
    private String estatus;
    private String codigo;
    private Boolean pagado;

    public Pedido() {

    }

    public String queryCrearTabla() {
        return "CREATE TABLE " + TABLE +
                " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ID_DIRECCION + " INTEGER, " +
                FECHA + " TEXT, " +
                CODIGO + " TEXT, " +
                PAGADO + " BOOLEAN, " +
                ESTATUS + " BOOLEAN, " +
                NUEVO + " BOOLEAN, " +
                MODIFICADO + " BOOLEAN " +
                ")";
    }

    public void registrarJson(JSONObject pag) {

        Long id_direccion = (Long) pag.get("id_direccion");
        Long id_pedido_grupo = (Long) pag.get("id_pedido_grupo");
        String estatus = (String) pag.get("estatus");
        String fecha = (String) pag.get("fecha");
        String codigo = (String) pag.get("codigo");
        Long pagado = (Long) pag.get("pagado");

        this.setId(id_pedido_grupo.intValue());
        this.setIdDireccion(id_direccion.intValue());
        if (pagado.intValue() == 1) {
            this.setPagado(true);
        }
        else {
            this.setPagado(false);
        }
        this.setEstatus(estatus);
        this.setFecha(fecha);
        this.setCodigo(codigo);
    }

    public ContentValues crearRegistro(Boolean nuevo, Boolean modificado) {
        ContentValues values = new ContentValues();

        if (this.getId() != null) {
            values.put(ID, this.getId());
        }

        if (this.getCodigo() != null) {
            values.put(CODIGO, this.getCodigo());
        }

        if (this.getFecha() != null) {
            values.put(FECHA, this.getFecha());
        }

        if (this.getIdDireccion() != null) {
            values.put(ID_DIRECCION, this.getIdDireccion());
        }

        if (this.getPagado() != null) {
            values.put(PAGADO, this.getPagado());
        }

        if (this.getEstatus() != null) {
            values.put(ESTATUS, this.getEstatus());
        }

        values.put(NUEVO, nuevo);
        values.put(MODIFICADO, modificado);

        return values;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Boolean getPagado() {
        return pagado;
    }

    public void setPagado(Boolean pagado) {
        this.pagado = pagado;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
}
